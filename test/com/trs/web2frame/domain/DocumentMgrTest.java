/**
 * 2008-1-31
 */
package com.trs.web2frame.domain;

import java.util.ArrayList;

import junit.framework.TestCase;

import com.trs.web2frame.WCMServiceCaller;
import com.trs.web2frame.WCMServiceCallerTest;
import com.trs.web2frame.dispatch.Dispatch;
import com.trs.web2frame.entity.WDocument;

/**
 * Title: TRS 内容协作平台（TRS WCM） <BR>
 * Description: <BR>
 * TODO <BR>
 * Copyright: Copyright (c) 2004-2005 TRS信息技术有限公司 <BR>
 * Company: TRS信息技术有限公司(www.trs.com.cn) <BR>
 * 
 * @author TRS信息技术有限公司 LY
 * @version 1.0
 */

public class DocumentMgrTest extends TestCase {

	public void testSequence() {
		WDocument oDocument = new WDocument();
		oDocument.setFieldValue("ChannelId", new Integer(10));
		oDocument.setFieldValue("ObjectId", new Integer(0));
		oDocument.setFieldValue("DocTitle", "lalala....");
		oDocument.setFieldValue("DocHtmlCon", "ohohoh....");
		oDocument.setQuoteToChannelIds("11");
		ArrayList methodSequence = new ArrayList();
		methodSequence.add("save");
		methodSequence.add("quoteTo");
		WDocumentMgr.doService(oDocument, methodSequence);
	}

	public void testCreateNew() throws Exception {
		WDocument oDocument = new WDocument();
		oDocument.setFieldValue("ChannelId", new Integer(10));
		oDocument.setFieldValue("DocumentId", new Integer(0));
		oDocument.setFieldValue("DocTitle", "lalala....");
		oDocument.setFieldValue("DocHtmlCon", "ohohoh....");
		WDocumentMgr.save(oDocument);
	}

	public void testSequence2() throws Exception {
		WDocument oDocument = new WDocument();
		oDocument.setFieldValue("ChannelId", new Integer(10));
		oDocument.setFieldValue("DocumentId", new Integer(0));
		oDocument.setFieldValue("DocTitle", "lalala....");
		oDocument.setFieldValue("DocHtmlCon", "ohohoh....");
		oDocument.addAppendix(10, WCMServiceCallerTest.FILE_1);
		WDocumentMgr.save(oDocument);
		WDocumentMgr.saveAppendixs(oDocument);
	}

	public void testSequence3() throws Exception {
		WDocument oDocument = new WDocument();
		oDocument.setFieldValue("ChannelId", new Integer(10));
		oDocument.setFieldValue("DocumentId", new Integer(0));
		oDocument.setFieldValue("DocTitle", "我是中文，我有相关文档....");
		oDocument.setFieldValue("DocHtmlCon", "ohohoh....");
		oDocument.addRelation(898);
		oDocument.addRelation(884);
		WDocumentMgr.save(oDocument);
		WDocumentMgr.saveRelations(oDocument);
	}

	/**
	 * 保存文档及其所有相关项，包括引用栏目，附件，相关文档
	 * 
	 * @param _oDocument
	 * @throws Exception 
	 */
	private static void saveAll(WDocument _oDocument) throws Exception {
	    
	    
		WDocumentMgr.save(_oDocument);
		WDocumentMgr.quoteTo(_oDocument);
		WDocumentMgr.saveAppendixs(_oDocument);
		WDocumentMgr.saveRelations(_oDocument);
	}
	
	 /** 附件类型标识：文档附件 */
    public final static int FLAG_DOCAPD = 10; // 文件:Document Appendix

    /** 附件类型标识：文档图片 */
    public final static int FLAG_DOCPIC = 20; // 图片:Document Picture

    /** 附件类型标识：HTML */
    public final static int FLAG_HTMLPIC = 30; // HTML图片:Html Picture

    /** 附件类型标识：链接附件 */
    public final static int FLAG_LINK = 40; // 链接:Link

	public void testSaveAll() throws Exception {
		WDocument oDocument = new WDocument();
		
		oDocument.setFieldValue("ChannelId", new Integer(10));
		oDocument.setFieldValue("ObjectId", new Integer(0));
		
		oDocument.setFieldValue("DocTitle", "lalala....");
		oDocument.setFieldValue("DocHtmlCon", "ohohoh....");
		
		oDocument.addAppendix(FLAG_DOCAPD, WCMServiceCallerTest.FILE_1);
		
		
		
		oDocument.addRelation(898);
		oDocument.addRelation(884);
		
		saveAll(oDocument);
	}

	public void testFindById() throws Exception {
		int nDocId = 908;
		int nChannelId = 10;
		WDocument oDocument = WDocumentMgr.findById(nDocId, nChannelId, 0);
		if (oDocument != null) {
			System.out.println("oDocument.getFieldValue(\"DocHTMLCon\"):"
					+ oDocument.getFieldValue("DocHTMLCon"));
		}
	}

	public void testEdit() throws Exception {
		int nDocId = 910;
		int nChannelId = 10;
		WDocument oDocument = WDocumentMgr.findById(nDocId, nChannelId, 0);
		if (oDocument != null) {
			System.out.println("oDocument.getFieldValue(\"DocHTMLCon\"):"
					+ oDocument.getFieldValue("DocHTMLCon"));
			// 修改
			oDocument.setFieldValue("DocHTMLCon", "<h1>你好，我是中国人234</h1>123123");
			oDocument.setFieldValue("DocType", String.valueOf(20));
			WDocumentMgr.save(oDocument);
			oDocument = WDocumentMgr.findById(nDocId, nChannelId, 0);
			if (oDocument != null) {
				System.out.println("oDocument.getFieldValue(\"DocHTMLCon\"):"
						+ oDocument.getFieldValue("DocHTMLCon"));
			}
		} else {
			System.out.println("没找到该文档");
		}
	}

	public void testDelete() throws Exception {
		int nDocId = 907;
		int nChannelId = 10;
		WDocument oDocument = WDocumentMgr.findById(nDocId, nChannelId, 0);
		if (oDocument != null) {
			System.out.println("oDocument.getFieldValue(\"DocHTMLCon\"):"
					+ oDocument.getFieldValue("DocHTMLCon"));
		}
		boolean bDeleted = WDocumentMgr.delete(String.valueOf(nDocId),
				nChannelId, 0);
		System.out.println("bDeleted:" + bDeleted);
		oDocument = WDocumentMgr.findById(nDocId, nChannelId, 0);
		if (oDocument != null) {
			System.out.println("oDocument.getFieldValue(\"DocHTMLCon\"):"
					+ oDocument.getFieldValue("DocHTMLCon"));
		}
	}
}
