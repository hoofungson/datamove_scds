/**
 * 2008-1-31
 */
package com.trs.web2frame.domain;

import junit.framework.TestCase;

import com.trs.web2frame.entity.WChannel;

/**
 * Title: TRS 内容协作平台（TRS WCM） <BR>
 * Description: <BR>
 * TODO <BR>
 * Copyright: Copyright (c) 2004-2005 TRS信息技术有限公司 <BR>
 * Company: TRS信息技术有限公司(www.trs.com.cn) <BR>
 * 
 * @author TRS信息技术有限公司 LY
 * @version 1.0
 */

public class ChannelMgrTest extends TestCase {
    public void testFindByLocalId() throws Exception {
        int nSiteId = 1;
        String sLocalId = String.valueOf(4);
        WChannel oChannel = WChannelMgr.findByLocalId(nSiteId, sLocalId);
        if (oChannel != null) {
            System.out.println("oChannel.getFieldValue(\"CHNLName\"):"
                    + oChannel.getFieldValue("CHNLName"));
        }
    }

    public void testFindById() throws Exception {
        int nChannelId = 56;
        WChannel oChannel = WChannelMgr.findById(nChannelId);
        if (oChannel != null) {
            System.out.println("oChannel.getFieldValue(\"CHNLName\"):"
                    + oChannel.getFieldValue("CHNLName"));
        }
    }

    /*public void testEdit() throws Exception {
        int nChannelId = 56;
        WChannel oChannel = WChannelMgr.findById(nChannelId);
        if (oChannel != null) {
            System.out.println("oChannel.getFieldValue(\"CHNLName\"):"
                    + oChannel.getFieldValue("CHNLName"));
            System.out.println("oChannel.getFieldValue(\"CHNLDESC\"):"
                    + oChannel.getFieldValue("CHNLDESC"));
            oChannel.setFieldValue("CHNLName", "IT新闻");
            oChannel.setFieldValue("CHNLDESC", "IT新闻");
            WChannelMgr.save(oChannel);
            oChannel = WChannelMgr.findById(nChannelId);
            if (oChannel != null) {
                System.out.println("oChannel.getFieldValue(\"CHNLName\"):"
                        + oChannel.getFieldValue("CHNLName"));
                System.out.println("oChannel.getFieldValue(\"CHNLDESC\"):"
                        + oChannel.getFieldValue("CHNLDESC"));
            }
        }
    }*/
    
    public void testCreateNew() throws Exception {
        WChannel oChannel = new WChannel();
        int nLocalChannelId = 1;
        oChannel.setLocalChannelId(String.valueOf(nLocalChannelId));
        oChannel.setFieldValue("SITEID", new Integer(1));
        oChannel.setFieldValue("ParentId", new Integer(10));
        oChannel.setFieldValue("ChannelId", new Integer(0));
        oChannel.setFieldValue("Chnlname", "lalala");
        oChannel.setFieldValue("CHNLDESC", "我是外部加的");
        WChannelMgr.save(oChannel);
    }

    public void testDelete() throws Exception {
        int nChannelId = 32;
        WChannel oChannel = WChannelMgr.findById(nChannelId);
        if (oChannel != null) {
            System.out.println("oChannel.getFieldValue(\"CHNLName\"):"
                    + oChannel.getFieldValue("CHNLName"));
        }
        // boolean bDeleted = ChannelMgr.delete(String.valueOf(nChannelId),
        // false);
        boolean bDeleted = WChannelMgr.delete(String.valueOf(nChannelId));
        System.out.println("bDeleted:" + bDeleted);
        oChannel = WChannelMgr.findById(nChannelId);
        if (oChannel != null) {
            System.out.println("oChannel.getFieldValue(\"CHNLName\"):"
                    + oChannel.getFieldValue("CHNLName"));
        }
    }
}
