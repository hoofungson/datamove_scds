package com.trs.web2frame.domain;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;

import com.trs.cms.ContextHelper;
import com.trs.cms.auth.persistent.User;
import com.trs.components.wcm.content.persistent.Appendix;
import com.trs.components.wcm.content.persistent.Appendixes;
import com.trs.components.wcm.content.persistent.Channel;
import com.trs.components.wcm.content.persistent.ChnlDoc;
import com.trs.components.wcm.content.persistent.ChnlDocs;
import com.trs.components.wcm.content.persistent.Document;
import com.trs.infra.common.WCMException;
import com.trs.infra.persistent.WCMFilter;
import com.trs.infra.support.file.FilesMan;
import com.trs.infra.util.CMyString;
import com.trs.web2frame.ServiceConfig;
import com.trs.web2frame.entity.WAppendix;
import com.trs.web2frame.entity.WBaseObj;
import com.trs.web2frame.entity.WDocument;

public class DataCenterMgrTest {
	private final String m_sWcmUrl = "http://155.16.161.90:8080/wcm";

	public static final String PATH_SEPRATOR = ">>";

	public static final String EXTRADATA_DOCCHNLPATH = "DocChannelPath";

	public static final String EXTRADATA_CRUSER = "CrUser";

	public static final String EXTRADATA_DOCCRTIME = "DocCrTime";

	public static final String EXTRADATA_DOCAUTHOR = "DocAuthor";

	/**
	 * 测试数据中心服务的接口
	 * @throws Exception
	 */
	public void testPostData() throws Exception {
		ChnlDocs chnldocs = ChnlDocs.findByIds(null, "1,2,3");
		List lstDocuments = getRealDocuments(chnldocs);
		int nDocsSize = lstDocuments.size();
		List lstPostDocuments = new ArrayList(nDocsSize);
		ArrayList lstDocIds = new ArrayList(nDocsSize);
		ServiceConfig oServiceConfig = new ServiceConfig(m_sWcmUrl);
		for (Iterator iter = lstDocuments.iterator(); iter.hasNext();) {
			Document oDocument = (Document) iter.next();
			int nDocId = oDocument.getId();
			WDocument wDocument = getWDocument(oDocument, oServiceConfig);
			lstPostDocuments.add(wDocument);
			lstDocIds.add(String.valueOf(nDocId));
		}
		HashMap mapNewIdOldIds = new HashMap();
		int[] arrNewIds = DataCenterMgr.saveNew(lstPostDocuments,
				mapNewIdOldIds);
		for (int i = 0; i < arrNewIds.length; i++) {
			int nNewDocId = arrNewIds[i];
			int nDocId = Integer.parseInt((String) mapNewIdOldIds.get(String
					.valueOf(nNewDocId)));
			postAppendixes(getLoginUser(), nDocId, nNewDocId, oServiceConfig);
		}
	}

	private User getLoginUser() {
		return ContextHelper.getLoginUser();
	}

	private WDocument getWDocument(Document document,
			ServiceConfig oServiceConfig) throws WCMException {
		WDocument oWDocument = (WDocument) WBaseObj.getWBaseObj(document,
				new WDocument());
		oWDocument.setId(document.getId());
		// 根据栏目ID传送栏目路径
		oWDocument.setFieldValue(EXTRADATA_DOCCHNLPATH, getChnlPath(document
				.getChannel()));
		return oWDocument;
	}

	/**
	 * @param channel
	 * @return
	 * @throws WCMException
	 */
	public static String getChnlPath(Channel channel) throws WCMException {
		ArrayList lstChnlPathParts = new ArrayList();
		Channel parent = null;
		lstChnlPathParts.add(0, channel.getName());
		parent = channel.getParent();
		while (parent != null) {
			lstChnlPathParts.add(0, parent.getName());
			parent = parent.getParent();
		}
		lstChnlPathParts.add(0, channel.getSite().getName());
		return CMyString.join(lstChnlPathParts, PATH_SEPRATOR);
	}

	private List getRealDocuments(ChnlDocs chnldocs) throws WCMException {
		if (chnldocs == null || chnldocs.size() == 0) {
			return new ArrayList(0);
		}
		ArrayList lstDocuments = new ArrayList(chnldocs.size());
		for (int i = 0, size = chnldocs.size(); i < size; i++) {
			ChnlDoc chnldoc = (ChnlDoc) chnldocs.getAt(i);
			if (chnldoc == null) {
				continue;
			}
			Document document = chnldoc.getDocument();
			if (document == null) {
				continue;
			}
			lstDocuments.add(document);
		}
		return lstDocuments;
	}

	private void postAppendixes(User loginUser, int nDocId, int nNewDocId,
			ServiceConfig oServiceConfig) throws Exception {
		// 上传文档的附件
		int[] arrAppFlags = { Appendix.APPENDIX_FLAG_DOCPIC,
				Appendix.APPENDIX_FLAG_DOCAPD, Appendix.APPENDIX_FLAG_LINK };
		for (int i = 0; i < arrAppFlags.length; i++) {
			Appendixes oAppendixes = getAppendixes(loginUser, nDocId,
					arrAppFlags[i]);
			if (oAppendixes == null || oAppendixes.size() == 0) {
				continue;
			}
			List lstWAppendixes = buildWAppendixes(oAppendixes);
			WAppendixMgr.uploadAllAppendixs(lstWAppendixes, oServiceConfig);
			WAppendixMgr.saveAppendixs(nNewDocId, arrAppFlags[i],
					lstWAppendixes, oServiceConfig);
		}
	}

	private Appendixes getAppendixes(User loginUser, int nDocId, int _nAppFlag)
			throws WCMException {
		Appendixes oAppendixes = Appendixes.createNewInstance(loginUser);
		WCMFilter filter = new WCMFilter("", "APPDOCID=? and APPFLAG=?", "");
		filter.addSearchValues(0, nDocId);
		filter.addSearchValues(1, _nAppFlag);
		oAppendixes.open(filter);
		return oAppendixes;
	}

	/**
	 * Appedixes对象转换成WAppendix的List
	 * 
	 * @param oAppendixes
	 * @return
	 * @throws WCMException
	 */
	private List buildWAppendixes(Appendixes oAppendixes) throws WCMException {
		List lstWAppendixes = new ArrayList(oAppendixes.size());
		Appendix oAppendix = null;
		WAppendix oWAppendix = null;
		String sLocalFileName = null;
		Hashtable appendixProps = null;
		FilesMan fileman = FilesMan.getFilesMan();
		for (int j = 0, appsize = oAppendixes.size(); j < appsize; j++) {
			oAppendix = (Appendix) oAppendixes.getAt(j);
			if (oAppendix != null) {
				oWAppendix = new WAppendix();
				appendixProps = oAppendix.getAllProperty();
				appendixProps.remove(Appendix.DB_ID_NAME);
				Iterator keysItr = appendixProps.keySet().iterator();
				while (keysItr.hasNext()) {
					String sKey = (String) keysItr.next();
					Object oValue = appendixProps.get(sKey);
					if (oValue != null) {
						if ("APPFILE".equalsIgnoreCase(sKey)) {
							sLocalFileName = mapFullFileName(fileman,
									(String) oValue);
							oWAppendix.setFieldValue(sKey, sLocalFileName);
							oWAppendix.setUploadFile(sLocalFileName);
						} else {
							oWAppendix.setFieldValue(sKey, oValue.toString());
						}
					}
				}
				appendixProps.clear();
				lstWAppendixes.add(oWAppendix);
			}
		}
		return lstWAppendixes;
	}

	private static String mapFullFileName(FilesMan fileman, String fn)
			throws WCMException {
		return fileman.mapFilePath(fn, FilesMan.PATH_LOCAL) + fn;
	}

}
