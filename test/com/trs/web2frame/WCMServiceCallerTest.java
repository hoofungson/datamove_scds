/**
 * 2008-1-30
 */
package com.trs.web2frame;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import junit.framework.TestCase;

import com.trs.web2frame.dispatch.Dispatch;
import com.trs.web2frame.eventhandler.ICallbackCompleteHandler;
import com.trs.web2frame.eventhandler.ICallbackEventHandler;
import com.trs.web2frame.eventhandler.ICallbackSuccessHandler;
import com.trs.web2frame.util.JsonHelper;
/**
 * Title: TRS 内容协作平台（TRS WCM） <BR>
 * Description: <BR>
 * TODO <BR>
 * Copyright: Copyright (c) 2004-2005 TRS信息技术有限公司 <BR>
 * Company: TRS信息技术有限公司(www.trs.com.cn) <BR>
 * 
 * @author TRS信息技术有限公司 LY
 * @version 1.0
 */

public class WCMServiceCallerTest extends TestCase {
    /**
	 * 
	 */
    //public static final String FILE_1 = "D:\\Webapps\\TRS_WCM_52\\Tomcat\\webapps\\wcm\\infoview\\infoview.js";
	
	public static final String FILE_1 = "G:\\AppData\\SCDS\\Appendix\\a_1207107583188.doc";

    public void testCall() throws Exception {
        String sServiceId = "wcm6_document";
        String sMethodName = "query";

        Map oPostData = new HashMap();
        oPostData.put("ChannelId", new Integer(10));
        oPostData.put("PageSize", new Integer(1));

        Dispatch oDispatch = WCMServiceCaller.Call(sServiceId, sMethodName,
                oPostData, false);

        // 输出返回的结果
        System.out.println("=============解析返回的结果示例===========");
        System.out.println(oDispatch.getResponseText());

        // 如果需要解析返回的结果
        // 根据XML的结构解析返回的结果
        System.out.println("=============解析返回的结果解析示例===========");
        Map resultMap = oDispatch.getJson();
        //System.out.println(resultMap);
        System.out.println("Documents.Num:"+JsonHelper.getValueAsString(resultMap,
                "Documents.Num"));

        System.out.println("Documents.Document.DocId:"+JsonHelper.getValueAsString(resultMap,
        "Documents.Document.DocId"));
        
        
        
        
        // 对于多个节点可以按照以下的方式进行遍历
        List arDocuments = JsonHelper.getList(resultMap, "Documents.Document");
        for (int i = 0, nSize = arDocuments.size(); i < nSize; i++) {
            Map documentMap = (Map) arDocuments.get(i);
            if (documentMap == null)
                continue;
            
            System.out.println("Documents.Document["+i+"].DocId;"+JsonHelper.getValueAsString(documentMap,
            "DocId"));
        }
        
        
    }

    public void testCallback() throws Exception {
        String sServiceId = "wcm6_document";
        String sMethodName = "query";
        Map oPostData = new HashMap();
        oPostData.put("ChannelId", new Integer(11));
        ServiceObject oServiceObject = new ServiceObject(sServiceId,
                sMethodName);
        oServiceObject.setPostData(oPostData);
        oServiceObject.addEventHandlers(new ICallbackEventHandler[] {
                new ICallbackCompleteHandler() {
                    public void onComplete(Dispatch _dispatch)
                            throws Web2frameClientException {
                        System.out.println("hahaha, i'm completed.");
                    }
                }, new ICallbackSuccessHandler() {
                    public void onSuccess(Dispatch _dispatch)
                            throws Web2frameClientException {
                        System.out.println("oh yeah, i'm successed.");
                        System.out.println("responseText:"
                                + _dispatch.getResponseText());
                        Map oJson = _dispatch.getJson();
                        System.out.println("oJson:" + oJson);
                        System.out.println("DOCUMENTS.NUM:"
                                + JsonHelper.getValueAsString(oJson,
                                        "DOCUMENTS.NUM"));
                        List lstDocuments = JsonHelper.getList(oJson,
                                "DOCUMENTS.Document");
                        System.out
                                .println("DOCUMENTS.Document:" + lstDocuments);
                        if (lstDocuments != null && lstDocuments.size() > 0) {
                            System.out.println("Document.CRTIME:"
                                    + JsonHelper
                                            .getValueAsString(
                                                    (Map) lstDocuments.get(0),
                                                    "CRTIME"));
                            System.out.println("Document.DOCTYPE:"
                                    + JsonHelper.getValueAsString(
                                            (Map) lstDocuments.get(0),
                                            "DOCTYPE"));
                            System.out.println("Document.DOCTYPE.TYPE:"
                                    + JsonHelper.getValueAsString(
                                            (Map) lstDocuments.get(0),
                                            "DOCTYPE.TYPE"));
                        }
                    }
                } });
        WCMServiceCaller.Call(oServiceObject, false);
    }

    public void testCall2() throws Exception {
        String sServiceId = "wcm6_document";
        String sMethodName = "query";
        Map oPostData = new HashMap();
        oPostData.put("ChannelId", new Integer(10));
        Dispatch oDispatch = WCMServiceCaller.Call(sServiceId, sMethodName,
                oPostData, false);
        System.out.println("oDispatch.getResponseText():"
                + oDispatch.getResponseText());
    }

    public void testCall3() throws Exception {
        String sServiceId = "wcm6_document";
        String sMethodName = "query";
        ServiceObject oServiceObject = new ServiceObject(sServiceId,
                sMethodName);
        oServiceObject.setParameter("ChannelId", "10");
        Dispatch oDispatch = WCMServiceCaller.Call(oServiceObject, false);
        System.out.println("oDispatch.getResponseText():"
                + oDispatch.getResponseText());
    }

    public void testUploadFile() throws Exception {
        Dispatch oDispatch = WCMServiceCaller.UploadFile(FILE_1);
        System.out.println("oDispatch.getResponseText():"
                + oDispatch.getResponseText());
        System.out.println("oDispatch.getUploadShowName():"
                + oDispatch.getUploadShowName());
    }

    public void testPost() throws Exception {
        String sServiceId = "wcm6_document";
        String sMethodName = "query";
        Map oPostData = new HashMap();
        oPostData.put("ChannelId", new Integer(10));
        WCMServiceCaller.Call(sServiceId, sMethodName, oPostData, true);
    }
}
