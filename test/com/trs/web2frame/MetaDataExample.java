package com.trs.web2frame;

import java.util.HashMap;
import java.util.Map;

import junit.framework.TestCase;

import com.trs.web2frame.dispatch.Dispatch;
import com.trs.web2frame.util.JsonHelper;

public class MetaDataExample extends TestCase {

    public void addNewMetaData() {
    	/*
         * 应用场景：进行员工通讯录信息上传，包含姓名，性别，照片，个人简介等<BR>
         * 
         * 实现：元数据试图设置成3个字段： 普通文本型的姓名字段(物理字段名为xm)<BR>
         * 枚举类型的性别字段(物理字段名为xb,可选值为男，女)<BR> 附件类型的照片字段(物理字段名为zp)<BR>
         * 个人简介以文档附件的形式上传<BR>
         */

    	// 1. 根据实际需求，从WCM中获取对应的服务名和方法名：元数据字段信息的保存
        String sServiceId = "wcm6_MetaDataCenter";
        String sMethodName = "savemetaviewdata";

        // 2. 从第三方系统获取数据，构造服务所需传递参数
        Map oPostData = new HashMap();
        oPostData.put("ObjectId", new Integer(0));
        oPostData.put("CHANNELID", new Integer(97));

        oPostData.put("xm", "张三");
        oPostData.put("xb", "男");

        // 2.2 给服务器上传本地文件获得一个文件名，然后将返回的文件名赋值到指定的字段上
        String sLocalFileName = "D:\\TRSWCMV6.1\\Tomcat\\webapps\\wcm\\infoview\\照片.gif";
        Dispatch oDispatch = null;
		try {
			oDispatch = WCMServiceCaller.UploadFile(sLocalFileName);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        oPostData.put("zp", oDispatch.getUploadShowName());

        // 3. 参照接口格式，发生服务请求
        try {
			oDispatch = WCMServiceCaller.Call(sServiceId, sMethodName, oPostData,
			        true);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
        //4.解析返回值对象，取到所需字
        Map oJson = oDispatch.getJson();
        String metaDataid = JsonHelper.getValueAsString( 
                oJson ,"METAVIEWDATA.METADATAID");
        
        
        // 5. 进行附件管理，具体步骤同以上3步
        sServiceId = "wcm6_document";
        sMethodName = "saveAppendixes";

        oPostData = new HashMap();
        oPostData.put("DocId", Integer.valueOf(metaDataid));
        oPostData.put("AppendixType", new Integer(10));
        
        //5.1上传本地个人简介文件
        sLocalFileName = "D:\\TRSWCMV6.1\\Tomcat\\webapps\\wcm\\infoview\\个人简介.txt";
        try {
			oDispatch = WCMServiceCaller.UploadFile(sLocalFileName);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        String postData = "<OBJECTS><OBJECT ID='0' APPFILE='"
                + oDispatch.getUploadShowName()
                + "' APPLINKALT='[object Object]' APPFLAG='10' APPDESC='个人简介.txt'/></OBJECTS>";
        oPostData.put("APPENDIXESXML", postData);
        
        try {
			oDispatch = WCMServiceCaller.Call(sServiceId, sMethodName, oPostData,
			        true);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
}
