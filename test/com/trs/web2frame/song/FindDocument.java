package com.trs.web2frame.song;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.sun.org.apache.bcel.internal.generic.NEW;
import com.sun.xml.internal.txw2.Document;
import com.trs.components.wcm.WCMAppConfig;
import com.trs.components.wcm.content.persistent.Documents;
import com.trs.components.wcm.resource.Source;
import com.trs.infra.BaseServer;
import com.trs.infra.ServerConfig;
import com.trs.infra.persistent.WCMFilter;
import com.trs.web2frame.WCMServiceCaller;
import com.trs.web2frame.dispatch.Dispatch;
import com.trs.web2frame.domain.WAppendixMgr;
import com.trs.web2frame.domain.WDocumentMgr;
import com.trs.web2frame.entity.WAppendix;
import com.trs.web2frame.entity.WDocument;
import com.trs.web2frame.util.JsonHelper;

import junit.framework.TestCase;

public class FindDocument{
	
   public static void findDoc(){
	   System.out.println("哈哈，成功！");
	   //读取指定栏目下文档。
	   //获取服务名，方法名
	   String sServiceId = "wcm6_document";
	   String sMethodName = "query";

	   //构造传递参数
	   Map oPostData = new HashMap();
	   oPostData.put("ChannelId", new Integer(56));
	   //oPostData.put("DocId", new Integer(515));
	   oPostData.put("PageSize", new Integer(5));

	   //调用WCMServiceCaller接口
	   Dispatch oDispatch=null;
	   try {
		  oDispatch = WCMServiceCaller.Call(sServiceId, sMethodName,oPostData, false);
	   } catch (Exception e) {
		  // TODO Auto-generated catch block
		  e.printStackTrace();
	   }
	   
	   //解析Dispatch对象，获取所需字段值
	   Map oJson = oDispatch.getJson();
	           System.out.println("oJson:" + oJson);
	           System.out.println("DOCUMENTS.NUM:"
	                      + JsonHelper.getValueAsString(oJson,
	                                           "DOCUMENTS.NUM"));
	           List lstDocuments = JsonHelper.getList(oJson,
	                                   "DOCUMENTS.Document");
	           for(int i=0;i<lstDocuments.size();i++){
	        	   if (lstDocuments != null && lstDocuments.size() > 0) {
	        		   //打印文档编号。
	        		   System.out.println("编号："+JsonHelper.getValueAsString(
	                           (Map) lstDocuments.get(i),"DocID"));
	        		   
	            	   //打印文档标题。
	            	   System.out.println("标题："+JsonHelper.getValueAsString(
	                           (Map) lstDocuments.get(i),"DOCTITLE"));
	            	   
	            	   //打印文档创建时间。
	                    System.out.println("Document.CRTIME:" 
	               + JsonHelper.getValueAsString( 
	              (Map) lstDocuments.get(i),"CRTIME"));
	                    
	                    //打印文章类型。
	                    System.out.println("Document.DOCTYPE.TYPE:"
	                             + JsonHelper.getValueAsString(
	                             (Map) lstDocuments.get(i),"DOCTYPE.TYPE"));
	                    
	                    System.out.println("文档所属频道："+JsonHelper.getValueAsString(
	                            (Map) lstDocuments.get(i),"DOCCHANNEL.NAME"));//DOCHTMLCON
	                    
	                    System.out.println("来源："+JsonHelper.getValueAsString(
	                            (Map) lstDocuments.get(i),"DOCSOURCE"));
	               }
	        	   
	        	   System.out.println("========================================");
	           }
   }
   
   public static void testFindById() throws Exception {
		int nDocId = 515;
		int nChannelId = 56;
		WDocument oDocument = WDocumentMgr.findById(nDocId, nChannelId, 0);
		if (oDocument != null) {
			System.out.println("数据已生成====");
			
			//打印文档标题
			System.out.println("文档编号:"
					+ oDocument.getId());
			
			//打印文档标题
			System.out.println("文档标题:"
					+ oDocument.getFieldValue("DOCTITLE"));
			//打印文档日期。
			System.out.println("文档日期:"
					+ oDocument.getFieldValue("DOCPUBTIME"));
			
			//打印文档来源。
			System.out.println("文档来源:"
					+ oDocument.getFieldValue("DOCSOURCE"));
			
			//打印文档正文.
			System.out.println("文档正文:"
					+ oDocument.getFieldValue("DocHTMLCon"));
			
			//定义一个附件实体。
			System.out.println("附件："+oDocument.getAppendix().size());
			//List appendixList=oDocument.getAppendix();
			/*for(int i=0;i<appendixList.size();i++){
				System.out.println("附件："+appendixList.get(i));
			}*/
			
			
			
			//Source source=Source.findById(38);
			
			//System.out.println("打印："+source);
			
			//WAppendixMgr wAppendixMgr=new WAppendixMgr();
			
			
			
			/*com.trs.components.wcm.content.persistent.Document document=com.trs.components.wcm.content.persistent.Document.findById(515);
			System.out.println("打印了："+document.getTitle());*/
		}
		
		
	}
   
   public static void getFujian(){
	   String sServiceId = "wcm61_viewdocument";
	   String sMethodName = "queryAppendixes";
	   
	 //构造传递参数
	   Map oPostData = new HashMap();
	   oPostData.put("DocumentId", new Integer(515));//
	   //oPostData.put("AppendixType", new Integer(10));
	   

	   //调用WCMServiceCaller接口
	   //WCMServiceCaller.Call(sServiceId, sMethodName,oPostData, false);
	   
	   //BasicDataHelper 
	   
	   //System.out.println("哈哈："+BasicDataHelper.Call("", "",{DocumentId:'302',AppendixType:20}, true););
	
   }
   
   public static void main(String[] args) {
	   findDoc();
	   //getFujian();
	  /* try {
		testFindById();
	} catch (Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}*/
   }
}
