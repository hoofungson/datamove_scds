package com.trs.web2frame.song;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import com.google.gson.Gson;
import com.trs.web2frame.WCMServiceCaller;
import com.trs.web2frame.dispatch.Dispatch;
import com.trs.web2frame.entity.WDocument;
import com.trs.web2frame.util.JsonHelper;

import junit.framework.TestCase;

public class MyTest extends TestCase{
   public void read(){
	   System.out.println("哈哈，成功！");
	   
	 //获取服务名，方法名
	   String sServiceId = "wcm6_document";
	   String sMethodName = "query";

	   //构造传递参数
	   Map oPostData = new HashMap();
	   oPostData.put("ChannelId", new Integer(62));

	   //调用WCMServiceCaller接口
	   Dispatch oDispatch = null;
	   try {
		  oDispatch = WCMServiceCaller.Call(sServiceId, sMethodName,
		   oPostData, false);
	} catch (Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	   System.out.println("数据："+oDispatch.getJson());
	   System.out.println("数据键："+oDispatch.getJson().keySet());
	   System.out.println("数据值："+oDispatch.getJson().get("DOCUMENTS"));
	   
	   System.out.println("\n将Json数据转换为Employee对象：");
	   //Gson gson = new Gson();//new一个Gson对象
	   
	   Map oJson = oDispatch.getJson();
       //System.out.println("oJson:" + oJson);
       System.out.println("DOCUMENTS.NUM:"
                  + JsonHelper.getValueAsString(oJson,
                                       "DOCUMENTS.NUM"));
       List lstDocuments = JsonHelper.getList(oJson,
                               "DOCUMENTS.Document");
       for(int i=0;i<lstDocuments.size();i++){
    	   if (lstDocuments != null && lstDocuments.size() > 0) {
    		   //打印文档编号。
    		   System.out.println("编号："+JsonHelper.getValueAsString(
                       (Map) lstDocuments.get(i),"DocID"));
    		   
        	   //打印文档标题。
        	   System.out.println("标题："+JsonHelper.getValueAsString(
                       (Map) lstDocuments.get(i),"DOCTITLE"));
        	   
        	   //打印文档创建时间。
                System.out.println("Document.CRTIME:" 
           + JsonHelper.getValueAsString( 
          (Map) lstDocuments.get(i),"CRTIME"));
                
                //打印文章类型。
                System.out.println("Document.DOCTYPE.TYPE:"
                         + JsonHelper.getValueAsString(
                         (Map) lstDocuments.get(i),"DOCTYPE.TYPE"));
                
                System.out.println("文档所属频道："+JsonHelper.getValueAsString(
                        (Map) lstDocuments.get(i),"DOCCHANNEL.NAME"));//DOCHTMLCON
                
                System.out.println("来源："+JsonHelper.getValueAsString(
                        (Map) lstDocuments.get(i),"DOCSOURCE"));
           }
    	   
    	   System.out.println("========================================");
       }
       
   }
}
