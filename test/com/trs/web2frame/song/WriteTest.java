package com.trs.web2frame.song;

import java.util.HashMap;
import java.util.Map;

import com.trs.web2frame.WCMServiceCaller;
import com.trs.web2frame.dispatch.Dispatch;

public class WriteTest {

	
	private static void writeChannel(){
		System.out.println("==================进入栏目写方法==================");
		//说明：在Id为10的栏目下导入文档lalala…
		String sServiceId = "wcm6_document";
		String sMethodName = "save";
		Map oPostData = new HashMap();
		oPostData.put("ChannelId", new Integer(81));
		oPostData.put("ObjectId ", new Integer(0));
		oPostData.put("DocTitle ", "我的测试数据");
		oPostData.put("DocHtmlCon ", "测试栏目写入数据是否成功！哈哈哈，好像成功了！");
		
		try {
			Dispatch oDispatch = WCMServiceCaller.Call(sServiceId, sMethodName,oPostData, true);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		System.out.println("==================完成栏目写方法==================");
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
        writeChannel();
	}

}
