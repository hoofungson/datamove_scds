/**
 * 2008-2-3
 */
package com.trs.web2frame.entity;

import java.util.Iterator;
import java.util.Map;

import com.trs.infra.util.CMyString;

/**
 * Title: TRS 内容协作平台（TRS WCM） <BR>
 * Description: <BR>
 * TODO <BR>
 * Copyright: Copyright (c) 2004-2005 TRS信息技术有限公司 <BR>
 * Company: TRS信息技术有限公司(www.trs.com.cn) <BR>
 * 
 * @author TRS信息技术有限公司 LY
 * @version 1.0
 */

public class WMetaViewData extends WBaseObj {
	public final static String FIELD_META_VIEW_ID = "MetaViewId";

	public WMetaViewData() {
		super();
	}

	public String getIdField() {
		return FIELD_META_VIEW_ID;
	}

	public String toObjectXml() {
		Map mapMetaViewData = getContentBody();
		StringBuffer sbResult = new StringBuffer();
		sbResult.append("<OBJECT ");
		for (Iterator iter = mapMetaViewData.keySet().iterator(); iter
				.hasNext();) {
			String sFieldName = (String) iter.next();
			String sFieldValue = CMyString.filterForXML(mapMetaViewData.get(
					sFieldName).toString());
			sbResult.append(sFieldName);
			sbResult.append("=\"");
			sbResult.append(sFieldValue);
			sbResult.append("\" ");
		}
		sbResult.append("/>");
		return sbResult.toString();
	}
}
