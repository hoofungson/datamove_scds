/**
 * 2008-2-19
 */
package com.trs.web2frame.entity;

import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map;

import com.trs.infra.persistent.BaseObj;

/**
 * Title: TRS 内容协作平台（TRS WCM） <BR>
 * Description: <BR>
 * TODO <BR>
 * Copyright: Copyright (c) 2004-2005 TRS信息技术有限公司 <BR>
 * Company: TRS信息技术有限公司(www.trs.com.cn) <BR>
 * 
 * @author TRS信息技术有限公司 LY
 * @version 1.0
 */

public class WBaseObj {
	private Map m_oContentBody = null;

	public WBaseObj() {
		m_oContentBody = new HashMap();
	}

	public String getIdField() {
		return null;
	}

	public void setId(int _nObjId) {
		if (getIdField() != null) {
			setFieldValue(getIdField(), String.valueOf(_nObjId));
		}
	}

	public int getId() {
		if (getIdField() != null) {
			String sObjId = getFieldValue(getIdField());
			if (sObjId != null) {
				return Integer.parseInt(sObjId);
			}
		}
		return 0;
	}

	/**
	 * @return the documentBody
	 */
	public Map getContentBody() {
		return m_oContentBody;
	}

	public void setContentBody(Map _mpContentBody) {
		m_oContentBody = _mpContentBody;
	}

	public static WBaseObj getWBaseObj(BaseObj oBaseObj, WBaseObj oWBaseObj) {
		Hashtable oBaseObjProps = oBaseObj.getAllProperty();
		Iterator keysItr = oBaseObjProps.keySet().iterator();
		while (keysItr.hasNext()) {
			String sKey = (String) keysItr.next();
			Object oValue = oBaseObjProps.get(sKey);
			if (oValue != null) {
				oWBaseObj.setFieldValue(sKey, oValue.toString());
			}
		}
		return oWBaseObj;
	}

	public void setFieldValue(String _sFieldName, Object _oFieldValue) {
		if (_oFieldValue == null) {
			getContentBody().put(_sFieldName.toUpperCase(), "");
			return;
		}
		getContentBody()
				.put(_sFieldName.toUpperCase(), _oFieldValue.toString());
	}

	public String getFieldValue(String _sFieldName) {
		Object oValue = getContentBody().get(_sFieldName.toUpperCase());
		return (oValue == null) ? null : oValue.toString();
	}
}
