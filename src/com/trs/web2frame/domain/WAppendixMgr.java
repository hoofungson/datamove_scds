package com.trs.web2frame.domain;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.trs.web2frame.ServiceConfig;
import com.trs.web2frame.ServiceObject;
import com.trs.web2frame.WCMServiceCaller;
import com.trs.web2frame.dispatch.Dispatch;
import com.trs.web2frame.entity.WAppendix;
import com.trs.web2frame.eventhandler.ICallbackEventHandler;

public class WAppendixMgr {

	static void saveAppendixs(int _nDocId, int nAppendixType, List lstAppendixes)
			throws Exception {
		saveAppendixs(_nDocId, nAppendixType, lstAppendixes,
				(ICallbackEventHandler[]) null, (ServiceConfig) null);
	}

	public static void saveAppendixs(int _nDocId, int nAppendixType,
			List lstAppendixes, ServiceConfig _oServiceConfig) throws Exception {
		saveAppendixs(_nDocId, nAppendixType, lstAppendixes,
				(ICallbackEventHandler[]) null, _oServiceConfig);
	}

	public static void saveAppendixs(int _nDocId, int nAppendixType,
			List lstAppendixes, ICallbackEventHandler[] _arrHandlers)
			throws Exception {
		saveAppendixs(_nDocId, nAppendixType, lstAppendixes, _arrHandlers,
				(ServiceConfig) null);
	}

	public static void saveAppendixs(int _nDocId, int nAppendixType,
			List lstAppendixes, ICallbackEventHandler[] _arrHandlers,
			ServiceConfig _oServiceConfig) throws Exception {
		uploadAllAppendixs(lstAppendixes, _oServiceConfig);
		ServiceObject oServiceObject = new ServiceObject(
				WDocumentMgr.ms_ServiceId, "saveAppendixes");
		Map oPostData = new HashMap();
		oPostData.put(WDocumentMgr.FIELD_DOC_ID, String.valueOf(_nDocId));
		oPostData.put("AppendixType", String.valueOf(nAppendixType));
		oPostData.put("AppendixesXML", getAppendixsXML(lstAppendixes));
		oServiceObject.setPostData(oPostData);
		oServiceObject.addEventHandlers(_arrHandlers);
		if (_oServiceConfig != null) {
			new WCMServiceCaller(_oServiceConfig).MyCall(oServiceObject, true);
		} else {
			WCMServiceCaller.Call(oServiceObject, true);
		}
	}

	public static Dispatch uploadFile(String _sFileName, String _sUploadFlag,
			ServiceConfig _oServiceConfig) throws Exception {
		if (_oServiceConfig != null) {
			return new WCMServiceCaller(_oServiceConfig).MyUploadFile(_sFileName,
					_sUploadFlag);
		} else {
			return WCMServiceCaller.uploadFile(_sFileName, _sUploadFlag);
		}
	}

	public static void upload(WAppendix oAppendix) throws Exception {
		oAppendix.upload();
	}

	public static void upload(WAppendix oAppendix, ServiceConfig _oServiceConfig)
			throws Exception {
		if (_oServiceConfig != null) {
			oAppendix.upload(new WCMServiceCaller(_oServiceConfig));
		} else {
			oAppendix.upload();
		}
	}

	static void uploadAllAppendixs(List lstAppendixes) throws Exception {
		for (Iterator iter = lstAppendixes.iterator(); iter.hasNext();) {
			WAppendix oAppendix = (WAppendix) iter.next();
			oAppendix.upload();
		}
	}

	public static void uploadAllAppendixs(List lstAppendixes,
			ServiceConfig _oServiceConfig) throws Exception {
		for (Iterator iter = lstAppendixes.iterator(); iter.hasNext();) {
			WAppendix oAppendix = (WAppendix) iter.next();
			upload(oAppendix, _oServiceConfig);
		}
	}

	public static String getAppendixsXML(List _lstAppendixs) {
		StringBuffer sbResult = new StringBuffer();
		sbResult.append("<OBJECTS>");
		for (Iterator iter = _lstAppendixs.iterator(); iter.hasNext();) {
			WAppendix oAppendix = (WAppendix) iter.next();
			sbResult.append(oAppendix.toObjectXml());
		}
		sbResult.append("</OBJECTS>");
		return sbResult.toString();
	}
}
