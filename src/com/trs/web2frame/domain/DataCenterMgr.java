package com.trs.web2frame.domain;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.trs.infra.util.CMyString;
import com.trs.web2frame.ServiceConfig;
import com.trs.web2frame.ServiceObject;
import com.trs.web2frame.WCMServiceCaller;
import com.trs.web2frame.dispatch.Dispatch;
import com.trs.web2frame.entity.WBaseObj;
import com.trs.web2frame.entity.WDocument;
import com.trs.web2frame.entity.WMetaViewData;
import com.trs.web2frame.util.JsonHelper;

public class DataCenterMgr {
	public static final String FIELD_OBJECT_IDS = "ObjectIds";

	public static final String FIELD_OBJECT_ID = "ObjectId";

	public final static String ms_ServiceId = "wcm6_DataCenter";

	static int save(final WDocument _oDocument) throws Exception {
		return WDocumentMgr.save(_oDocument);
	}

	public static int save(final WDocument _oDocument,
			ServiceConfig _oServiceConfig) throws Exception {
		// TODO
		return WDocumentMgr.save(_oDocument);
	}

	static int save(final WMetaViewData _oMetaViewData) throws Exception {
		return save(_oMetaViewData, null);
	}

	public static int save(final WMetaViewData _oMetaViewData,
			ServiceConfig _oServiceConfig) throws Exception {
		ServiceObject oServiceObject = new ServiceObject(ms_ServiceId,
				"saveMetaViewData");
		Map oPostData = _oMetaViewData.getContentBody();
		oPostData.put(FIELD_OBJECT_ID, CMyString.showNull(_oMetaViewData
				.getFieldValue(WMetaViewData.FIELD_META_VIEW_ID), "0"));
		oServiceObject.setPostData(oPostData);
		Dispatch oDispatch = null;
		if (_oServiceConfig != null) {
			oDispatch = new WCMServiceCaller(_oServiceConfig).MyCall(
					oServiceObject, true);
		} else {
			oDispatch = WCMServiceCaller.Call(oServiceObject, true);
		}
		String sDocumentId = JsonHelper.getValueAsString(oDispatch.getJson(),
				"result");
		_oMetaViewData.setFieldValue(WMetaViewData.FIELD_META_VIEW_ID,
				sDocumentId);
		return Integer.parseInt(sDocumentId);
	}

	public static int[] saveNew(List lstWDocuments) throws Exception {
		return saveNew(lstWDocuments, (Map) null);
	}

	public static int[] saveNew(List lstWDocuments,
			ServiceConfig _oServiceConfig) throws Exception {
		return saveNew(lstWDocuments, (Map) null, _oServiceConfig);
	}

	static int[] saveNew(List lstWDocuments, Map _mapSomeMoreResult)
			throws Exception {
		return saveNew(lstWDocuments, (Map) null, (ServiceConfig) null);
	}

	public static int[] saveNew(List lstWDocuments, Map _mapSomeMoreResult,
			ServiceConfig _oServiceConfig) throws Exception {
		if (lstWDocuments == null || lstWDocuments.size() == 0) {
			return new int[0];
		}
		ArrayList lstServiceObject = new ArrayList(lstWDocuments.size());
		ArrayList lstOkWDocuments = new ArrayList(lstWDocuments.size());
		ServiceObject oServiceObject = null;
		for (Iterator iter = lstWDocuments.iterator(); iter.hasNext();) {
			WBaseObj element = (WBaseObj) iter.next();
			Map oPostData = element.getContentBody();
			if (element == null)
				continue;
			if (element instanceof WMetaViewData) {
				oServiceObject = new ServiceObject(ms_ServiceId,
						"saveMetaViewData");
				oPostData.put(FIELD_OBJECT_ID, "0");
			} else if (element instanceof WDocument) {
				oServiceObject = new ServiceObject(ms_ServiceId, "saveDocument");
				oPostData.put(FIELD_OBJECT_ID, "0");
			} else {
				// 暂不支持
				continue;
			}
			oServiceObject.setPostData(oPostData);
			lstServiceObject.add(oServiceObject);
			lstOkWDocuments.add(element);
		}
		ServiceObject[] arrServiceObject = new ServiceObject[lstServiceObject
				.size()];
		lstServiceObject.toArray(arrServiceObject);
		Dispatch oDispatch = null;
		if (_oServiceConfig != null) {
			oDispatch = new WCMServiceCaller(_oServiceConfig)
					.MyMultiCall(arrServiceObject);
		} else {
			oDispatch = WCMServiceCaller.MultiCall(arrServiceObject);
		}
		List lstIds = JsonHelper.getList(oDispatch.getJson(), "result");
		int[] rstIds = new int[lstIds.size()];
		for (int i = 0; i < rstIds.length; i++) {
			rstIds[i] = Integer.parseInt((String) lstIds.get(i));
			WBaseObj element = (WBaseObj) lstOkWDocuments.get(i);
			// 缓存新ID对应的旧ID
			if (_mapSomeMoreResult != null) {
				_mapSomeMoreResult.put(String.valueOf(rstIds[i]), String
						.valueOf(element.getId()));
			}
			element.setId(rstIds[i]);
		}
		return rstIds;
	}
}
