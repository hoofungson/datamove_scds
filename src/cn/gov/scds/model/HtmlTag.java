package cn.gov.scds.model;

public class HtmlTag {
   private int singleFlag;
   private String htmlStartTag;
   private String flterStartTag;
   private String htmlEndTag;
   private String flterEndTag;
   
   public HtmlTag() {
	 super();
	 // TODO Auto-generated constructor stub
   }
   
   public HtmlTag(int singleFlag, String htmlStartTag, String flterStartTag,
		String htmlEndTag, String flterEndTag) {
	super();
	this.singleFlag = singleFlag;
	this.htmlStartTag = htmlStartTag;
	this.flterStartTag = flterStartTag;
	this.htmlEndTag = htmlEndTag;
	this.flterEndTag = flterEndTag;
   }

   public int getSingleFlag() {
	  return singleFlag;
   }

   public void setSingleFlag(int singleFlag) {
	  this.singleFlag = singleFlag;
   }

   public String getHtmlStartTag() {
	  return htmlStartTag;
   }

   public void setHtmlStartTag(String htmlStartTag) {
	  this.htmlStartTag = htmlStartTag;
   }

   public String getFlterStartTag() {
	  return flterStartTag;
   }

   public void setFlterStartTag(String flterStartTag) {
	  this.flterStartTag = flterStartTag;
   }

   public String getHtmlEndTag() {
	  return htmlEndTag;
   }

   public void setHtmlEndTag(String htmlEndTag) {
	  this.htmlEndTag = htmlEndTag;
   }

   public String getFlterEndTag() {
	  return flterEndTag;
   }

   public void setFlterEndTag(String flterEndTag) {
	  this.flterEndTag = flterEndTag;
   }
}
