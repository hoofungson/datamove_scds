package cn.gov.scds.service.impl;

import java.util.List;

import cn.gov.scds.dao.ContentDao;
import cn.gov.scds.entity.Content;
import cn.gov.scds.service.ContentService;

public class ContentServiceImpl implements ContentService{

	private ContentDao contentDao;
	
	public ContentServiceImpl(ContentDao contentDao){
		this.setContentDao(contentDao);
	}
	
	public ContentDao getContentDao() {
		return contentDao;
	}

	public void setContentDao(ContentDao contentDao) {
		this.contentDao = contentDao;
	}

	public List<Content> findAll() {
		// TODO Auto-generated method stub
		return this.getContentDao().findAll();
	}

	public List<Content> findByItemID(int item_id) {
		// TODO Auto-generated method stub
		return this.getContentDao().findByItemID(item_id);
	}

	public List<Content> findByID(int contentID) {
		// TODO Auto-generated method stub
		return this.getContentDao().findByID(contentID);
	}

	public List<Content> find(int item_id, int contentID) {
		// TODO Auto-generated method stub
		return this.getContentDao().find(item_id, contentID);
	}

}
