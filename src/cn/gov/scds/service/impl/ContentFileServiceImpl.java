package cn.gov.scds.service.impl;

import java.util.List;

import cn.gov.scds.dao.ContentFileDao;
import cn.gov.scds.entity.ContentFile;
import cn.gov.scds.service.ContentFileService;

public class ContentFileServiceImpl implements ContentFileService{
	
	private ContentFileDao contentFileDao;
	

	public ContentFileDao getContentFileDao() {
		return contentFileDao;
	}

	public void setContentFileDao(ContentFileDao contentFileDao) {
		this.contentFileDao = contentFileDao;
	}

	public List<ContentFile> findAll() {
		// TODO Auto-generated method stub
		return this.getContentFileDao().findAll();
	}

	public List<ContentFile> findByContentID(int content_id) {
		// TODO Auto-generated method stub
		return this.getContentFileDao().findByContentID(content_id);
	}

	public List<ContentFile> findByID(int contentID) {
		// TODO Auto-generated method stub
		return this.getContentFileDao().findByID(contentID);
	}

	public List<ContentFile> find(int content_id, int contentID) {
		// TODO Auto-generated method stub
		return this.getContentFileDao().find(content_id, contentID);
	}

}
