package cn.gov.scds.service;

import java.util.List;

import cn.gov.scds.entity.Content;
import cn.gov.scds.entity.ContentFile;

public interface ContentFileService {
	public List<ContentFile> findAll();
    public List<ContentFile> findByContentID(int content_id);
    public List<ContentFile> findByID(int contentID);
    public List<ContentFile> find(int content_id,int contentID);
}
