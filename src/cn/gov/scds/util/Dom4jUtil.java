/**
 * @Title: Dom4jUtil.java
 * @Description: TODO
 * @author sunghu
 * @date 2015年7月21日 上午11:05:31
 * @最后修改人：aiowang
 * @最后修改时间：2015年7月21日 上午11:05:31
 * @version V1.0
 * @copyright: 四川凯普顿信息技术有限公司
 */ 

package cn.gov.scds.util;

import java.io.File;  
import java.io.FileOutputStream;  
import java.io.FileWriter;  
import java.io.OutputStreamWriter;  
import java.nio.charset.Charset;  
import java.nio.charset.CharsetEncoder;  
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;  
import java.util.List;
import java.util.Map;

import org.dom4j.Attribute;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.OutputFormat;
import org.dom4j.io.SAXReader;
import org.dom4j.io.XMLWriter;

/**
 * @author : sunghu
 * @Description: TODO
 * @JDK version ：jdk1.6 
 */
public class Dom4jUtil {
    
    // 创建saxReader对象  
   public static SAXReader reader;
    // 通过read方法读取一个文件 转换成Document对象  
   public static Document document;
    //获取根节点元素对象  
   public static Element node;
   public  List<Map<String, Object>> nodes;
   public Map<String, Object> nodeMap;
    
    /**
     * 
     */
    public Dom4jUtil() {
        super();
        // TODO Auto-generated constructor stub
        reader = new SAXReader();
        try {
        	String relativelyPath=System.getProperty("user.dir");
        	//System.out.println("项目路径："+relativelyPath);
            document = reader.read(new File(relativelyPath+"\\src\\CityConfig.xml"));
        } catch (DocumentException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        node = document.getRootElement();
        nodes=new ArrayList<Map<String, Object>>();
    	nodeMap=new HashMap<String, Object>();
    }
    
    
    /** 
     * 把document对象写入新的文件 
     *  
     * @param document 
     * @throws Exception 
     */  
    public void writer(Document document) throws Exception {  
        // 紧凑的格式  
        // OutputFormat format = OutputFormat.createCompactFormat();  
        // 排版缩进的格式  
        OutputFormat format = OutputFormat.createPrettyPrint();  
        // 设置编码  
        format.setEncoding("UTF-8");  
        // 创建XMLWriter对象,指定了写出文件及编码格式  
        // XMLWriter writer = new XMLWriter(new FileWriter(new  
        // File("src//a.xml")),format);  
        XMLWriter writer = new XMLWriter(new OutputStreamWriter(new FileOutputStream(new File("src//a.xml")), "UTF-8"), format);  
        // 写入  
        writer.write(document);
        // 立即写入  
        writer.flush();
        // 关闭操作  
        writer.close();
    }  
  
    /** 
     * 遍历当前节点元素下面的所有(元素的)子节点 
     *  
     * @param node 
     */  
    public void listNodes(Element node) {  
        System.out.println("当前节点的名称：" + node.getName());
        // 获取当前节点的所有属性节点 
        List<Attribute> list = node.attributes();
        // 遍历属性节点  
        for (Attribute attr : list) {
            System.out.println(attr.getText() + "-----" + attr.getName()
                    + "---" + attr.getValue());
        }
  
        if (!(node.getTextTrim().equals(""))) {
            System.out.println("文本内容：" + node.getText());
        }
  
        // 当前节点下面子节点迭代器  
        Iterator<Element> it = node.elementIterator();
        // 遍历  
        while (it.hasNext()) {
            // 获取某个子节点对象  
            Element e = it.next();
            // 对子节点进行遍历  
            listNodes(e);
        }  
    }
    
    /*
     * 遍历某节点下面的子节点，并且返回一个列表
     * */
    public List<Map<String, Object>> listNode(Element node){
    	
    	System.out.println("当前节点的名称：" + node.getName());
    	
        // 获取当前节点的所有属性节点 
        List<Attribute> list = node.attributes();
        // 遍历属性节点  
        for (Attribute attr : list) {
            System.out.println(attr.getText() + "-----" + attr.getName()
                    + "---" + attr.getValue());
        }
  
        if (!(node.getTextTrim().equals(""))) {
            System.out.println("文本内容：" + node.getText());
            //将当前节点添加到键值映射对
            nodeMap.put(node.getName(), node.getText());
        }
        
        // 当前节点下面子节点迭代器  
        Iterator<Element> it = node.elementIterator();
        // 遍历  
        while (it.hasNext()) {
            // 获取某个子节点对象  
            Element e = it.next();
            //nodes.add(it.next());
            // 对子节点进行遍历  
            listNode(e);
        }
    	
        //将键值映射对添加到列表中
        nodes.add(nodeMap);
    	return nodes;
    }
  
    /** 
     * 介绍Element中的element方法和elements方法的使用 
     *  
     * @param node 
     */  
    public void elementMethod(Element node) {
        // 获取node节点中，子节点的元素名称为西游记的元素节点。 
        Element e = node.element("西游记");
        // 获取西游记元素节点中，子节点为作者的元素节点(可以看到只能获取第一个作者元素节点)
        Element author = e.element("作者");
  
        System.out.println(e.getName() + "----" + author.getText());
  
        // 获取西游记这个元素节点 中，所有子节点名称为作者元素的节点 。  
  
        List<Element> authors = e.elements("作者");
        for (Element aut : authors) {
            System.out.println(aut.getText());
        }
  
        // 获取西游记这个元素节点 所有元素的子节点。 
        List<Element> elements = e.elements();
  
        for (Element el : elements) {
            System.out.println(el.getText());
        }
  
    }
    
    /*
     * 返回指定某节点下面的某个子节点的值
     * */
    public Object elementValue(Element node){
        String elementValue="";
     // 获取当前节点的所有属性节点 
        List<Attribute> list = node.attributes();
        // 遍历属性节点  
        for (Attribute attr : list) {
            /*System.out.println(attr.getText() + "-----" + attr.getName()
                    + "---" + attr.getValue());*/
        }
  
        if (!(node.getTextTrim().equals(""))) {
            //System.out.println("文本内容：：：：" + node.getText());
            elementValue=node.getText();
        }else{
            
        }
  
        // 当前节点下面子节点迭代器  
        Iterator<Element> it = node.elementIterator();
        // 遍历  
        while (it.hasNext()) {
            // 获取某个子节点对象  
            Element e = it.next();
            // 对子节点进行遍历  
            listNodes(e);
        }
        
        return elementValue;
    }
}
