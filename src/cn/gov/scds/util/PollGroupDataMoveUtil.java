package cn.gov.scds.util;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import cn.gov.scds.entity.Poll;
import cn.gov.scds.entity.PollGroup;
import cn.gov.scds.jdbc.DBConnection;
import cn.gov.scds.jdbc.DBInteractionConnection;
import cn.gov.scds.jdbc.DBUitl;

public class PollGroupDataMoveUtil {
    /*
     * 问卷调查模块的数据迁移
     * */
	public static Connection connect=null;
	public static Statement statement=null;
	public static ResultSet resultSet=null;
	private static int recordId=1900;
	
	/*
	 * 数据迁移方法
	 * */
	public void dataMove(){
		readDc_projectByJDBC();
	}
	
	/*
	 * 读取调查模块中的调查项目表
	 * */
	private void readDc_projectByJDBC(){
		connect=DBConnection.getConnection();
		statement=DBUitl.getStatement(connect);
		
		Connection iconnection=DBInteractionConnection.getConnection();
		Statement iStatement=DBUitl.getStatement(iconnection);
		
		String sqlStr="select * from DC_VOTE where ZF=0 order by VOTE_ID";
		ResultSet resultSet=DBUitl.getResultSet(statement,sqlStr);
		try {
			while(resultSet.next()){
				PollGroup pollGroup=new PollGroup();
				pollGroup.setPollgroupid(resultSet.getInt("VOTE_ID")+2215);
				pollGroup.setPollid(resultSet.getInt("VOTE_PROJECT_ID")+2148);
				pollGroup.setGrouptitle(resultSet.getString("VOTE_TITLE"));
				
				System.out.println("调查项目编号："+pollGroup.getPollgroupid());
				System.out.println("调查项目标题："+pollGroup.getPollid());
				System.out.println("调查项目描述："+pollGroup.getGrouptitle());
				System.out.println("===========分割线========");
				
				writeDc_projectByJDBC(pollGroup,iStatement);
			  }
			}catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	}
	
	/*
	 * 写入调查模块的调查项目表
	 * */
	private void writeDc_projectByJDBC(PollGroup pollGroup,Statement iStatement){
		String sqlStr="insert into POLLGROUP (pollgroupid,pollid,grouptitle) values ("+pollGroup.getPollgroupid()+","+pollGroup.getPollid()+",'"+pollGroup.getGrouptitle()+"')";
		try {
			iStatement.executeUpdate(sqlStr);
			System.out.println("=========数据导入成功.......");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		recordId++;
	}
}
