package cn.gov.scds.util;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import org.omg.CORBA.PRIVATE_MEMBER;

import com.hxtt.sql.cn;

/*
 * 正文中的媒体部分替换工具。
 * */
public class MediaUtil {
	private static HtmlRegexpUtil htmlRegexpUtil=null;
	public static List<String> dimages;
	public static List<String> dlinks;
	
     /*
      * 媒体替换
      * */
	
	public static String replaceMediaCharacter(String content){
		//content=replaceSRCPath(content);
		content=replaceSWFCharacter(content);
		//content=replaceMP3Character(content);
		
		return content;
	}
	
	/*
	 * MP3字符串替换
	 * */
	private static String replaceMP3Character(String content){
		htmlRegexpUtil=new HtmlRegexpUtil();
		content=htmlRegexpUtil.replaceHtmlTag(content, "object id=\"MediaPlayer\" height=\"343\" width=\"397\"", "src", "<embed src=\"", "\"/>");
		content=htmlRegexpUtil.fiterHtmlTag(content, "param");
		content=content.replace("classid=\"clsid:22D6F312-B0F6-11D0-94AB-0080C74C7E95\"", "");
		content=replaceSRCPath(content);
		content=content.replace("</object>", "");
		
		return content;
	}
	
	/*
	 * SWF字符串替换
	 * */
	private static String replaceSWFCharacter(String content){
		htmlRegexpUtil=new HtmlRegexpUtil();
		try {
			content=htmlRegexpUtil.replaceHtmlTag(content, "param", "value", "<embed src=\"", "\" />");
			content=htmlRegexpUtil.fiterHtmlTag(content, "object");
			content=content.replace("name=\"movie\" ", "");
			content=content.replace("name=\"quality\" <embed src=\"high\" />", "");
			content=content.replace("</object>", "");
			
			content=replaceSRCPath(content);
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("记录异常提醒===========");
		}
		//content=htmlRegexpUtil.replaceHtmlTag(content, "param", "value", "<embed src=\"", "\" />");
		//content=htmlRegexpUtil.fiterHtmlTag(content, "object");
		//content=content.replace("name=\"movie\" ", "");
		//content=content.replace("name=\"quality\" <embed src=\"high\" />", "");
		//content=replaceSRCPath(content);
		//content=content.replace("</object>", "");
		
		return content;
	}
	
	/*
	 * SRC路径替换
	 * */
	private static String replaceSRCPath(String content){
		
		/*四川省地税内网*/
		content=content.replace("src=\"/", "src=\"http://155.16.161.140/");
		
		/*四川省地税外网*/
		//content=content.replace("src=\"/", "src=\"http://155.16.16.60:7001/");
		
		/*成都地税外网*/
		//content=content.replace("src=\"/", "src=\"http://www.cdlocaltax.chengdu.gov.cn/");
		//content=content.replace("src=\"http://155.20.45.23:7001", "src=\"http://www.cdlocaltax.chengdu.gov.cn");
		
		/*成都地税内网*/
		//content=content.replace("src=\"/", "src=\"http://155.20.30.51/");
		
		/*达州地税内网*/
		/*content=content.replace("src=\"[InstallDir_ChannelDir]{$UploadDir}", "src=\"/Article/UploadFiles");
		content=content.replace("href=\"[InstallDir_ChannelDir]{$UploadDir}", "href=\"http://155.76.16.9/Article/UploadFiles");*/
		
		/*乐山地税内网*/
		/*content=content.replace("href=\"/", "href=\"http://155.60.16.88/");
		content=content.replace("在线看杂志", "说明：为了适应新的平台系统，嘉州地税部分栏目的在线看杂志已经取消链接，用户可以自行下载附件到本地查看，给大家带来的不便还请见谅。");
		content=content.replace("点击下载：", "请下载附件：");
		content=content.replace("下载打包文件", "点击附件，下载打包文件！");*/
		
		/*内江地税内网*/
		/*content=content.replace("href=\"/", "href=\"http://155.52.16.4/");
		content=content.replace("src=\"/", "src=\"http://155.52.16.4/");
		content=content.replace("127.0.0.1", "155.52.16.4");*/
		
		/*内江地税外网*/
		/*content=content.replace("href=\"/", "href=\"http://www.scnj-l-tax.gov.cn/");
		content=content.replace("src=\"/", "src=\"http://www.scnj-l-tax.gov.cn/");
		content=content.replace("127.0.0.1", "www.scnj-l-tax.gov.cn");*/
		
		/*眉山地税内网*/
		/*content=content.replace("href=\"/", "href=\"http://155.64.16.15/");
		content=content.replace("src=\"/", "src=\"http://155.64.16.15/");
		content=content.replace("127.0.0.1", "155.64.16.15");
		content=content.replace("alt=&quot;&quot;", "alt=\"眉山地税\"");
		content=content.replace("&quot;", "\"");
		content=content.replace("src=\"/", "src=\"http://155.64.16.15/");*/
		
		/*自贡地税外网*/
		/*content=content.replace("&nbsp;", " ");
		content=content.replace("&#61;", "=");
		content=content.replace("src=\"/", "src=\"http://www.sczg-l-tax.gov.cn/");
		content=content.replace("href=\"/", "href=\"http://www.sczg-l-tax.gov.cn/");*/
		
		/*眉山地税外网*/
		/*content=content.replace("src=\"/", "src=\"http://www.scms-l-tax.gov.cn/");
		content=content.replace("href=\"/", "href=\"http://www.scms-l-tax.gov.cn/");*/
		//content=content.replace("http://www.ms.gov.cn/", "");
		
		/*资阳地税外网*/
		//content=content.replace("src=\"/", "src=\"http://www.sczy-l-tax.gov.cn/");
		//content=content.replace("href=\"/", "href=\"http://www.sczy-l-tax.gov.cn/");
		
		//巴中地税内网
		//content=content.replace("src=\"/", "src=\"http://www.bztax.gov.cn/");
		
		/*成都地税外网*/
		/*content=content.replace("src=\"/", "src=\"http://125.71.28.2/");
		content=content.replace("href=\"/", "href=\"http://125.71.28.2/");*/
		
		/*德阳地税内网*/
		//content=content.replace("src=\"/", "src=\"http://155.16.211.51:8088/");
		//content=content.replace("href=\"/", "href=\"http://155.16.211.51:8088/");
		
		/*德阳地税外网*/
		/*content=content.replace("src=\"/", "src=\"http://www.dyltax.gov.cn/");
		content=content.replace("href=\"/", "href=\"http://www.dyltax.gov.cn/");*/
		
		/*遂宁地税外网*/
		/*content=content.replace("src=\"/", "src=\"http://sndfswj.suining.gov.cn/");
		content=content.replace("href=\"/", "href=\"http://sndfswj.suining.gov.cn/");*/
		
		/*宜宾地税外网*/
		/*content=content.replace("src=\"/", "src=\"http://ds.ybxww.com/");
		content=content.replace("href=\"/", "href=\"http://ds.ybxww.com/");*/
		
		/*广安地税外网*/
		//content=content.replace("src=\"/", "src=\"http://www.scga-l-tax.gov.cn/");
		//content=content.replace("href=\"/", "href=\"http://www.scga-l-tax.gov.cn/");
		
		/*攀枝花地税外网*/
		//content=content.replace("src=\"/", "src=\"http://www.pzhdsj.gov.cn/");
		//content=content.replace("href=\"/", "href=\"http://www.pzhdsj.gov.cn/");
		
		/*甘孜州地税外网*/
		//content=content.replace("src=\"/", "src=\"http://www.sc-l-tax.gov.cn:81/");
		//content=content.replace("href=\"/", "href=\"http://www.sc-l-tax.gov.cn:81/");
		
		/*甘孜州地税内网*/
		//content=content.replace("src=\"/", "src=\"http://155.16.161.140/");
		
		/*initImages(content,1);
		init_link(content);*/
		//initImages(content,1);
		
		/*德阳地税外网数据图片路径替换*/
		//content=content.replace("src=\"/", "src=\"http://155.16.211.51:8088/");
		
		/*甘孜州地税外网图片路径替换*/
		//content=content.replace("src=\"http://www.sc-l-tax.gov.cn:81/", "src=\"http://155.16.211.51:8088/");
		
		/*乐山外网数据图片路径替换*/
		/*content=content.replace("src=\"http://www.lsdsj.gov.cn/", "src=\"http://155.16.211.51:8088/");
		content=content.replace("src=\"/", "src=\"http://155.16.211.51:8088/");//http://www.leshan.gov.cn/
		//content=content.replace("src=\"http://www.leshan.gov.cn/", "src=\"http://155.16.211.51:8088/");
		content=content.replace("href=\"/", "href=\"http://www.lsdsj.gov.cn/");*/
		
		/*达州外网数据图片路径替换*/
		/*content=content.replace("src=\"http://www.scdz-l-tax.gov.cn/", "src=\"http://155.16.211.51:8088/");
		content=content.replace("src=\"/", "src=\"http://155.16.211.51:8088/");//http://www.leshan.gov.cn/
		//content=content.replace("src=\"http://www.leshan.gov.cn/", "src=\"http://155.16.211.51:8088/");
		content=content.replace("href=\"/", "href=\"http://www.scdz-l-tax.gov.cn/");*/
		
		/*凉山外网数据图片路径替换*/
		/*content=content.replace("src=\"http://www.lszds.com/", "src=\"http://155.16.211.51:8088/");
		content=content.replace("src=\"http://localhost/dishui/", "http://155.16.211.51:8088/");
		content=content.replace("src=\"http://125.68.56.114/", "src=\"http://155.16.211.51:8088/");
		content=content.replace("src=\"/", "src=\"http://155.16.211.51:8088/");//http://www.leshan.gov.cn/
		//content=content.replace("src=\"http://www.leshan.gov.cn/", "src=\"http://155.16.211.51:8088/");
		content=content.replace("href=\"/", "href=\"http://www.lszds.com/");*/
		
		/*达州地税内网数据图片路径替换*/
		/*content=content.replace("src=\"http://www.lszds.com/", "src=\"http://155.16.211.51:8088/");
		content=content.replace("src=\"http://localhost/dishui/", "http://155.16.211.51:8088/");
		content=content.replace("src=\"/", "src=\"http://155.76.16.9/");
		content=content.replace("href=\"/", "href=\"http://155.76.16.9/");
		content=content.replace("[InstallDir_ChannelDir]{$UploadDir}", "http://155.76.16.9/Article/UploadFiles");*/
		
		/*乐山地税内网图片路径替换*/
		//content=content.replace("src=\"/", "http://155.60.16.88/");
		
		/*自贡地税外网图片路径替换*/
		/*content=content.replace("src=\"/", "src=\"http://155.16.211.51:8088/");
		content=content.replace("src=\"http://www.sczg-l-tax.gov.cn/", "src=\"http://155.16.211.51:8088/");
		content=content.replace("href=\"/", "href=\"http://155.16.211.51:8088/");
		content=content.replace("href=\"http://www.sczg-l-tax.gov.cn/", "href=\"http://155.16.211.51:8088/");*/
		
		/*眉山地税外网图片路径替换*/
		/*content=content.replace("src=\"http://www.scms-l-tax.gov.cn/", "src=\"http://155.16.211.51:8088/");*/
		
		/*资阳地税外网图片路径替换*/
		//content=content.replace("src=\"http://www.sczy-l-tax.gov.cn/", "src=\"http://155.16.211.51:8088/");
		
		/*内江地税外网*/
		/*content=content.replace("src=\"http://www.scnj-l-tax.gov.cn/", "src=\"http://155.16.211.51:8088/");*/
		
		/*成都外网图片路径替换*/
		/*content=content.replace("src=\"/", "src=\"http://155.16.211.51:8088/");
		content=content.replace("href=\"/", "href=\"http://155.16.211.51:8088/");
		content=content.replace("src=\"http://125.71.28.2/", "src=\"http://155.16.211.51:8088/");
		content=content.replace("href=\"http://125.71.28.2/", "href=\"http://155.16.211.51:8088/");*/
		
		/*遂宁地税外网图片路径替换*/
		/*content=content.replace("src=\"http://221.237.222.8:101/", "src=\"http://155.16.211.51:8088/");
		content=content.replace("src=\"http://221.237.222.8:102/", "src=\"http://155.16.211.51:8088/");
		content=content.replace("src=\"http://sndfswj.suining.gov.cn/", "src=\"http://155.16.211.51:8088/");
		content=content.replace("src=\"http://scsn.gov.cn/", "src=\"http://155.16.211.51:8088/");*/
		
		/*宜宾地税外网图片路径替换*/
		//content=content.replace("src=\"http://ds.ybxww.com/UploadFiles", "src=\"http://155.16.211.51:8088/UpLoadFiles");
		
		/*成都地税外网图片路径替换*/
		//content=content.replace("src=\"http://www.cdlocaltax.chengdu.gov.cn", "src=\"http://155.16.211.51:8088");
		
		//巴中地税内网
		//content=content.replace("src=\"http://www.bztax.gov.cn/", "src=\"http://155.16.211.51:8088/");
		
		/*广安地税外网*/
		//content=content.replace("src=\"http://www.scga-l-tax.gov.cn/", "src=\"http://155.16.211.51:8088/");
		//content=content.replace("href=\"/", "href=\"http://www.scga-l-tax.gov.cn/");
		
		/*攀枝花地税外网*/
		//content=content.replace("src=\"http://www.pzhdsj.gov.cn/", "src=\"http://155.16.211.51:8088/");
		//content=content.replace("href=\"/", "href=\"http://www.pzhdsj.gov.cn/");
		
		
		/*处理*/
		
		return content;
	}
	
	/*处理正文中的图片*/
	private static boolean initImages(String content,int flag){
		/*简易黑名单*/
		List<String> imageList=new ArrayList<String>();
		/*imageList.add("http://24.20.21.130");
		imageList.add("http://155.20.45.23");*/
		
		/*简易白名单*/
		List<String> imageWhiteList=new ArrayList<String>();
		//imageWhiteList.add("http://ds.ybxww.com");
		//imageWhiteList.add("http://www.sc-l-tax.gov.cn:81");
		//imageWhiteList.add("http://www.gatv.com.cn");
		imageWhiteList.add("http://www.sczy-l-tax.gov.cn");
		
		boolean imageBackList_flag=false;
		boolean imageWhiteList_flag=false;
		
		htmlRegexpUtil=new HtmlRegexpUtil();
		dimages=new ArrayList<String>();
		List<String> mmStr=htmlRegexpUtil.replaceHtmlTag3(content, "img", "src", "", "");
		for(String string:mmStr){
			System.out.println("图片路径："+string);
			dimages.add(string);
		}
		for(String dString:dimages){
			
			/*获取原始的图片路径*/
			String originalImage=dString;
			
			/*遍历白名单*/
			for(String image:imageWhiteList){
				dString=dString.replace(image, "");
			}
			
			/*遍历黑名单*/
			for(String images:imageList){
				dString=dString.replace(images, "");
			}
			String[] ss=dString.split("/");
			String tq=dString;
			tq=ss[ss.length-1];
			String fileString = null;
			StringBuffer tempBuffer=new StringBuffer();
			for(int i=0;i<ss.length-1;i++){
				if(i==0){
					tempBuffer.append(ss[i]);
				}else{
					tempBuffer.append("/"+ss[i]);
				}
			}
			fileString=tempBuffer.toString();
			/*if(flag==1){
				fileString=new StringBuffer().append(ss[0]+"/"+ss[1]+"/"+ss[2]+"/"+ss[3]).toString();
			}
			if(flag==0){
				fileString=new StringBuffer().append(ss[0]+"/"+ss[1]+"/"+ss[2]+"/"+ss[3]+"/"+ss[4]).toString();
			}*/
			
			/*遍历黑名单*/
			for(String images:imageList){
				if(originalImage.indexOf(images)!=-1){
					//如果能找到，那么立即拉黑。
					imageBackList_flag=true;
					System.out.println("黑名单内容："+images);
				}
			}
			
			/*判断其是否是黑名单成员*/
			if(imageBackList_flag==false){
				/*try {
					dString=URLEncoder.encode(dString,"gbk");
				} catch (UnsupportedEncodingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}*/
				String[] myTempPath=dString.split("/");
				StringBuffer tempFilePath=new StringBuffer();
				for(int i=0;i<myTempPath.length-1;i++){
					if(i<=myTempPath.length-3){
						tempFilePath.append(myTempPath[i]+"/");
					}else{
						tempFilePath.append(myTempPath[i]);
					}
				}
				String tempDString=tempFilePath.toString();
				String endDString=myTempPath[myTempPath.length-1];
				try {
					endDString=URLEncoder.encode(endDString, "UTF-8");
				} catch (UnsupportedEncodingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				if(endDString.indexOf("+")>-1){
					endDString=endDString.replace("+", " ");
				}
				
				tempDString=tempDString+"/"+endDString;
				
				//System.out.println(tempFilePath.toString());
				System.out.println(myTempPath[myTempPath.length-1]);
				
				dString="http://www.sczy-l-tax.gov.cn"+tempDString;

				RemoteFileUtil.getRemoteFile(dString, tq,flag,fileString);
				System.out.println("处理后的图片路径："+dString+",::"+tq+";;;"+fileString);
			}else{
				System.out.println("图片处理，已进入过滤黑名单....");
			}
			
		}
		return true;
	}
	
	/*抓取正文中的超链接*/
	private static boolean init_link(String content){
		/*简易黑名单*/
		List<String> linkList=new ArrayList<String>();
		/*linkList.add("http://125.64.140.2:7075");
		linkList.add("http://125.64.140.3:9004");
		linkList.add("http://www.ybxww.com");*/
		
		/*简易白名单*/
		List<String> linkWhiteList=new ArrayList<String>();
		//linkWhiteList.add("http://www.sc-l-tax.gov.cn:81/");
		linkWhiteList.add("http://www.sczy-l-tax.gov.cn/");
		
		/*黑名单标识*/
		boolean linkList_flag=false;
		
		/*白名单标识*/
		boolean linkWhiteList_flag=false;
		
		htmlRegexpUtil=new HtmlRegexpUtil();
		dlinks=new ArrayList<String>();
		List<String> mmStr=htmlRegexpUtil.replaceHtmlTag3(content, "a", "href", "", "");
		for(String string:mmStr){
			System.out.println("连接路径："+string);
			String temp=string.replace(".", ",");
			String[] tempStr=temp.split(",");
			System.out.println("当前连接类型："+tempStr[tempStr.length-1]);
			if(tempStr[tempStr.length-1].equalsIgnoreCase("html")){
				System.out.println("已忽略捕捉后缀："+tempStr[tempStr.length-1]+"文件！");
			}else if(tempStr[tempStr.length-1].equalsIgnoreCase("htm")){
				System.out.println("已忽略捕捉后缀："+tempStr[tempStr.length-1]+"文件！");
			}else if(tempStr[tempStr.length-1].equalsIgnoreCase("jsp")){
				System.out.println("已忽略捕捉后缀："+tempStr[tempStr.length-1]+"文件！");
			}else if(tempStr[tempStr.length-1].equalsIgnoreCase("asp")){
				System.out.println("已忽略捕捉后缀："+tempStr[tempStr.length-1]+"文件！");
			}else if(tempStr[tempStr.length-1].equalsIgnoreCase("php")){
				System.out.println("已忽略捕捉后缀："+tempStr[tempStr.length-1]+"文件！");
			}else if(tempStr[tempStr.length-1].equalsIgnoreCase("xhtml")){
				System.out.println("已忽略捕捉后缀："+tempStr[tempStr.length-1]+"文件！");
			}else if(tempStr[tempStr.length-1].equalsIgnoreCase("shtml")){
				System.out.println("已忽略捕捉后缀："+tempStr[tempStr.length-1]+"文件！");
			}else if(tempStr[tempStr.length-1].equalsIgnoreCase("action")){
				System.out.println("已忽略捕捉后缀："+tempStr[tempStr.length-1]+"文件！");
			}else if(tempStr[tempStr.length-1].equalsIgnoreCase("do")){
				System.out.println("已忽略捕捉后缀："+tempStr[tempStr.length-1]+"文件！");
			}else{
				dlinks.add(string);
			}
		}
		
		System.out.println("/**路径处理**/");
		for(String mylink:dlinks){
			String currentLink=mylink;
			//System.out.println("我的连接路径："+mylink);
			/*遍历白名单*/
			for(String link:linkWhiteList){
				mylink=mylink.replace(link,"");
			}
			
			/*遍历黑名单*/
			for(String link:linkList){
				mylink=mylink.replace(link,"");
			}
			mylink=mylink.replace("&ldquo;", "“");
			mylink=mylink.replace("&rdquo;", "”");
			System.out.println("处理后的连接路径："+mylink);
			String[] templink=mylink.split("/");
			System.out.println("处理后连接数组长度："+templink.length);
			StringBuffer mBuffer=new StringBuffer();
			for(int i=0;i<=templink.length-2;i++){
				if(i==templink.length-2){
					mBuffer.append(templink[i]);
				}else{
					mBuffer.append(templink[i]+"/");
				}
				
			}
			System.out.println("组合后的连接路径："+mBuffer.toString());
			/*组合目录*/
			String zhmu=mBuffer.toString();
			/*具体文件*/
			String fileName=templink[templink.length-1];
			/*连接组合*/
			/*try {
				mylink=URLEncoder.encode(mylink,"GB2312");
			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}*/
			
			String ljzh=mylink;
			
			/*遍历黑名单*/
			for(String links:linkList){
				if(currentLink.indexOf(links)!=-1){
					//如果能找到，那么立即拉黑。
					linkList_flag=true;
				}
			}
			
			/*判断其是否是黑名单成员*/
			if(linkList_flag==false){
				//ljzh="http://ds.ybxww.com/"+mylink;
				ljzh=currentLink;
				System.out.println("最后的连接路径："+mylink);
				System.out.println("传参的连接路径："+ljzh);
				System.out.println("传参的文件名："+fileName);
				System.out.println("传参的主目录："+zhmu);
				RemoteFileUtil.getRemoteFile(ljzh, fileName, 0, zhmu);
			}else{
				System.out.println("连接处理，已进入过滤黑名单...");
			}
		}
		
		return true;
	}
	
}
