package cn.gov.scds.util;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import cn.gov.scds.model.HtmlTag;

/*
 * 文章正文替换工具
 * */
public class DataContentUtil {
	
	private static MediaUtil mediaUtil;
	private static HtmlTagCharacterUtil htmlTagCharacterUtil;
	private static HtmlRegexpUtil htmlRegexpUtil;
	public static String baseStyle;
	public static List<HtmlTag> htmlTags;
	
	 /*
	  * 替换字符串
	  * */
     public static String replaceContentCharacter(String content){
    	 
    	 mediaUtil=new MediaUtil();
    	 try {
    		 htmlTagCharacterUtil=new HtmlTagCharacterUtil();
    		 htmlRegexpUtil=new HtmlRegexpUtil();
    		 
    		 /*中文里全角转换为半角*/
    		 content=full2HalfChange(content);

        	 /*替换文档特殊字符串*/
        	 System.out.println("/**********进入替换文档特殊字符串************/");
        	 content=mediaUtil.replaceMediaCharacter(content);
        	 
        	 /*预先处理标签*/
        	 content=content.replace("div", "p");
        	 /*content=content.replace("<br/>", "</p><p>");
        	 content=content.replace("<br />", "</p><p>");*/
        	 /*处理Html样式*/
        	 content=filterHtmlStyle(content);
        	 /*最后处理*/
        	 content=content.replace("<img", "<br/><img style=\"display:block;margin-left:auto;margin-right:auto;width:70%;height:100%;\" ");
        	 content=content.replace("<table", "<table style=\"display:block;margin-left:auto;margin-right:auto;\" ");
        	 //content=content.replaceAll("<br\\s{1,10}/>", "<br/>");
        	 content=content.replace("<br/>", "<br/>&nbsp;&nbsp;&nbsp;&nbsp;");
        	 content=content.replace("<br />", "<br/>&nbsp;&nbsp;&nbsp;&nbsp;");
        	 content=content.replaceAll("<p><br/>(&nbsp;){1,10}", "<p>");
        	 //content=content.replaceAll("<p>", "<p>&nbsp;&nbsp;&nbsp;&nbsp;");
        	 content=content.replaceAll("<br/>(&nbsp;){1,10}<br/>", "<br/>&nbsp;&nbsp;&nbsp;&nbsp;");
        	 content=content.replaceAll("<br/>(&nbsp;){5,10}", "<br/>&nbsp;&nbsp;&nbsp;&nbsp;");
        	 
        	 //System.out.println("最终正文处理结果："+content);
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("错误了============");
		}
    	 //baseStyle="<style type=\"text/css\">"+".TRS_Editor P{line-height:2;font-family:宋体;font-size:14px;}.TRS_Editor DIV{line-height:2;font-family:宋体;font-size:14px;}.TRS_Editor TD{line-height:2;font-family:宋体;font-size:14px;}.TRS_Editor TH{line-height:2;font-family:宋体;font-size:14px;}.TRS_Editor SPAN{line-height:2;font-family:宋体;font-size:14px;}.TRS_Editor FONT{line-height:2;font-family:宋体;font-size:14px;}.TRS_Editor UL{line-height:2;font-family:宋体;font-size:14px;}.TRS_Editor LI{line-height:2;font-family:宋体;font-size:14px;}.TRS_Editor A{line-height:2;font-family:宋体;font-size:14px;}</style>";
    	 baseStyle="<style type=\"text/css\">"+
".TRS_Editor P{line-height:2;font-family:宋体;font-size:12pt;}.TRS_Editor DIV{line-height:2;font-family:宋体;font-size:12pt;}.TRS_Editor TD{line-height:2;font-family:宋体;font-size:12pt;}.TRS_Editor TH{line-height:2;font-family:宋体;font-size:12pt;}.TRS_Editor SPAN{line-height:2;font-family:宋体;font-size:12pt;}.TRS_Editor FONT{line-height:2;font-family:宋体;font-size:12pt;}.TRS_Editor UL{line-height:2;font-family:宋体;font-size:12pt;}.TRS_Editor LI{line-height:2;font-family:宋体;font-size:12pt;}.TRS_Editor A{line-height:2;font-family:宋体;font-size:12pt;}</style>";
    	 //content=baseStyle+content;
    	 
    	 return content;
     }
     
     /*初始化要保留的html标签*/
     public static boolean initHtmlTag(){
    	 htmlTags=new ArrayList<HtmlTag>();
    	 htmlTags.add(new HtmlTag(0, "<p", "#d", "</p>", "$d"));
    	 htmlTags.add(new HtmlTag(0, "<table", "#t", "</table>", "$t"));
    	 htmlTags.add(new HtmlTag(0, "<tr", "#r", "</tr>", "$r"));
    	 htmlTags.add(new HtmlTag(0, "<td", "#g", "</td>", "$g"));
    	 htmlTags.add(new HtmlTag(0, "<a", "#a", "</a>", "$a"));
    	 htmlTags.add(new HtmlTag(1, "<img", "#i", "", ""));
    	 htmlTags.add(new HtmlTag(1, "<br", "#br", "", ""));
    	 
    	 return true;
     }
     
     /*去掉所有类选择器和style样式*/
     public static String filterHtmlStyle(String content){
    	 initHtmlTag();
    	 for(HtmlTag htmlTag:htmlTags){
    		 if(htmlTag.getSingleFlag()==0){
    			 content=content.replace(htmlTag.getHtmlStartTag(), htmlTag.getFlterStartTag());
        		 content=content.replace(htmlTag.getHtmlEndTag(), htmlTag.getFlterEndTag()); 
    		 }else{
    			 content=content.replace(htmlTag.getHtmlStartTag(), htmlTag.getFlterStartTag());
    		 }
    	 }
    	 
    	 content=htmlRegexpUtil.filterHtml(content);
    	 
    	 for(HtmlTag htmlTag:htmlTags){
    		 //System.out.println("这段不运行吗？");
    		 if(htmlTag.getSingleFlag()==0){
    			 content=content.replace(htmlTag.getFlterStartTag(), htmlTag.getHtmlStartTag());
        		 content=content.replace(htmlTag.getFlterEndTag(), htmlTag.getHtmlEndTag());
    		 }else{
    			 content=content.replace(htmlTag.getFlterStartTag(), htmlTag.getHtmlStartTag());
    		 }
    	 }
    	 
    	 for(HtmlTag htmlTag:htmlTags){
    		 //System.out.println("这段不运行吗？");
    		 //content=content.replaceAll(htmlTag.getHtmlStartTag()+".+\"", htmlTag.getHtmlStartTag());
    		 //content=htmlRegexpUtil.replaceHtmlTag2(content, htmlTag.getHtmlStartTag().replace("<", ""), "class", "", "");
    		 //content=htmlRegexpUtil.replaceHtmlTag2(content, htmlTag.getHtmlStartTag().replace("<", ""), "style", "", "");
    		 
    		 if(!htmlTag.getHtmlStartTag().equalsIgnoreCase("<img")){
    			 //content=htmlRegexpUtil.replaceHtmlTag2(content, htmlTag.getHtmlStartTag().replace("<", ""), "align", "", "");
    			 content=content.replaceAll("class"+"=\"([^\"]+)\"", "");
    			 content=content.replaceAll("style"+"=\"([^\"]+)\"", "");
    			 content=content.replaceAll("align"+"=\"([^\"]+)\"", "");
    		 }
    	 }
    	 content=content.replaceAll("<p\\s{1,5}>", "<p>");
    	 content=content.replaceAll("<p>(&nbsp;){1,50}</p>", "");
    	 //content=content.replaceAll("^\\s*\\n", "");
    	 content=content.replaceAll("<p>(&nbsp;){1,100}(&nbsp;)", "<p>");
    	 content=content.replace("\n", "");
    	 //content=content.replace("\n\\s", "");
    	 content=content.replaceAll("^\\n|\\r(&nbsp;){1,100}(&nbsp;)", "");
    	 content=content.replace("<p>", "<p style=\"text-indent: 2em;line-height:2em;\">");
    	 
    	 
    	 return content;
     }
     
     /*中文字体全角转换为半角*/
     public static final String full2HalfChange(String QJstr)  
             throws UnsupportedEncodingException {
         StringBuffer outStrBuf = new StringBuffer("");
         String Tstr = "";
         byte[] b = null;
         for (int i = 0; i < QJstr.length(); i++) {
             Tstr = QJstr.substring(i, i + 1);
             // 全角空格转换成半角空格
             if (Tstr.equals("　")) {
                 outStrBuf.append(" ");
                 continue;
             }
             b = Tstr.getBytes("unicode");
             // 得到 unicode 字节数据
             if (b[2] == -1) {
                 // 表示全角？
                 b[3] = (byte) (b[3] + 32);
                 b[2] = 0;
                 outStrBuf.append(new String(b, "unicode"));
             } else {
                 outStrBuf.append(Tstr);
             }
         } // end for.
         return outStrBuf.toString();
     }
     
     /*中文字体半角转换为全角*/
     public static final String half2Fullchange(String QJstr)  
             throws UnsupportedEncodingException{
         StringBuffer outStrBuf = new StringBuffer("");
         String Tstr = "";
         byte[] b = null;
         for (int i = 0; i < QJstr.length(); i++){
             Tstr = QJstr.substring(i, i + 1);
             if (Tstr.equals(" ")) {
                 // 半角空格
                 outStrBuf.append(Tstr);
                 continue;
             }
             b = Tstr.getBytes("unicode");
             if (b[2] == 0) {
                 // 半角?
                 b[3] = (byte) (b[3] - 32);
                 b[2] = -1;
                 outStrBuf.append(new String(b, "unicode"));
             } else {
                 outStrBuf.append(Tstr);
             }
         }
         return outStrBuf.toString();
     }
}
