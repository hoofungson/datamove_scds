package cn.gov.scds.util;

import java.util.ArrayList;
import java.util.List;

public class MenuTreeUtilTest {
	
	
  private static void run(){
	  List<TreeNode> treeNodes = new ArrayList<TreeNode>();
	  treeNodes.add(new TreeNode(56, 56));
      treeNodes.add(new TreeNode(69, 56));
      ManyNodeTree tree = new ManyNodeTree();
      System.out.println(tree.iteratorTree(tree.createTree(treeNodes).getRoot()));
  }
	
  public static void main(String[] args) {
	 run();
  }
}
