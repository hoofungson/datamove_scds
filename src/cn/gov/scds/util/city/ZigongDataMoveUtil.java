package cn.gov.scds.util.city;

import java.sql.Connection;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.trs.web2frame.domain.WDocumentMgr;
import com.trs.web2frame.entity.WDocument;

import cn.gov.scds.entity.Content;
import cn.gov.scds.entity.JEmialBox;
import cn.gov.scds.entity.Poll;
import cn.gov.scds.jdbc.DBConnection;
import cn.gov.scds.jdbc.DBInteractionConnection;
import cn.gov.scds.jdbc.DBUitl;
import cn.gov.scds.util.AppContentUtil;
import cn.gov.scds.util.DataContentUtil;
import cn.gov.scds.util.HtmlRegexpUtil;
import oracle.sql.CLOB;
import oracle.sql.DATE;

public class ZigongDataMoveUtil extends DataMoveUtilBase{

	/*
	 * 自贡地税数据迁移工具
	 * */
	public static Connection connect=null;
	public static Statement statement=null;
	public static ResultSet resultSet=null;
	private static boolean appendixFlag=false;
	int y_channel;
	static int x_channel;
	static String city_name;
	CLOB clob = null;
	private static int dataId=7618;
	private static int recordId=1900;
	
	public void runMove() {
		// TODO Auto-generated method stub
		//moveDocument();
		//wai_moveDocument();
		readJZXXFromJDBC();
	}

	/*
	 * 自贡地税内网
	 * */
	
	/*
	 * 读取新闻文档
	 * */
	private void moveDocument(){
		int[] y_channel={32};
		x_channel=4227;
		city_name="四川省自贡市地方税务局";
		
		String sqlStr="select * from bx_Article where ClassId in(";
		StringBuffer sqlBuffer=new StringBuffer();
		
		for(int i=0;i<y_channel.length;i++){
			if(i==0){
				sqlBuffer.append(y_channel[i]);
			}else{
				sqlBuffer.append(","+y_channel[i]);
			}
			
		}
		sqlStr=sqlStr+sqlBuffer.toString()+") order by InNo asc";
		
		//String sqlStr="select * from mynews where NEWSTYPE like '图片新闻'";
		
		System.out.println("打印SQL语句："+sqlStr);
		
		int countIndex=1;
        statement=DBUitl.getStatement(DBConnection.getConnection());
		ResultSet resultSet=DBUitl.getResultSet(statement,sqlStr);
		try {
			while (resultSet.next()) {
				Content content=new Content();
				content.setContentID(resultSet.getInt("Id"));
				if(resultSet.getString("U1")==null){
					content.setTitle_1("暂无标题");
				}else{
					content.setTitle_1(resultSet.getString("U1"));
				}
				System.out.println("源文档编号："+content.getContentID());
				
				String songContent=resultSet.getString("U5");
				//处理clob字段
				/*String songContent="";
				clob = (oracle.sql.CLOB) resultSet.getClob("CONTENT_2");
				if(clob!=null){
					BufferedReader in = new BufferedReader(clob.getCharacterStream());
					StringWriter out = new StringWriter();
					int c=0;
					try {
						while ((c = in.read()) != -1) {
						     out.write(c);
						}
						songContent=out.toString();
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						//e1.printStackTrace();
						System.out.println(".....+++此处文档异常...=======");
						songContent="暂无内容！";
					}
					
					
					try {
						out.close();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						//e.printStackTrace();
						System.out.println(".....+++此处文档输出流关闭异常...=======");
					}
					try {
						in.close();
					} catch (IOException e) {
						// TODO Auto-generated catch block
					    //e.printStackTrace();
						System.out.println(".....+++此处文档输入流关闭异常...=======");
					}
				}else{
					songContent="暂无内容";
				}*/
				
				if(songContent==null){
					content.setItem_text("");
				}else{
					System.out.println("内容："+songContent);
				    String itext = null;
					//itext = ClobToString(clob);
					itext=songContent;
					String item_text=DataContentUtil.replaceContentCharacter(itext);
					//String item_text="";
					/*String picInfoString=resultSet.getString("U3");
					if(picInfoString==null){
						content.setItem_text(item_text);
					}else if(picInfoString.isEmpty()){
						content.setItem_text(item_text);
					}else{
						picInfoString="<img src=\"http://155.24.16.2"+picInfoString+"\"/>";
						content.setItem_text(picInfoString+item_text);
					}*/
					content.setItem_text(item_text);
					
				}
				content.setWrite_date(resultSet.getTimestamp("Dtime"));
				//content.setWrite_user(resultSet.getInt("WRITE_USER"));
				content.setAudit_date(resultSet.getTimestamp("Etime"));
				//content.setAudit_user(resultSet.getInt("AUDIT_USER"));
				content.setIssue_date(resultSet.getTimestamp("Dtime"));
				//content.setIssue_user(resultSet.getInt("ISSUE_USER"));
				int showNumber=resultSet.getInt("Views");
				content.setShow_number(showNumber==0?0:showNumber);
				String contentFrom=resultSet.getString("Source_Text");
				content.setContent_from(contentFrom==null?city_name:contentFrom);
				//content.setDocchannel(readItemIterator(resultSet.getInt("ITEM_ID")));
				
				System.out.println("文档标题："+content.getTitle_1());
				System.out.println("WRITE_DATE："+content.getWrite_date());
				//System.out.println("WRITE_USER："+resultSet.getInt("WRITE_USER"));
				System.out.println("AUDIT_DATE："+content.getAudit_date());
				//System.out.println("AUDIT_USER："+resultSet.getInt("AUDIT_USER"));
				System.out.println("ISSUE_DATE："+content.getIssue_date());
				//System.out.println("ISSUE_USER："+resultSet.getInt("ISSUE_USER"));
				System.out.println("SHOW_NUMBER："+content.getShow_number());
				System.out.println("CONTENT_FROM："+content.getContent_from());
				
				//调用附件读取方法，一个文档读取一次数据附件。
				//connect=DBConnection.getConnection();
				//content.setContentFiles(readContentFile(connect,content.getContentID()));
				//System.out.println("附件个数："+content.getContentFiles().size());
				
				System.out.println("==============文档读取完毕,,,,,,,,,,,,");
				
				//调用WCM文档推送方法，将封装好后的文档推送到新数据库中。
				
				try {
				   //testSaveAll(content);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				System.out.println("当前处理记录是第："+countIndex);
				countIndex++;
			}
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			try {
				resultSet.close();
				statement.close();
				//connect.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		System.out.println("好无聊！");
	}
	
	/*外网部分*/
	/*
	 * 读取新闻文档
	 * */
	private void wai_moveDocument(){
		int[] y_channel={93};
		x_channel=2268;
		city_name="四川省自贡市地方税务局";
		
		String sqlStr="select * from News where fid in(";
		StringBuffer sqlBuffer=new StringBuffer();
		
		for(int i=0;i<y_channel.length;i++){
			if(i==0){
				sqlBuffer.append(y_channel[i]);
			}else{
				sqlBuffer.append(","+y_channel[i]);
			}
			
		}
		sqlStr=sqlStr+sqlBuffer.toString()+") order by shi asc";
		
		//String sqlStr="select * from mynews where NEWSTYPE like '图片新闻'";
		
		System.out.println("打印SQL语句："+sqlStr);
		
		int countIndex=1;
        statement=DBUitl.getStatement(DBConnection.getConnection());
		ResultSet resultSet=DBUitl.getResultSet(statement,sqlStr);
		try {
			while (resultSet.next()) {
				Content content=new Content();
				content.setContentID(resultSet.getInt("NewsId"));
				if(resultSet.getString("title")==null){
					content.setTitle_1("暂无标题");
				}else{
					content.setTitle_1(resultSet.getString("title"));
				}
				System.out.println("源文档编号："+content.getContentID());
				
				String songContent=resultSet.getString("content");
				//处理clob字段
				/*String songContent="";
				clob = (oracle.sql.CLOB) resultSet.getClob("CONTENT_2");
				if(clob!=null){
					BufferedReader in = new BufferedReader(clob.getCharacterStream());
					StringWriter out = new StringWriter();
					int c=0;
					try {
						while ((c = in.read()) != -1) {
						     out.write(c);
						}
						songContent=out.toString();
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						//e1.printStackTrace();
						System.out.println(".....+++此处文档异常...=======");
						songContent="暂无内容！";
					}
					
					
					try {
						out.close();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						//e.printStackTrace();
						System.out.println(".....+++此处文档输出流关闭异常...=======");
					}
					try {
						in.close();
					} catch (IOException e) {
						// TODO Auto-generated catch block
					    //e.printStackTrace();
						System.out.println(".....+++此处文档输入流关闭异常...=======");
					}
				}else{
					songContent="暂无内容";
				}*/
				
				if(songContent==null){
					content.setItem_text("");
				}else{
					System.out.println("内容："+songContent);
				    String itext = null;
					//itext = ClobToString(clob);
					itext=songContent;
					String item_text=DataContentUtil.replaceContentCharacter(itext);
					//String item_text="";
					/*String picInfoString=resultSet.getString("U3");
					if(picInfoString==null){
						content.setItem_text(item_text);
					}else if(picInfoString.isEmpty()){
						content.setItem_text(item_text);
					}else{
						picInfoString="<img src=\"http://155.24.16.2"+picInfoString+"\"/>";
						content.setItem_text(picInfoString+item_text);
					}*/
					content.setItem_text(item_text);
					
				}
				content.setWrite_date(resultSet.getTimestamp("shi"));
				//content.setWrite_user(resultSet.getInt("WRITE_USER"));
				content.setAudit_date(resultSet.getTimestamp("shi"));
				//content.setAudit_user(resultSet.getInt("AUDIT_USER"));
				content.setIssue_date(resultSet.getTimestamp("shi"));
				//content.setIssue_user(resultSet.getInt("ISSUE_USER"));
				int showNumber=resultSet.getInt("djs");
				content.setShow_number(showNumber==0?0:showNumber);
				String contentFrom=resultSet.getString("author");
				content.setContent_from(contentFrom==null?city_name:contentFrom);
				//content.setDocchannel(readItemIterator(resultSet.getInt("ITEM_ID")));
				
				System.out.println("文档标题："+content.getTitle_1());
				System.out.println("WRITE_DATE："+content.getWrite_date());
				//System.out.println("WRITE_USER："+resultSet.getInt("WRITE_USER"));
				System.out.println("AUDIT_DATE："+content.getAudit_date());
				//System.out.println("AUDIT_USER："+resultSet.getInt("AUDIT_USER"));
				System.out.println("ISSUE_DATE："+content.getIssue_date());
				//System.out.println("ISSUE_USER："+resultSet.getInt("ISSUE_USER"));
				System.out.println("SHOW_NUMBER："+content.getShow_number());
				System.out.println("CONTENT_FROM："+content.getContent_from());
				
				//调用附件读取方法，一个文档读取一次数据附件。
				//connect=DBConnection.getConnection();
				//content.setContentFiles(readContentFile(connect,content.getContentID()));
				//System.out.println("附件个数："+content.getContentFiles().size());
				
				System.out.println("==============文档读取完毕,,,,,,,,,,,,");
				
				//调用WCM文档推送方法，将封装好后的文档推送到新数据库中。
				
				try {
				   testSaveAll(content);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				System.out.println("当前处理记录是第："+countIndex);
				countIndex++;
			}
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			try {
				resultSet.close();
				statement.close();
				//connect.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		System.out.println("好无聊！");
	}
	
	/*
	 * 导入旧数据库中的局长信箱数据
	 * */
	private void readJZXXFromJDBC(){
		connect=DBConnection.getConnection();
		statement=DBUitl.getStatement(connect);
		
		Connection iconnection=DBInteractionConnection.getConnection();
		Statement iStatement=DBUitl.getStatement(iconnection);
		
		String sqlStr="select * from gestbook where classID=300 order by gestbookID";
		ResultSet resultSet=DBUitl.getResultSet(statement,sqlStr);
		try {
			while(resultSet.next()){
				 JEmialBox jBox=new JEmialBox();
				 jBox.setMailId(resultSet.getInt("gestbookID"));
				 jBox.setComemMails(resultSet.getString("name"));
				 jBox.setComeMailDate(resultSet.getTimestamp("shi"));
				 jBox.setMailContent(resultSet.getString("content"));
				 //jBox.setMailEmail(resultSet.getString("email"));
				 jBox.setMailTitle(resultSet.getString("title"));
				 Timestamp hfrq=resultSet.getTimestamp("ReplyShi");
				 if(hfrq!=null){
					 jBox.setReplyDate(hfrq);
				 }else{
					 jBox.setReplyDate(Timestamp.valueOf("2015-05-15 12:12:20"));
				 }
				 //jBox.setReplyDate(resultSet.getTimestamp("ReplyShi"));
				 jBox.setReplyIdea(resultSet.getString("Reply"));
				 
				 //打印出来
				 System.out.println("信件标题："+jBox.getMailTitle());
				 System.out.println("来信人："+jBox.getComemMails());
				 System.out.println("来信日期："+jBox.getComeMailDate());
				 //System.out.println("来信邮箱："+jBox.getMailEmail());
				 //System.out.println("来信内容："+jBox.getMailContent().length());
				 System.out.println("回复日期："+jBox.getReplyDate());
				 //System.out.println("来信邮箱："+jBox.getMailEmail());
				 //System.out.println("回复内容："+jBox.getReplyIdea());
				 System.out.println("==========分割线============");
				 
				 writeJZXXByJDBC(jBox,iStatement);
				}
			    iStatement.close();
			    iconnection.close();
			}catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		
		
		try {
			statement.close();
			connect.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void writeJZXXByJDBC(JEmialBox jBox,Statement iStatement){
		String sqlStr="";
		if(jBox.getComeMailDate()!=null){
			sqlStr="insert into MAIL (mailid,comemmails,comemaildate,mailcontent,mailtitle,replydate,replyidea,areaclass,leardclass) values ("+dataId+",'"+jBox.getComemMails()+"',to_date('"+jBox.getComeMailDate().toString().substring(0, 19)+"','YYYY-MM-DD HH24:MI:SS'),'"+jBox.getMailContent()+"','"+jBox.getMailTitle()+"',to_date('"+jBox.getReplyDate().toString().substring(0, 19)+"','YYYY-MM-DD HH24:MI:SS'),'"+jBox.getReplyIdea()+"',3,2)";
		}else{
			sqlStr="insert into MAIL (mailid,comemmails,mailcontent,mailemail,areaclass)values("+dataId+",'"+jBox.getComemMails()+"','"+jBox.getMailContent()+"','"+jBox.getMailEmail()+"',11)";
		}
		
		try {
			iStatement.executeUpdate(sqlStr);
			System.out.println("=========数据导入成功.......");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		dataId++;
	}
	
	/*
	 * 读取调查模块中的调查项目表
	 * */
	private void readDc_projectByJDBC(){
		connect=DBConnection.getConnection();
		statement=DBUitl.getStatement(connect);
		
		Connection iconnection=DBInteractionConnection.getConnection();
		Statement iStatement=DBUitl.getStatement(iconnection);
		
		String sqlStr="select * from DC_PROJECT where ZF=0 order by SHOW_ORDER";
		ResultSet resultSet=DBUitl.getResultSet(statement,sqlStr);
		try {
			while(resultSet.next()){
				Poll poll=new Poll();
				poll.setPollId(resultSet.getInt("PROJECT_ID")+1900);
				poll.setPollTitle(resultSet.getString("PROJECT_NAME"));
				poll.setPollMessage(resultSet.getString("PROJECT_DESC"));
				poll.setChannelId(7);
				
				System.out.println("调查项目编号："+poll.getPollId());
				System.out.println("调查项目标题："+poll.getPollTitle());
				System.out.println("调查项目描述："+poll.getPollMessage());
				System.out.println("===========分割线========");
				
				writeDc_projectByJDBC(poll,iStatement);
			  }
			}catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	}
	
	/*
	 * 写入调查模块的调查项目表
	 * */
	private void writeDc_projectByJDBC(Poll poll,Statement iStatement){
		String sqlStr="insert into Poll (pollid,polltitle,pollmessage,channelid) values ("+poll.getPollId()+",'"+poll.getPollTitle()+"','"+poll.getPollMessage()+"',"+poll.getChannelId()+")";
		try {
			iStatement.executeUpdate(sqlStr);
			System.out.println("=========数据导入成功.......");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		recordId++;
	}
	
	/*
	 * 根据旧数据库读取的文档，写入到新的数据库。
	 * */
	/**
	 * 保存文档及其所有相关项，包括引用栏目，附件，相关文档
	 * 
	 * @param _oDocument
	 * @throws Exception 
	 */
	private static void saveAll(WDocument _oDocument) throws Exception {
		long recordId=WDocumentMgr.save(_oDocument);
		WDocumentMgr.quoteTo(_oDocument);
		
		if(appendixFlag==true){
			try {
				WDocumentMgr.saveAppendixs(_oDocument);
				//WDocumentMgr.saveRelations(_oDocument);
			} catch (Exception e) {
				// TODO: handle exception
				System.out.println("妈的，附件后缀格式又不支持了！");
			}
			
		}
		//WDocumentMgr.saveAppendixs(_oDocument);
		//WDocumentMgr.saveRelations(_oDocument);
		
		
	}
	
	/** 附件类型标识：文档附件 */
    public final static int FLAG_DOCAPD = 10; // 文件:Document Appendix

    /** 附件类型标识：文档图片 */
    public final static int FLAG_DOCPIC = 20; // 图片:Document Picture

    /** 附件类型标识：HTML */
    public final static int FLAG_HTMLPIC = 30; // HTML图片:Html Picture

    /** 附件类型标识：链接附件 */
    public final static int FLAG_LINK = 40; // 链接:Link
    
    
	public static void testSaveAll(Content content) throws Exception {
		   System.out.println("=========进入文档写方法======");
		  WDocument oDocument = new WDocument();
		
		  oDocument.setFieldValue("ChannelId", x_channel);
		  oDocument.setFieldValue("ObjectId", new Integer(0));
		
		  oDocument.setFieldValue("DocTitle", content.getTitle_1());
		  oDocument.setFieldValue("DOCTYPE", new Integer(20));
		  System.out.println("content.getWrite_date()"+content.getWrite_date());
		  oDocument.setFieldValue("CRTIME", content.getWrite_date());
		  oDocument.setFieldValue("DOCRELTIME", content.getWrite_date());
		  String htmlContent=new HtmlRegexpUtil().fiterHtmlTag(content.getItem_text(), "a");
		  htmlContent=htmlContent.replace("</a>", "");
		  htmlContent=new HtmlRegexpUtil().fiterHtmlTag(htmlContent, "A");
		  htmlContent=htmlContent.replace("</A>", "");
		  oDocument.setFieldValue("DOCCONTENT", AppContentUtil.replaceSpecialCharacter(htmlContent));
		  System.out.println("打印内容长度："+content.getItem_text().length());
		  oDocument.setFieldValue("DocHtmlCon", DataContentUtil.baseStyle+htmlContent);
		  oDocument.setFieldValue("DOCSOURCENAME ", content.getContent_from()==null?city_name:content.getContent_from());
		  oDocument.setFieldValue("HITSCOUNT ", content.getShow_number());
		  
		  
		  //System.out.println("创建时间："+content.getWrite_date());
		  //System.out.println("发布时间："+content.getIssue_date());
		  //System.out.println("撰写时间："+content.getWrite_date());
		  
		
		  /*if(content.getContentFiles().size()!=0||content.getContentFiles()!=null){
			  for(int i=0;i<content.getContentFiles().size();i++){
				  Map<String,Object> oAppendixMore=new HashMap<String, Object>();
				  oAppendixMore.put("APPDESC", content.getContentFiles().get(i).getFile_info());
				  String localFile="G://AppData/SCDS/Appendix/"+content.getContentFiles().get(i).getFile_name();
				  //oDocument.addAppendix(FLAG_DOCAPD, localFile);
				  oDocument.addAppendix(FLAG_DOCAPD, localFile, oAppendixMore);
				  //oDocument.addRelation(898);
				  //oDocument.addRelation(884);
			  }
			oDocument.uploadAllAppendixs();
			appendixFlag=true;
		  }*/
		  
		  /*抓取文件中的超链接，上传到附件*/
		  /*List<String> linkAppendix=new HtmlRegexpUtil().replaceHtmlTag3(content.getItem_text(), "a", "href", "", "");
		  if(linkAppendix.size()!=0){
			  for(int i=0;i<linkAppendix.size();i++){
				  String mStr=linkAppendix.get(i).replace(".", ",");
				  String[] temStr=mStr.split(",");
				  System.out.println("哈啊："+temStr.length);
				  System.out.println("捕捉的连接后缀：："+temStr[temStr.length-1]);
				  if(temStr[temStr.length-1].equalsIgnoreCase("html")){
					  System.out.println("已忽略后缀："+temStr[temStr.length-1]+"文件！");
				  }else if(temStr[temStr.length-1].equalsIgnoreCase("htm")){
					  System.out.println("已忽略后缀："+temStr[temStr.length-1]+"文件！");
				  }else if(temStr[temStr.length-1].equalsIgnoreCase("asp")){
					  System.out.println("已忽略后缀："+temStr[temStr.length-1]+"文件！");
				  }else if(temStr[temStr.length-1].equalsIgnoreCase("jsp")){
					  System.out.println("已忽略后缀："+temStr[temStr.length-1]+"文件！");
				  }else if(temStr[temStr.length-1].equalsIgnoreCase("action")){
					  System.out.println("已忽略后缀："+temStr[temStr.length-1]+"文件！");
				  }else if(temStr[temStr.length-1].equalsIgnoreCase("do")){
					  System.out.println("已忽略后缀："+temStr[temStr.length-1]+"文件！");
				  }else if(temStr[temStr.length-1].equalsIgnoreCase("shtml")){
					  System.out.println("已忽略后缀："+temStr[temStr.length-1]+"文件！");
				  }else if(temStr[temStr.length-1].equalsIgnoreCase("xhtml")){
					  System.out.println("已忽略后缀："+temStr[temStr.length-1]+"文件！");
				  }else{
					  Map<String,Object> oAppendixMore=new HashMap<String, Object>();
					  String tempFile=linkAppendix.get(i).replace("http://www.sczg-l-tax.gov.cn/", "");
					  tempFile=tempFile.replace("exe", "rar");
					  
					  String localFile="D://AppData/SCDS/Appendix/"+tempFile;
					  System.out.println(tempFile);
					  oDocument.addAppendix(FLAG_DOCAPD, localFile, oAppendixMore);
				  }
				  
			  }
		  }
		  oDocument.uploadAllAppendixs();
		  appendixFlag=true;*/
		
		  saveAll(oDocument);
	}
	
	/*
	 * 图片路径替换
	 * */
	private String ImagePathReplace(String src){
		src=src.replace("src=\"/UserFiles", "src=\"http://155.24.16.2/UserFiles");
		//src=src.replace("src=\"/", "src=\"http://155.16.16.60:7001/");
		//src=src.replace("src=\"/", "src=\"http://155.16.161.140/");
		
		
		/*自贡地税内网图片路径替换*/
		src=src.replace("src=\"/", "src=\"http://155.24.16.2/");
		
		return src;
	}
	
	/*
	 * 时间戳转换为日期
	 * */
	private Timestamp getDate(int dateNumber){
		Date nDate=null;
		Timestamp dateTime = null;
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		String date = sdf.format(new Date(dateNumber*1000));
		
		try {
			//nDate=(Date) sdf.parse(date);
			dateTime = Timestamp.valueOf(date);
			//nDate=(Date)dateTime;
		}finally{
			return dateTime;
		}
	}
}
