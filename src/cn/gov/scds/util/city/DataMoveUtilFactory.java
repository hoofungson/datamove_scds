package cn.gov.scds.util.city;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.dom4j.Element;

import cn.gov.scds.util.Dom4jUtil;

public class DataMoveUtilFactory {
    /*
     * 数据迁移工具工厂：初始化所有市州数据迁移工具
     * */
	private static List<Map<String, DataMoveUtilBase>> dataUtils;
	
	/*
	 * 加载并初始化工具类
	 * */
	private static boolean loadUtil(){
		if(dataUtils==null){
			dataUtils=new ArrayList<Map<String,DataMoveUtilBase>>();
			Map<String, DataMoveUtilBase> noMap=new HashMap<String, DataMoveUtilBase>();
			Dom4jUtil dom4jUtil=new Dom4jUtil();
			Element cityInfo=dom4jUtil.node.element("cdds");
			//dom4jUtil.listNodes(dom4jUtil.node);
			List<Map<String, Object>> y_nodes=dom4jUtil.listNode(dom4jUtil.node);
			Map<String, Object> y_nodemMap=y_nodes.get(0);
			//String mm=dom4jUtil.elementValue(cityInfo.element("cdds")).toString();
			int nodeNumber=y_nodes.size();
			System.out.println("列表长度："+nodeNumber);
			
			for(int i=0;i<nodeNumber;i++){
				
			}
			
			Iterator iterator=y_nodemMap.entrySet().iterator();
			while (iterator.hasNext()) {
				Entry entry=(Entry) iterator.next();
				//y_nodemMap.get(iterator);
				Class<?> util = null;
				try {
					util = Class.forName("cn.gov.scds.util.city."+entry.getValue());
				} catch (ClassNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				DataMoveUtilBase dataMoveUtil = null;
				try {
					dataMoveUtil = (DataMoveUtilBase)util.newInstance();
				} catch (InstantiationException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IllegalAccessException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				noMap.put(entry.getKey().toString(), dataMoveUtil);
			}
			
			dataUtils.add(noMap);
		}
		return true;
	}
	
	/*
	 * 创建数据迁移工具
	 * */
	public static DataMoveUtilBase createDataMoveUtil(String key){
		DataMoveUtilBase dataMoveUtil=null;
		if(loadUtil()){
			System.out.println("数据初始化，成功！");
			System.out.println("工具类的个数："+dataUtils.size());
			
			//开始解析工具列表
			for(Map<String, DataMoveUtilBase> dmap:dataUtils){
				dataMoveUtil=dmap.get(key);
			}
		}else{
			System.out.print("抱歉，工具类初始化失败！");
		}
		
		return dataMoveUtil;
	}
}
