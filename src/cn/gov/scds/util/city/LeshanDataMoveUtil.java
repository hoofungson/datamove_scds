package cn.gov.scds.util.city;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringWriter;
import java.net.URLEncoder;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.gov.scds.entity.Content;
import cn.gov.scds.entity.ContentFile;
import cn.gov.scds.entity.JEmialBox;
import cn.gov.scds.jdbc.DBConnection;
import cn.gov.scds.jdbc.DBInteractionConnection;
import cn.gov.scds.jdbc.DBUitl;
import cn.gov.scds.util.AppContentUtil;
import cn.gov.scds.util.DataContentUtil;
import cn.gov.scds.util.HtmlRegexpUtil;
import cn.gov.scds.util.RemoteFileUtil;

import com.trs.web2frame.domain.WDocumentMgr;
import com.trs.web2frame.entity.WDocument;

import oracle.sql.CLOB;

public class LeshanDataMoveUtil extends DataMoveUtilBase{

	/*
	 * 乐山地税数据迁移工具
	 * */
	public static Connection connect=null;
	public static Statement statement=null;
	public static ResultSet resultSet=null;
	private static boolean appendixFlag=false;
	int y_channel;
	static int x_channel;
	static String city_name;
	CLOB clob = null;
	private static int dataId=7500;
	
	/*
	 * 运行迁移
	 * */
	public void runMove() {
		// TODO Auto-generated method stub
		//moveNews();
		//readJZXXFromJDBC();
		nei_moveNews();
	}
	
	/**********外网数据迁移*************/
	
	
	/**********内网数据迁移*************/
	/*
	 * 读取新闻
	 * */
	private void moveNews(){
		int[] y_channel={159};
		x_channel=7044;//6874
		city_name="四川省乐山市地方税务局";
		
		String sqlStr="select * from dede_addonarticle left join dede_archives on dede_archives.ID=dede_addonarticle.aid where dede_archives.typeid in(";
		StringBuffer sqlBuffer=new StringBuffer();
		
		for(int i=0;i<y_channel.length;i++){
			if(i==0){
				sqlBuffer.append(y_channel[i]);
			}else{
				sqlBuffer.append(","+y_channel[i]);
			}
			
		}
		sqlStr=sqlStr+sqlBuffer.toString()+") order by dede_archives.ID asc";
		
		//String sqlStr="select * from mynews where NEWSTYPE like '图片新闻'";
		
		System.out.println("打印SQL语句："+sqlStr);
		
		int countIndex=1;
        statement=DBUitl.getStatement(DBConnection.getConnection());
		ResultSet resultSet=DBUitl.getResultSet(statement,sqlStr);
		try {
			while (resultSet.next()) {
				Content content=new Content();
				content.setContentID(resultSet.getInt("ID"));
				if(resultSet.getString("TITLE")==null){
					content.setTitle_1("暂无标题");
				}else{
					content.setTitle_1(resultSet.getString("title"));
				}
				System.out.println("源文档编号："+content.getContentID());
				
				String songContent=resultSet.getString("body");
				//处理clob字段
				/*String songContent="";
				clob = (oracle.sql.CLOB) resultSet.getClob("CONTENT_2");
				if(clob!=null){
					BufferedReader in = new BufferedReader(clob.getCharacterStream());
					StringWriter out = new StringWriter();
					int c=0;
					try {
						while ((c = in.read()) != -1) {
						     out.write(c);
						}
						songContent=out.toString();
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						//e1.printStackTrace();
						System.out.println(".....+++此处文档异常...=======");
						songContent="暂无内容！";
					}
					
					
					try {
						out.close();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						//e.printStackTrace();
						System.out.println(".....+++此处文档输出流关闭异常...=======");
					}
					try {
						in.close();
					} catch (IOException e) {
						// TODO Auto-generated catch block
					    //e.printStackTrace();
						System.out.println(".....+++此处文档输入流关闭异常...=======");
					}
				}else{
					songContent="暂无内容";
				}*/
				
				if(songContent==null){
					content.setItem_text("");
				}else{
					System.out.println("内容："+songContent);
				    String itext = null;
					//itext = ClobToString(clob);
					itext=songContent;
					String item_text=DataContentUtil.replaceContentCharacter(itext);
					//String item_text="";
					
					content.setItem_text(item_text);
				}
				content.setWrite_date(getDate(resultSet.getLong("senddate")));
				//content.setWrite_user(resultSet.getInt("WRITE_USER"));
				content.setAudit_date(getDate(resultSet.getLong("senddate")));
				//content.setAudit_user(resultSet.getInt("AUDIT_USER"));
				content.setIssue_date(getDate(resultSet.getLong("pubdate")));
				//content.setIssue_user(resultSet.getInt("ISSUE_USER"));
				int showNumber=resultSet.getInt("click");
				content.setShow_number(showNumber==0?0:showNumber);
				String contentFrom=resultSet.getString("source");
				content.setContent_from(contentFrom==null?city_name:contentFrom);
				//content.setDocchannel(readItemIterator(resultSet.getInt("ITEM_ID")));
				
				System.out.println("文档标题："+content.getTitle_1());
				System.out.println("WRITE_DATE："+content.getWrite_date());
				//System.out.println("WRITE_USER："+resultSet.getInt("WRITE_USER"));
				System.out.println("AUDIT_DATE："+content.getAudit_date());
				//System.out.println("AUDIT_USER："+resultSet.getInt("AUDIT_USER"));
				System.out.println("ISSUE_DATE："+content.getIssue_date());
				//System.out.println("ISSUE_USER："+resultSet.getInt("ISSUE_USER"));
				System.out.println("SHOW_NUMBER："+content.getShow_number());
				System.out.println("CONTENT_FROM："+content.getContent_from());
				
				//调用附件读取方法，一个文档读取一次数据附件。
				connect=DBConnection.getConnection();
				content.setContentFiles(readContentFile(connect,content.getContentID()));
				System.out.println("附件个数："+content.getContentFiles().size());
				
				System.out.println("==============文档读取完毕,,,,,,,,,,,,");
				
				//调用WCM文档推送方法，将封装好后的文档推送到新数据库中。
				
				try {
				   testSaveAll(content);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				System.out.println("当前处理记录是第："+countIndex);
				countIndex++;
			}
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			try {
				resultSet.close();
				statement.close();
				//connect.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		System.out.println("好无聊！");
	}
	
	/*
	 * 根据旧数据库中读取到的文档编号，查询该文档所带的附件。
	 * */
	@SuppressWarnings("finally")
	private List<ContentFile> readContentFile(Connection connection,int contentID){
		//Connection connection=DBConnection.getConnection();
		System.out.println("传入的文档编号："+contentID);
		Statement statement=DBUitl.getStatement(connection);
		List<ContentFile> contentFiles=new ArrayList<ContentFile>();
		String sqlStr="select * from dede_uploads where arcid="+contentID;
		ResultSet resultSet1=DBUitl.getResultSet(statement, sqlStr);
		
		try {
			while (resultSet1.next()) {
				ContentFile contentFile=new ContentFile();
				contentFile.setFile_id(resultSet1.getInt("aid"));
				contentFile.setContent_id(resultSet1.getInt("arcid"));
				contentFile.setFile_name(resultSet1.getString("title"));
				contentFile.setFile_info(resultSet1.getString("title"));
				contentFile.setFile_size(resultSet1.getInt("filesize"));
				contentFile.setFile_text(resultSet1.getString("url"));
				
				System.out.println("附件名称："+contentFile.getFile_info());
				
				//String fileUrl = "http://155.16.161.140/manager/download/downfj.jsp?fileid="+URLEncoder.encode(contentFile.getFile_name(),"GB2312");
				String fileUrl = "http://www.lsdsj.gov.cn"+URLEncoder.encode(contentFile.getFile_text(),"GB2312");
				String fileName = contentFile.getFile_text();
				RemoteFileUtil.getRemoteFile(fileUrl, fileName,0,"");
				contentFiles.add(contentFile);
				
				
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			//DBUitl.close(connection, resultSet1, statement);
			try {
				
				resultSet1.close();
				statement.close();
				connection.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			return contentFiles;
		}
	}
	
	/*
	 * 导入旧数据库中的局长信箱数据
	 * */
	private void readJZXXFromJDBC(){
		connect=DBConnection.getConnection();
		statement=DBUitl.getStatement(connect);
		
		Connection iconnection=DBInteractionConnection.getConnection();
		Statement iStatement=DBUitl.getStatement(iconnection);
		
		String sqlStr="select * from dede_guestbook where face=01 order by id";
		ResultSet resultSet=DBUitl.getResultSet(statement,sqlStr);
		try {
			while(resultSet.next()){
				 JEmialBox jBox=new JEmialBox();
				 jBox.setMailId(resultSet.getInt("id"));
				 jBox.setComemMails(resultSet.getString("uname"));
				 jBox.setComeMailDate(getDate(resultSet.getLong("dtime")));
				 jBox.setMailContent(resultSet.getString("msg"));
				 jBox.setMailEmail(resultSet.getString("email"));
				 /*jBox.setMailTitle(resultSet.getString("ZT"));
				 jBox.setReplyDate(resultSet.getTimestamp("HFRQ"));
				 jBox.setReplyIdea(resultSet.getString("HFNR"));*/
				 
				 //打印出来
				 System.out.println("信件标题："+jBox.getMailTitle());
				 System.out.println("来信人："+jBox.getComemMails());
				 System.out.println("来信日期："+jBox.getComeMailDate());
				 System.out.println("来信邮箱："+jBox.getMailEmail());
				 //System.out.println("来信内容："+jBox.getMailContent().length());
				 System.out.println("回复日期："+jBox.getReplyDate());
				 //System.out.println("来信邮箱："+jBox.getMailEmail());
				 //System.out.println("回复内容："+jBox.getReplyIdea());
				 System.out.println("==========分割线============");
				 
				 writeJZXXByJDBC(jBox,iStatement);
				}
			    iStatement.close();
			    iconnection.close();
			}catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		
		
		try {
			statement.close();
			connect.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void writeJZXXByJDBC(JEmialBox jBox,Statement iStatement){
		String sqlStr="";
		Date hfrq=jBox.getReplyDate();
		if(hfrq!=null){
			//sqlStr="insert into MAIL (mailid,comemmails,comemaildate,mailcontent,mailemail,areaclass) values ("+dataId+",'"+jBox.getComemMails()+"','"+jBox.getComeMailDate().toString()+",'"+jBox.getMailContent()+"','"+jBox.getMailEmail()+"',11)";
		}else{
			sqlStr="insert into MAIL (mailid,comemmails,mailcontent,mailemail,areaclass)values("+dataId+",'"+jBox.getComemMails()+"','"+jBox.getMailContent()+"','"+jBox.getMailEmail()+"',11)";
		}
		
		try {
			iStatement.executeUpdate(sqlStr);
			System.out.println("=========数据导入成功.......");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		dataId++;
	}
	
	/*乐山地税内网数据迁移*/
	private void nei_moveNews(){
		String[] y_channel={"swyj"};
		x_channel=4496;
		city_name="四川省乐山市地方税务局";
		
		String sqlStr="select * from News where SmallClassEN like '"+y_channel[0]+"' order by NewsID asc";
		//String sqlStr="select * from PE_Article";
		//String sqlStr="select * from mynews where NEWSTYPE like '图片新闻'";
		System.out.println("打印SQL语句："+sqlStr);
		int countIndex=1;
        statement=DBUitl.getStatement(DBConnection.getConnection());
		ResultSet resultSet=DBUitl.getResultSet(statement,sqlStr);
		try {
			while (resultSet.next()) {
				Content content=new Content();
				content.setContentID(resultSet.getInt("NewsID"));
				String contentTitle=resultSet.getString("Title");
				if(contentTitle==null){
					content.setTitle_1("暂无标题");
				}else{
					content.setTitle_1(contentTitle);
				}
				System.out.println("源文档编号："+content.getContentID());
				
				String songContent=resultSet.getString("Content");
				if(songContent==null){
					content.setItem_text("");
				}else{
					//System.out.println("内容："+songContent);
				    String itext = null;
					//itext = ClobToString(clob);
					itext=songContent;
					String item_text=DataContentUtil.replaceContentCharacter(itext);
					//String item_text="";
					/*String picInfoString=resultSet.getString("U3");
					if(picInfoString==null){
						content.setItem_text(item_text);
					}else if(picInfoString.isEmpty()){
						content.setItem_text(item_text);
					}else{
						picInfoString="<img src=\"http://155.24.16.2"+picInfoString+"\"/>";
						content.setItem_text(picInfoString+item_text);
					}*/
					content.setItem_text(item_text);
					
				}
				Timestamp xtDate=resultSet.getTimestamp("Date");
				content.setWrite_date(xtDate);
				//content.setWrite_user(resultSet.getInt("WRITE_USER"));
				content.setAudit_date(xtDate);
				//content.setAudit_user(resultSet.getInt("AUDIT_USER"));
				content.setIssue_date(xtDate);
				//content.setIssue_user(resultSet.getInt("ISSUE_USER"));
				int showNumber=resultSet.getInt("Click");
				content.setShow_number(showNumber==0?0:showNumber);
				String contentFrom=resultSet.getString("Source");
				content.setContent_from(contentFrom==null?city_name:contentFrom);
				//content.setDocchannel(readItemIterator(resultSet.getInt("ITEM_ID")));
				
				System.out.println("文档标题："+content.getTitle_1());
				System.out.println("WRITE_DATE："+content.getWrite_date());
				//System.out.println("WRITE_USER："+resultSet.getInt("WRITE_USER"));
				System.out.println("AUDIT_DATE："+content.getAudit_date());
				//System.out.println("AUDIT_USER："+resultSet.getInt("AUDIT_USER"));
				System.out.println("ISSUE_DATE："+content.getIssue_date());
				//System.out.println("ISSUE_USER："+resultSet.getInt("ISSUE_USER"));
				System.out.println("SHOW_NUMBER："+content.getShow_number());
				System.out.println("CONTENT_FROM："+content.getContent_from());
				
				System.out.println("==============文档读取完毕,,,,,,,,,,,,");
				
				//调用WCM文档推送方法，将封装好后的文档推送到新数据库中。
				
				try {
				   testSaveAll(content);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				System.out.println("当前处理记录是第："+countIndex);
				countIndex++;
			}
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			try {
				resultSet.close();
				statement.close();
				//connect.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		System.out.println("好无聊！");
	}
	
	/*
	 * 根据旧数据库读取的文档，写入到新的数据库。
	 * */
	/**
	 * 保存文档及其所有相关项，包括引用栏目，附件，相关文档
	 * 
	 * @param _oDocument
	 * @throws Exception 
	 */
	private static void saveAll(WDocument _oDocument) throws Exception {
		long recordId=WDocumentMgr.save(_oDocument);
		WDocumentMgr.quoteTo(_oDocument);
		
		if(appendixFlag==true){
			try {
				WDocumentMgr.saveAppendixs(_oDocument);
				//WDocumentMgr.saveRelations(_oDocument);
			} catch (Exception e) {
				// TODO: handle exception
				System.out.println("妈的，附件后缀格式又不支持了！");
			}
			
		}
		WDocumentMgr.saveAppendixs(_oDocument);
		//WDocumentMgr.saveRelations(_oDocument);
		
		
	}
	
	/** 附件类型标识：文档附件 */
    public final static int FLAG_DOCAPD = 10; // 文件:Document Appendix

    /** 附件类型标识：文档图片 */
    public final static int FLAG_DOCPIC = 20; // 图片:Document Picture

    /** 附件类型标识：HTML */
    public final static int FLAG_HTMLPIC = 30; // HTML图片:Html Picture

    /** 附件类型标识：链接附件 */
    public final static int FLAG_LINK = 40; // 链接:Link
    
    
	public static void testSaveAll(Content content) throws Exception {
		   System.out.println("=========进入文档写方法======");
		  WDocument oDocument = new WDocument();
		
		  oDocument.setFieldValue("ChannelId", x_channel);
		  oDocument.setFieldValue("ObjectId", new Integer(0));
		
		  oDocument.setFieldValue("DocTitle", content.getTitle_1());
		  oDocument.setFieldValue("DOCTYPE", new Integer(20));
		  System.out.println("content.getWrite_date()"+content.getWrite_date());
		  oDocument.setFieldValue("CRTIME", content.getWrite_date());
		  oDocument.setFieldValue("DOCRELTIME", content.getWrite_date());
		  String htmlContent=new HtmlRegexpUtil().fiterHtmlTag(content.getItem_text(), "a");
		  htmlContent=htmlContent.replace("</a>", "");
		  htmlContent=new HtmlRegexpUtil().fiterHtmlTag(htmlContent, "A");
		  htmlContent=htmlContent.replace("</A>", "");
		  //System.out.println("处理后的内容："+htmlContent);
		  oDocument.setFieldValue("DOCCONTENT", AppContentUtil.replaceSpecialCharacter(htmlContent));
		  System.out.println("打印内容长度："+content.getItem_text().length());
		  oDocument.setFieldValue("DocHtmlCon", DataContentUtil.baseStyle+htmlContent);
		  oDocument.setFieldValue("DOCSOURCENAME ", content.getContent_from()==null?city_name:content.getContent_from());
		  oDocument.setFieldValue("HITSCOUNT ", content.getShow_number());
		  
		  
		  System.out.println("创建时间："+content.getWrite_date());
		  //System.out.println("发布时间："+content.getIssue_date());
		  System.out.println("撰写时间："+content.getWrite_date());
		  
		
		  /*if(content.getContentFiles().size()!=0||content.getContentFiles()!=null){
			  for(int i=0;i<content.getContentFiles().size();i++){
				  Map<String,Object> oAppendixMore=new HashMap<String, Object>();
				  oAppendixMore.put("APPDESC", content.getContentFiles().get(i).getFile_info());
				  String localFile="D://AppData/SCDS/Appendix/"+content.getContentFiles().get(i).getFile_text();
				  //oDocument.addAppendix(FLAG_DOCAPD, localFile);
				  oDocument.addAppendix(FLAG_DOCAPD, localFile, oAppendixMore);
				  //oDocument.addRelation(898);
				  //oDocument.addRelation(884);
			  }
			oDocument.uploadAllAppendixs();
			appendixFlag=true;
		  }*/
		  
		  /*抓取文件中的超链接，上传到附件*/
		  List<String> linkAppendix=new HtmlRegexpUtil().replaceHtmlTag3(content.getItem_text(), "a", "href", "", "");
		  if(linkAppendix.size()!=0){
			  for(int i=0;i<linkAppendix.size();i++){
				  String mStr=linkAppendix.get(i).replace(".", ",");
				  String[] temStr=mStr.split(",");
				  System.out.println("哈啊："+temStr.length);
				  System.out.println("捕捉的连接后缀：："+temStr[temStr.length-1]);
				  if(temStr[temStr.length-1].equalsIgnoreCase("html")){
					  System.out.println("已忽略后缀："+temStr[temStr.length-1]+"文件！");
				  }else if(temStr[temStr.length-1].equalsIgnoreCase("htm")){
					  System.out.println("已忽略后缀："+temStr[temStr.length-1]+"文件！");
				  }else if(temStr[temStr.length-1].equalsIgnoreCase("asp")){
					  System.out.println("已忽略后缀："+temStr[temStr.length-1]+"文件！");
				  }else if(temStr[temStr.length-1].equalsIgnoreCase("jsp")){
					  System.out.println("已忽略后缀："+temStr[temStr.length-1]+"文件！");
				  }else if(temStr[temStr.length-1].equalsIgnoreCase("action")){
					  System.out.println("已忽略后缀："+temStr[temStr.length-1]+"文件！");
				  }else if(temStr[temStr.length-1].equalsIgnoreCase("do")){
					  System.out.println("已忽略后缀："+temStr[temStr.length-1]+"文件！");
				  }else if(temStr[temStr.length-1].equalsIgnoreCase("shtml")){
					  System.out.println("已忽略后缀："+temStr[temStr.length-1]+"文件！");
				  }else if(temStr[temStr.length-1].equalsIgnoreCase("xhtml")){
					  System.out.println("已忽略后缀："+temStr[temStr.length-1]+"文件！");
				  }else{
					  Map<String,Object> oAppendixMore=new HashMap<String, Object>();
					  String tempFile=linkAppendix.get(i).replace("http://155.60.16.88/", "");
					  tempFile=tempFile.replace("exe", "rar");
					  
					  String localFile="G://AppData/SCDS/Appendix/"+tempFile;
					  System.out.println(tempFile);
					  oDocument.addAppendix(FLAG_DOCAPD, localFile, oAppendixMore);
				  }
				  
			  }
		  }
		  oDocument.uploadAllAppendixs();
		  appendixFlag=true;
		
		  saveAll(oDocument);
	}
	
	/*
	 * 时间戳转换为日期
	 * */
	private Timestamp getDate(long dateNumber){
		Date nDate=null;
		Timestamp dateTime = null;
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		String date = sdf.format(new Date(dateNumber*1000));
		
		try {
			//nDate=(Date) sdf.parse(date);
			dateTime = Timestamp.valueOf(date);
			//nDate=(Date)dateTime;
		}finally{
			return dateTime;
		}
	}
}
