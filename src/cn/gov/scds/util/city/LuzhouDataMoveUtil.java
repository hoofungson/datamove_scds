package cn.gov.scds.util.city;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.StringWriter;
import java.net.URLEncoder;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import oracle.sql.CLOB;
import cn.gov.scds.entity.Content;
import cn.gov.scds.entity.ContentFile;
import cn.gov.scds.entity.JEmialBox;
import cn.gov.scds.jdbc.DBConnection;
import cn.gov.scds.jdbc.DBInteractionConnection;
import cn.gov.scds.jdbc.DBUitl;
import cn.gov.scds.util.AppContentUtil;
import cn.gov.scds.util.DataContentUtil;
import cn.gov.scds.util.PrimaryKeyUtil;
import cn.gov.scds.util.RemoteFileUtil;

import com.trs.web2frame.domain.WDocumentMgr;
import com.trs.web2frame.entity.WDocument;

public class LuzhouDataMoveUtil extends DataMoveUtilBase{

	/*
	 * 泸州数据迁移工具
	 * */
	public static Connection connect=null;
	public static Statement statement=null;
	public static ResultSet resultSet=null;
	private static List<Integer> specialIdList;
	private static boolean currentRecordFlag=false;
	private static List<Long> specailRecordList=new ArrayList<Long>();
	private static boolean appendixFlag=false;
	private static int currentIndex=1;
	FileReader _frd = null;
	CLOB clob = null;
	BufferedReader _brd;
    String _rs = null;
    int y_channel;
	static int x_channel;
	static String city_name;
	
	static List<Content> contents=new ArrayList<Content>();
	//static List<ContentFile> contentFiles=new ArrayList<ContentFile>();
	
	@Override
	public void runMove() {
		// TODO Auto-generated method stub
		//readJZXXFromJDBC();
		readContentFromJDBC();
	}
	
	/*
	 * 数据迁移。
	 * */
	public void DataMove(){
		//initSpecailId();
		//readContentFromJDBC();
		/*if(specailRecordList.size()!=0){
			System.out.println("=========特殊记录编号如下：");
			for(Long reInteger:specailRecordList){
				System.out.println(reInteger);
			}
		}*/
		
		readJZXXFromJDBC();
	}
	
	/*
	 * 初始化特殊ID列表
	 * */
	private boolean initSpecailId(){
		specialIdList=new ArrayList<Integer>();
		/*specialIdList.add(19051);
		specialIdList.add(19090);
		specialIdList.add(19092);
		specialIdList.add(19091);
		specialIdList.add(19658);*/
		
		return true;
	}
	
	/*
	 * 导入旧数据库中的局长信箱数据
	 * */
	private void readJZXXFromJDBC(){
		connect=DBConnection.getConnection();
		statement=DBUitl.getStatement(connect);
		
		Connection iconnection=DBInteractionConnection.getConnection();
		Statement iStatement=DBUitl.getStatement(iconnection);
		
		String sqlStr="select * from DD_JZXX where zf=0 and ZZJG_ID='2026' and WZJY='0' order by LRRQ";
		ResultSet resultSet=DBUitl.getResultSet(statement,sqlStr);
		try {
			while(resultSet.next()){
				 JEmialBox jBox=new JEmialBox();
				 jBox.setMailId(resultSet.getInt("ID"));
				 jBox.setComemMails(resultSet.getString("NAME"));
				 jBox.setComeMailDate(resultSet.getTimestamp("LRRQ"));
				 jBox.setMailContent(resultSet.getString("LRNR"));
				 jBox.setMailEmail(resultSet.getString("EMAIL"));
				 jBox.setMailTitle(resultSet.getString("ZT"));
				 jBox.setReplyDate(resultSet.getTimestamp("HFRQ"));
				 jBox.setReplyIdea(resultSet.getString("HFNR"));
				 
				 //打印出来
				 System.out.println("信件标题："+jBox.getMailTitle());
				 System.out.println("来信人："+jBox.getComemMails());
				 System.out.println("来信日期："+jBox.getComeMailDate());
				 System.out.println("来信邮箱："+jBox.getMailEmail());
				 //System.out.println("来信内容："+jBox.getMailContent().length());
				 System.out.println("回复日期："+jBox.getReplyDate());
				 //System.out.println("来信邮箱："+jBox.getMailEmail());
				 //System.out.println("回复内容："+jBox.getReplyIdea());
				 System.out.println("==========分割线============");
				 
				 writeJZXXByJDBC(jBox,iStatement);
				}
			    iStatement.close();
			    iconnection.close();
			}catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		
		
		try {
			statement.close();
			connect.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void writeJZXXByJDBC(JEmialBox jBox,Statement iStatement){
		String sqlStr="";
		Date hfrq=jBox.getReplyDate();
		PrimaryKeyUtil primaryKeyUtil=new PrimaryKeyUtil();
		String sql="select max(mailid) from mail";
		int dataId=primaryKeyUtil.getProgressivelyKey(sql);
		if(hfrq!=null){
			sqlStr="insert into MAIL (MAILID,COMEMMAILS,COMEMAILDATE,MAILCONTENT,MAILEMAIL,MAILTITLE,REPLYDATE,REPLYIDEA,AREACLASS,LEARDCLASS) values ("+dataId+",'"+jBox.getComemMails()+"',to_date('"+jBox.getComeMailDate().toString().substring(0, 19)+"','yyyy-mm-dd,hh24:mi:ss'),'"+jBox.getMailContent()+"','"+jBox.getMailEmail()+"','"+jBox.getMailTitle()+"',to_date('"+jBox.getReplyDate().toString().substring(0, 19)+"','yyyy-mm-dd,hh24:mi:ss'),'"+jBox.getReplyIdea()+"',8,1)";
		}else{
			sqlStr="insert into MAIL (MAILID,COMEMMAILS,COMEMAILDATE,MAILCONTENT,MAILEMAIL,MAILTITLE,AREACLASS,LEARDCLASS) values ("+dataId+",'"+jBox.getComemMails()+"',to_date('"+jBox.getComeMailDate().toString().substring(0, 19)+"','yyyy-mm-dd,hh24:mi:ss'),'"+jBox.getMailContent()+"','"+jBox.getMailEmail()+"','"+jBox.getMailTitle()+"',8,1)";
		}
		
		try {
			iStatement.executeUpdate(sqlStr);
			System.out.println("=========数据导入成功.......");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/*
	 * 从旧数据库中读取文档，通过JDBC方式。
	 * */
	private void readContentFromJDBC(){
		y_channel=1183;
		x_channel=8249;
		city_name="四川省泸州市地方税务局";
		
		int countIndex=1;
		statement=DBUitl.getStatement(DBConnection.getConnection());
		String sqlStr="select * from CMS_ITEM_CONTENT where  ITEM_ID in("+y_channel+") and is_ww=1 order by WRITE_DATE asc";
		//String sqlStr="select * from CMS_ITEM_CONTENT where ITEM_ID in(244) and WRITE_DATE>=to_date('2015-01-01,00:00:00','yyyy-mm-dd,hh24:mi:ss')  order by WRITE_DATE ";
		//String sqlStr="select * from CMS_ITEM_CONTENT where ITEM_ID in(544) order by WRITE_DATE ";
		ResultSet resultSet=DBUitl.getResultSet(statement,sqlStr);
		try {
			while(resultSet.next()){
				
				/*try {
					_frd = new FileReader(new File("D:\\DOS.txt"));
				} catch (FileNotFoundException e2) {
					// TODO Auto-generated catch block
					e2.printStackTrace();
				}
				_brd = new BufferedReader(_frd);
				try {
					_rs = _brd.readLine();
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
		        StringBuffer _input = new StringBuffer();
		        while (_rs != null) {
		            _input.append(_rs);
		            try {
						_rs = _brd.readLine();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
		        }*/
				
				currentRecordFlag=false;
				appendixFlag=false;
				Content content=new Content();
				content.setContentID(resultSet.getInt("ID"));
				//content.setItem_ID(resultSet.getInt("ITEM_ID"));
				if(resultSet.getString("TITLE_1")==null){
					content.setTitle_1("暂无标题");
				}else{
					content.setTitle_1(resultSet.getString("TITLE_1"));
				}
				
				//content.setSubject(resultSet.getString("SUBJECT"));
				//content.setKeyword(resultSet.getString("KEYWORD"));
				System.out.println("源文档编号："+content.getContentID());
				
				//String myContent=resultSet.getString("ITEM_TEXT");
				//System.out.println("正文内容："+myContent);
				
				/*if(specialIdList.size()>=0){
					for(Integer specailID:specialIdList){
						if(content.getContentID()==specailID){
							String item_text=ImagePathReplace(myContent);
							content.setItem_text(item_text);
							currentRecordFlag=true;
						}else{
							if(myContent.length()==0||myContent==null||myContent.equalsIgnoreCase("")||myContent==""){
								content.setItem_text("");
							}else{
								String item_text=DataContentUtil.replaceContentCharacter(resultSet.getString("ITEM_TEXT"));
								//String item_text="";
								content.setItem_text(item_text);
							}
						}
					}
				}else{
					if(myContent.length()==0||myContent==null||myContent.equalsIgnoreCase("")||myContent==""){
						content.setItem_text("");
					}else{
						String item_text=DataContentUtil.replaceContentCharacter(resultSet.getString("ITEM_TEXT"));
						content.setItem_text(item_text);
					}
				}*/
				String songContent="";
				clob = (oracle.sql.CLOB) resultSet.getClob("ITEM_TEXT");
				if(clob!=null){
					BufferedReader in = new BufferedReader(clob.getCharacterStream());
					StringWriter out = new StringWriter();
					int c=0;
					try {
						while ((c = in.read()) != -1) {
						     out.write(c);
						}
						songContent=out.toString();
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						//e1.printStackTrace();
						System.out.println(".....+++此处文档异常...=======");
						songContent="暂无内容！";
					}
					
					
					try {
						out.close();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						//e.printStackTrace();
						System.out.println(".....+++此处文档输出流关闭异常...=======");
					}
					try {
						in.close();
					} catch (IOException e) {
						// TODO Auto-generated catch block
					    //e.printStackTrace();
						System.out.println(".....+++此处文档输入流关闭异常...=======");
					}
				}else{
					songContent="暂无内容";
				}
				
				if(songContent==null){
					/*if(resultSet.getString("ITEM_TEXT")==null){
						
					}*/
					content.setItem_text("");
				}else{
					System.out.println("内容："+songContent);
				    String itext = null;
					//itext = ClobToString(clob);
					itext=songContent;
					String item_text=DataContentUtil.replaceContentCharacter(itext);
					//String item_text="";
					
					content.setItem_text(item_text);
				}
				content.setWrite_date(resultSet.getTimestamp("WRITE_DATE"));
				//content.setWrite_user(resultSet.getInt("WRITE_USER"));
				content.setAudit_date(resultSet.getTimestamp("AUDIT_DATE"));
				//content.setAudit_user(resultSet.getInt("AUDIT_USER"));
				content.setIssue_date(resultSet.getTimestamp("ISSUE_DATE"));
				//content.setIssue_user(resultSet.getInt("ISSUE_USER"));
				int showNumber=resultSet.getInt("SHOW_NUMBER");
				content.setShow_number(showNumber==0?0:showNumber);
				String contentFrom=resultSet.getString("CONTENT_FROM");
				content.setContent_from(contentFrom==null?city_name:contentFrom);
				//content.setDocchannel(readItemIterator(resultSet.getInt("ITEM_ID")));
				
				System.out.println("文档标题："+content.getTitle_1());
				System.out.println("WRITE_DATE："+resultSet.getTimestamp("WRITE_DATE"));
				//System.out.println("WRITE_USER："+resultSet.getInt("WRITE_USER"));
				System.out.println("AUDIT_DATE："+resultSet.getTimestamp("AUDIT_DATE"));
				//System.out.println("AUDIT_USER："+resultSet.getInt("AUDIT_USER"));
				System.out.println("ISSUE_DATE："+resultSet.getTimestamp("ISSUE_DATE"));
				//System.out.println("ISSUE_USER："+resultSet.getInt("ISSUE_USER"));
				System.out.println("SHOW_NUMBER："+resultSet.getInt("SHOW_NUMBER"));
				System.out.println("CONTENT_FROM："+resultSet.getString("CONTENT_FROM"));
				
				//调用附件读取方法，一个文档读取一次数据附件。
				connect=DBConnection.getConnection();
				content.setContentFiles(readContentFile(connect,content.getContentID()));
				System.out.println("附件个数："+content.getContentFiles().size());
				
				System.out.println("==============文档读取完毕,,,,,,,,,,,,");
				
				//调用WCM文档推送方法，将封装好后的文档推送到新数据库中。
				
				try {
				   testSaveAll(content);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				/*System.out.println("文档编号："+content.getContentID());
				System.out.println("创建时间："+content.getWrite_date());
				System.out.println("发布时间："+content.getIssue_date());
				System.out.println("撰写时间："+content.getWrite_date());
				System.out.println("附件记录："+content.getContentFiles().size());*/
				
				/*try {
					_frd.close();
					_brd.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}*/
				System.out.println("当前处理记录是第："+countIndex);
				countIndex++;
				currentIndex++;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	// 将字CLOB转成STRING类型
    public String ClobToString(CLOB clob) throws SQLException, IOException {

    	 if (clob == null) {
    		   return null;
    		  }
    		  BufferedReader reader = null;
    		  try {
    		   reader = new BufferedReader(clob.getCharacterStream());
    		   StringBuffer buf = new StringBuffer();
    		   String line = null;
    		   while ((line = reader.readLine()) != null) {
    		    buf.append(line);
    		   }
    		   return buf.toString();
    		  } catch (Exception e) {
    		   e.printStackTrace(System.out);
    		   return null;
    		  } finally {
    		   if (reader != null) {
    		    try {
    		     reader.close();
    		    } catch (IOException e) {
    		     //
    		    }
    		   }
    		  }
    }
	
	/*
	 * 根据旧数据库中读取到的文档编号，查询该文档所带的附件。
	 * */
	@SuppressWarnings("finally")
	private List<ContentFile> readContentFile(Connection connection,int contentID){
		//Connection connection=DBConnection.getConnection();
		System.out.println("传入的文档编号："+contentID);
		Statement statement=DBUitl.getStatement(connection);
		List<ContentFile> contentFiles=new ArrayList<ContentFile>();
		String sqlStr="select * from CMS_ITEM_CONTENT_FILES where CONTENT_ID="+contentID;
		ResultSet resultSet1=DBUitl.getResultSet(statement, sqlStr);
		
		try {
			while (resultSet1.next()) {
				ContentFile contentFile=new ContentFile();
				contentFile.setFile_id(resultSet1.getInt("FILE_ID"));
				contentFile.setContent_id(resultSet1.getInt("CONTENT_ID"));
				contentFile.setFile_name(resultSet1.getString("FILE_NAME"));
				contentFile.setFile_info(resultSet1.getString("FILE_INFO"));
				contentFile.setFile_size(resultSet1.getInt("FILE_SIZE"));
				contentFile.setFile_text(resultSet1.getString("FILE_EXT"));
				
				System.out.println("附件名称："+contentFile.getFile_info());
				
				//String fileUrl = "http://155.16.161.140/manager/download/downfj.jsp?fileid="+URLEncoder.encode(contentFile.getFile_name(),"GB2312");
				String fileUrl = "http://155.16.16.60:7001/manager/download/downfj.jsp?fileid="+URLEncoder.encode(contentFile.getFile_name(),"GB2312");
				String fileName = contentFile.getFile_name();
				RemoteFileUtil.getRemoteFile(fileUrl, fileName,0,"");
				contentFiles.add(contentFile);
				
				
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			//DBUitl.close(connection, resultSet1, statement);
			try {
				
				resultSet1.close();
				statement.close();
				connection.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			return contentFiles;
		}
	}
	
	/*
	 * 根据旧数据库读取的文档，写入到新的数据库。
	 * */
	/**
	 * 保存文档及其所有相关项，包括引用栏目，附件，相关文档
	 * 
	 * @param _oDocument
	 * @throws Exception 
	 */
	private static void saveAll(WDocument _oDocument) throws Exception {
	    
		long recordId=WDocumentMgr.save(_oDocument);
	    /*if(currentRecordFlag==true){
	    	specailRecordList.add(recordId);
	    	System.out.println("当前记录是特殊记录，编号是："+recordId);
	    }else{
	    	System.out.println("当前记录不是特殊记录，编号是："+recordId);
	    }*/
	    //System.out.println("saveAll："+_oDocument);
		WDocumentMgr.quoteTo(_oDocument);
		
		if(appendixFlag==true){
			try {
				WDocumentMgr.saveAppendixs(_oDocument);
				//WDocumentMgr.saveRelations(_oDocument);
			} catch (Exception e) {
				// TODO: handle exception
				System.out.println("妈的，附件后缀格式又不支持了！");
			}
			
		}
		//WDocumentMgr.saveAppendixs(_oDocument);
		//WDocumentMgr.saveRelations(_oDocument);
		
		
	}
	
	/** 附件类型标识：文档附件 */
    public final static int FLAG_DOCAPD = 10; // 文件:Document Appendix

    /** 附件类型标识：文档图片 */
    public final static int FLAG_DOCPIC = 20; // 图片:Document Picture

    /** 附件类型标识：HTML */
    public final static int FLAG_HTMLPIC = 30; // HTML图片:Html Picture

    /** 附件类型标识：链接附件 */
    public final static int FLAG_LINK = 40; // 链接:Link
    
    
	public static void testSaveAll(Content content) throws Exception {
		   System.out.println("=========进入文档写方法======");
		  WDocument oDocument = new WDocument();
		
		  oDocument.setFieldValue("ChannelId", x_channel);
		  oDocument.setFieldValue("ObjectId", new Integer(0));
		
		  oDocument.setFieldValue("DocTitle", content.getTitle_1());
		  oDocument.setFieldValue("DOCTYPE", new Integer(20));
		  System.out.println("content.getWrite_date()"+content.getWrite_date());
		  oDocument.setFieldValue("CRTIME", content.getWrite_date());
		  oDocument.setFieldValue("DOCRELTIME", content.getWrite_date());
		  oDocument.setFieldValue("DOCCONTENT", AppContentUtil.replaceSpecialCharacter(content.getItem_text()));
		  System.out.println("打印内容长度："+content.getItem_text().length());
		  oDocument.setFieldValue("DocHtmlCon", content.getItem_text());
		  oDocument.setFieldValue("DOCSOURCENAME ", content.getContent_from()==null?city_name:content.getContent_from());
		  oDocument.setFieldValue("HITSCOUNT ", content.getShow_number());
		  
		  
		  System.out.println("创建时间："+content.getWrite_date());
		  //System.out.println("发布时间："+content.getIssue_date());
		  System.out.println("撰写时间："+content.getWrite_date());
		  
		
		  if(content.getContentFiles().size()!=0){
			  for(int i=0;i<content.getContentFiles().size();i++){
				  Map<String,Object> oAppendixMore=new HashMap<String, Object>();
				  oAppendixMore.put("APPDESC", content.getContentFiles().get(i).getFile_info());
				  String localFile="D://AppData/SCDS/Appendix/"+content.getContentFiles().get(i).getFile_name();
				  //oDocument.addAppendix(FLAG_DOCAPD, localFile);
				  oDocument.addAppendix(FLAG_DOCAPD, localFile, oAppendixMore);
				  //oDocument.addRelation(898);
				  //oDocument.addRelation(884);
			  }
			oDocument.uploadAllAppendixs();
			appendixFlag=true;
		  }
		
		  saveAll(oDocument);
	}

}
