package cn.gov.scds.util.city;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;

import oracle.sql.CLOB;
import cn.gov.scds.entity.Content;
import cn.gov.scds.jdbc.DBConnection;
import cn.gov.scds.jdbc.DBUitl;
import cn.gov.scds.util.AppContentUtil;
import cn.gov.scds.util.DataContentUtil;

import com.trs.web2frame.domain.WDocumentMgr;
import com.trs.web2frame.entity.WDocument;

public class BazhongDataMoveUtil extends DataMoveUtilBase{

	/*
	 * 巴中数据迁移工具
	 * */
	
	public static Connection connect=null;
	public static Statement statement=null;
	public static ResultSet resultSet=null;
	private static boolean appendixFlag=false;
	int y_channel;
	static int x_channel;
	static String city_name;
	CLOB clob = null;
	
	/*
	 * 开始数据迁移
	 * */
	public void runMove() {
		// TODO Auto-generated method stub
		System.out.println("=======开始迁移巴中地税数据........");
		dataMove_wai();
	}
	
	/*外网数据迁移*/
	private boolean dataMove_wai(){
		//wai_moveDocument();
		moveDocument();
		//moveGongao();
		//moveLeader();
		
		return false;
	}
	
	/*读取新闻数据*/
	private void moveDocument(){
		//int[] y_channel={802,0803,0805,0806,0807,0809,0810,0811};
		String[] y_channel={"0503"};
		x_channel=4714;
		city_name="四川省巴中市地方税务局";
		
		String sqlStr="select * from MAIN where code in(";
		StringBuffer sqlBuffer=new StringBuffer();
		
		for(int i=0;i<y_channel.length;i++){
			if(i==0){
				sqlBuffer.append(y_channel[i]);
			}else{
				sqlBuffer.append(","+y_channel[i]);
			}
			
		}
		sqlStr=sqlStr+sqlBuffer.toString()+") order by 录入时间 asc";
		
		System.out.println("打印SQL语句："+sqlStr);
		
		moveData(sqlStr);
	}
	
	/*
	 * 外网部分
	 * */
	private void wai_moveDocument(){
		String[] y_channel={"0107"};
		x_channel=3576;
		city_name="四川省巴中市地方税务局";
		
		String sqlStr="select MAIN.*,archive.CONTENT from MAIN left join archive on archive.id=main.id where main.code in(";
		StringBuffer sqlBuffer=new StringBuffer();
		
		for(int i=0;i<y_channel.length;i++){
			if(i==0){
				sqlBuffer.append(y_channel[i]);
			}else{
				sqlBuffer.append(","+y_channel[i]);
			}
			
		}
		sqlStr=sqlStr+sqlBuffer.toString()+") order by main.录入时间 asc";
		
		System.out.println("打印SQL语句："+sqlStr);
		
		wai_moveData(sqlStr);
	}
	
	/*
	 * 读取数据通用方法
	 * */
	private void moveData(String sqlStr){
		int countIndex=1;
        statement=DBUitl.getStatement(DBConnection.getConnection());
		ResultSet resultSet=DBUitl.getResultSet(statement,sqlStr);
		try {
			while (resultSet.next()) {
				Content content=new Content();
				content.setContentID(resultSet.getInt("ID"));
				if(resultSet.getString("标题")==null){
					content.setTitle_1("暂无标题");
				}else{
					content.setTitle_1(resultSet.getString("标题"));
				}
				System.out.println("源文档编号："+content.getContentID());
				
				//String songContent="";
				//处理clob字段
				String songContent="";
				clob = (oracle.sql.CLOB) resultSet.getClob("NR");
				if(clob!=null){
					BufferedReader in = new BufferedReader(clob.getCharacterStream());
					StringWriter out = new StringWriter();
					int c=0;
					try {
						while ((c = in.read()) != -1) {
						     out.write(c);
						}
						songContent=out.toString();
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						//e1.printStackTrace();
						System.out.println(".....+++此处文档异常...=======");
						songContent="暂无内容！";
					}
					
					
					try {
						out.close();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						//e.printStackTrace();
						System.out.println(".....+++此处文档输出流关闭异常...=======");
					}
					try {
						in.close();
					} catch (IOException e) {
						// TODO Auto-generated catch block
					    //e.printStackTrace();
						System.out.println(".....+++此处文档输入流关闭异常...=======");
					}
				}else{
					songContent="暂无内容";
				}
				
				if(songContent==null){
					content.setItem_text("");
				}else{
					System.out.println("内容："+songContent);
				    String itext = null;
					//itext = ClobToString(clob);
					itext=songContent;
					String item_text=DataContentUtil.replaceContentCharacter(itext);
					//String item_text="";
					
					content.setItem_text(item_text);
				}
				content.setWrite_date(resultSet.getTimestamp("录入时间"));
				//content.setWrite_user(resultSet.getInt("WRITE_USER"));
				content.setAudit_date(resultSet.getTimestamp("生效时间"));
				//content.setAudit_user(resultSet.getInt("AUDIT_USER"));
				content.setIssue_date(resultSet.getTimestamp("发文日期"));
				//content.setIssue_user(resultSet.getInt("ISSUE_USER"));
				int showNumber=resultSet.getInt("点击");
				content.setShow_number(showNumber==0?0:showNumber);
				String contentFrom=resultSet.getString("出处");
				content.setContent_from(contentFrom==null?city_name:contentFrom);
				//content.setDocchannel(readItemIterator(resultSet.getInt("ITEM_ID")));
				
				System.out.println("文档标题："+content.getTitle_1());
				System.out.println("WRITE_DATE："+resultSet.getTimestamp("录入时间"));
				//System.out.println("WRITE_USER："+resultSet.getInt("WRITE_USER"));
				System.out.println("AUDIT_DATE："+resultSet.getTimestamp("生效时间"));
				//System.out.println("AUDIT_USER："+resultSet.getInt("AUDIT_USER"));
				System.out.println("ISSUE_DATE："+resultSet.getTimestamp("发文日期"));
				//System.out.println("ISSUE_USER："+resultSet.getInt("ISSUE_USER"));
				System.out.println("SHOW_NUMBER："+resultSet.getInt("点击"));
				System.out.println("CONTENT_FROM："+resultSet.getString("出处"));
				
				//调用附件读取方法，一个文档读取一次数据附件。
				//connect=DBConnection.getConnection();
				//content.setContentFiles(readContentFile(connect,content.getContentID()));
				//System.out.println("附件个数："+content.getContentFiles().size());
				
				System.out.println("==============文档读取完毕,,,,,,,,,,,,");
				
				//调用WCM文档推送方法，将封装好后的文档推送到新数据库中。
				
				try {
				   //testSaveAll(content);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				System.out.println("当前处理记录是第："+countIndex);
				countIndex++;
				
			}
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			try {
				resultSet.close();
				statement.close();
				//connect.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		System.out.println("好无聊！");
	}
	
	/*
	 * 读取数据通用方法
	 * */
	private void wai_moveData(String sqlStr){
		int countIndex=1;
        statement=DBUitl.getStatement(DBConnection.getConnection());
		ResultSet resultSet=DBUitl.getResultSet(statement,sqlStr);
		try {
			while (resultSet.next()) {
				Content content=new Content();
				content.setContentID(resultSet.getInt("ID"));
				if(resultSet.getString("标题")==null){
					content.setTitle_1("暂无标题");
				}else{
					content.setTitle_1(resultSet.getString("标题"));
				}
				System.out.println("源文档编号："+content.getContentID());
				
				//String songContent="";
				//处理clob字段
				//Long textLong=resultSet.getLong("内容");
				String songContent="";
				clob = (oracle.sql.CLOB) resultSet.getClob("content");
				if(clob!=null){
					BufferedReader in = new BufferedReader(clob.getCharacterStream());
					StringWriter out = new StringWriter();
					int c=0;
					try {
						while ((c = in.read()) != -1) {
						     out.write(c);
						}
						songContent=out.toString();
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						//e1.printStackTrace();
						System.out.println(".....+++此处文档异常...=======");
						songContent="暂无内容！";
					}
					
					
					try {
						out.close();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						//e.printStackTrace();
						System.out.println(".....+++此处文档输出流关闭异常...=======");
					}
					try {
						in.close();
					} catch (IOException e) {
						// TODO Auto-generated catch block
					    //e.printStackTrace();
						System.out.println(".....+++此处文档输入流关闭异常...=======");
					}
				}else{
					songContent="暂无内容";
				}
				
				if(songContent==null){
					content.setItem_text("");
				}else{
					System.out.println("内容："+songContent);
				    String itext = null;
					//itext = ClobToString(clob);
					itext=songContent;
					String item_text=DataContentUtil.replaceContentCharacter(itext);
					//String item_text="";
					
					content.setItem_text(item_text);
				}
				content.setWrite_date(resultSet.getTimestamp("录入时间"));
				//content.setWrite_user(resultSet.getInt("WRITE_USER"));
				content.setAudit_date(resultSet.getTimestamp("生效时间"));
				//content.setAudit_user(resultSet.getInt("AUDIT_USER"));
				content.setIssue_date(resultSet.getTimestamp("发文日期"));
				//content.setIssue_user(resultSet.getInt("ISSUE_USER"));
				int showNumber=resultSet.getInt("点击");
				content.setShow_number(showNumber==0?0:showNumber);
				String contentFrom=resultSet.getString("出处");
				content.setContent_from(contentFrom==null?city_name:contentFrom);
				//content.setDocchannel(readItemIterator(resultSet.getInt("ITEM_ID")));
				
				System.out.println("文档标题："+content.getTitle_1());
				System.out.println("WRITE_DATE："+resultSet.getTimestamp("录入时间"));
				//System.out.println("WRITE_USER："+resultSet.getInt("WRITE_USER"));
				System.out.println("AUDIT_DATE："+resultSet.getTimestamp("生效时间"));
				//System.out.println("AUDIT_USER："+resultSet.getInt("AUDIT_USER"));
				System.out.println("ISSUE_DATE："+resultSet.getTimestamp("发文日期"));
				//System.out.println("ISSUE_USER："+resultSet.getInt("ISSUE_USER"));
				System.out.println("SHOW_NUMBER："+resultSet.getInt("点击"));
				System.out.println("CONTENT_FROM："+resultSet.getString("出处"));
				
				//调用附件读取方法，一个文档读取一次数据附件。
				//connect=DBConnection.getConnection();
				//content.setContentFiles(readContentFile(connect,content.getContentID()));
				//System.out.println("附件个数："+content.getContentFiles().size());
				
				System.out.println("==============文档读取完毕,,,,,,,,,,,,");
				
				//调用WCM文档推送方法，将封装好后的文档推送到新数据库中。
				
				try {
				   testSaveAll(content);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				System.out.println("当前处理记录是第："+countIndex);
				countIndex++;
				
			}
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			try {
				resultSet.close();
				statement.close();
				//connect.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		System.out.println("好无聊！");
	}
	
	/*
	 * 迁移公告数据
	 * */
	private void moveGongao(){
		String[] y_channel={"102"};
		x_channel=4707;
		city_name="四川省巴中市地方税务局";
		
		/*String sqlStr="select * from MAIN where code in(";
		StringBuffer sqlBuffer=new StringBuffer();
		
		for(int i=0;i<y_channel.length;i++){
			if(i==0){
				sqlBuffer.append(y_channel[i]);
			}else{
				sqlBuffer.append(","+y_channel[i]);
			}
			
		}
		sqlStr=sqlStr+sqlBuffer.toString()+")";*/
		
		String sqlStr="select * from mynews where NEWSTYPE like '图片新闻'";
		
		System.out.println("打印SQL语句："+sqlStr);
		
		int countIndex=1;
        statement=DBUitl.getStatement(DBConnection.getConnection());
		ResultSet resultSet=DBUitl.getResultSet(statement,sqlStr);
		try {
			while (resultSet.next()) {
				Content content=new Content();
				content.setContentID(resultSet.getInt("ID"));
				if(resultSet.getString("TITLE")==null){
					content.setTitle_1("暂无标题");
				}else{
					content.setTitle_1(resultSet.getString("TITLE"));
				}
				System.out.println("源文档编号："+content.getContentID());
				
				//String songContent=String.valueOf(resultSet.getLong("CONTENT"));
				//处理clob字段
				String songContent="";
				clob = (oracle.sql.CLOB) resultSet.getClob("CONTENT_2");
				if(clob!=null){
					BufferedReader in = new BufferedReader(clob.getCharacterStream());
					StringWriter out = new StringWriter();
					int c=0;
					try {
						while ((c = in.read()) != -1) {
						     out.write(c);
						}
						songContent=out.toString();
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						//e1.printStackTrace();
						System.out.println(".....+++此处文档异常...=======");
						songContent="暂无内容！";
					}
					
					
					try {
						out.close();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						//e.printStackTrace();
						System.out.println(".....+++此处文档输出流关闭异常...=======");
					}
					try {
						in.close();
					} catch (IOException e) {
						// TODO Auto-generated catch block
					    //e.printStackTrace();
						System.out.println(".....+++此处文档输入流关闭异常...=======");
					}
				}else{
					songContent="暂无内容";
				}
				
				if(songContent==null){
					content.setItem_text("");
				}else{
					System.out.println("内容："+songContent);
				    String itext = null;
					//itext = ClobToString(clob);
					itext=songContent;
					String item_text=DataContentUtil.replaceContentCharacter(itext);
					//String item_text="";
					
					content.setItem_text(item_text);
				}
				content.setWrite_date(resultSet.getTimestamp("ADDTIME"));
				//content.setWrite_user(resultSet.getInt("WRITE_USER"));
				content.setAudit_date(resultSet.getTimestamp("ADDTIME"));
				//content.setAudit_user(resultSet.getInt("AUDIT_USER"));
				content.setIssue_date(resultSet.getTimestamp("ADDTIME"));
				//content.setIssue_user(resultSet.getInt("ISSUE_USER"));
				int showNumber=resultSet.getInt("HITS");
				content.setShow_number(showNumber==0?0:showNumber);
				//String contentFrom=resultSet.getString("出处");
				//content.setContent_from(contentFrom==null?city_name:contentFrom);
				//content.setDocchannel(readItemIterator(resultSet.getInt("ITEM_ID")));
				
				System.out.println("文档标题："+content.getTitle_1());
				System.out.println("WRITE_DATE："+resultSet.getTimestamp("ADDTIME"));
				//System.out.println("WRITE_USER："+resultSet.getInt("WRITE_USER"));
				System.out.println("AUDIT_DATE："+resultSet.getTimestamp("ADDTIME"));
				//System.out.println("AUDIT_USER："+resultSet.getInt("AUDIT_USER"));
				System.out.println("ISSUE_DATE："+resultSet.getTimestamp("ADDTIME"));
				//System.out.println("ISSUE_USER："+resultSet.getInt("ISSUE_USER"));
				System.out.println("SHOW_NUMBER："+resultSet.getInt("HITS"));
				//System.out.println("CONTENT_FROM："+resultSet.getString("出处"));
				
				//调用附件读取方法，一个文档读取一次数据附件。
				//connect=DBConnection.getConnection();
				//content.setContentFiles(readContentFile(connect,content.getContentID()));
				//System.out.println("附件个数："+content.getContentFiles().size());
				
				System.out.println("==============文档读取完毕,,,,,,,,,,,,");
				
				//调用WCM文档推送方法，将封装好后的文档推送到新数据库中。
				
				try {
				   testSaveAll(content);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				System.out.println("当前处理记录是第："+countIndex);
				countIndex++;
			}
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			try {
				resultSet.close();
				statement.close();
				//connect.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		System.out.println("好无聊！");
	}
	
	/*
	 * 读取领导简介数据
	 * */
	private void moveLeader(){
		String[] y_channel={"102"};
		x_channel=4711;
		city_name="四川省巴中市地方税务局";
        String sqlStr="select * from leder";
		
		System.out.println("打印SQL语句："+sqlStr);
		
		int countIndex=1;
        statement=DBUitl.getStatement(DBConnection.getConnection());
		ResultSet resultSet=DBUitl.getResultSet(statement,sqlStr);
		try {
			while (resultSet.next()) {
				Content content=new Content();
				content.setContentID(resultSet.getInt("ID"));
				if(resultSet.getString("NAME")==null){
					content.setTitle_1("暂无标题");
				}else{
					content.setTitle_1(resultSet.getString("NAME"));
				}
				System.out.println("源文档编号："+content.getContentID());
				
				//String songContent=String.valueOf(resultSet.getLong("CONTENT"));
				//处理clob字段
				String songContent="";
				clob = (oracle.sql.CLOB) resultSet.getClob("CONTENT_1");
				if(clob!=null){
					BufferedReader in = new BufferedReader(clob.getCharacterStream());
					StringWriter out = new StringWriter();
					int c=0;
					try {
						while ((c = in.read()) != -1) {
						     out.write(c);
						}
						songContent=out.toString();
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						//e1.printStackTrace();
						System.out.println(".....+++此处文档异常...=======");
						songContent="暂无内容！";
					}
					
					
					try {
						out.close();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						//e.printStackTrace();
						System.out.println(".....+++此处文档输出流关闭异常...=======");
					}
					try {
						in.close();
					} catch (IOException e) {
						// TODO Auto-generated catch block
					    //e.printStackTrace();
						System.out.println(".....+++此处文档输入流关闭异常...=======");
					}
				}else{
					songContent="暂无内容";
				}
				
				if(songContent==null){
					content.setItem_text("");
				}else{
					
				    String itext = null;
					//itext = ClobToString(clob);
					itext=songContent;
					String item_text=DataContentUtil.replaceContentCharacter(itext);
					//String item_text="";
					String picInfoString=resultSet.getString("PIC");
					picInfoString="<img src=\"http://155.80.16.21"+picInfoString.replace("//", "/")+"\"/>";
					content.setItem_text(picInfoString+item_text);
					
					System.out.println("内容："+content.getItem_text());
				}
				SimpleDateFormat dateFormat=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				content.setWrite_date(dateFormat.parse("2005-10-01 10:10:00"));
				//content.setWrite_user(resultSet.getInt("WRITE_USER"));
				content.setAudit_date(dateFormat.parse("2005-10-01 10:11:00"));
				//content.setAudit_user(resultSet.getInt("AUDIT_USER"));
				content.setIssue_date(dateFormat.parse("2005-10-01 10:12:00"));
				//content.setIssue_user(resultSet.getInt("ISSUE_USER"));
				int showNumber=1000;
				content.setShow_number(showNumber==0?0:showNumber);
				//String contentFrom=resultSet.getString("出处");
				//content.setContent_from(contentFrom==null?city_name:contentFrom);
				//content.setDocchannel(readItemIterator(resultSet.getInt("ITEM_ID")));
				
				/*System.out.println("文档标题："+content.getTitle_1());
				System.out.println("WRITE_DATE："+resultSet.getTimestamp("ADDTIME"));
				//System.out.println("WRITE_USER："+resultSet.getInt("WRITE_USER"));
				System.out.println("AUDIT_DATE："+resultSet.getTimestamp("ADDTIME"));
				//System.out.println("AUDIT_USER："+resultSet.getInt("AUDIT_USER"));
				System.out.println("ISSUE_DATE："+resultSet.getTimestamp("ADDTIME"));
				//System.out.println("ISSUE_USER："+resultSet.getInt("ISSUE_USER"));
				System.out.println("SHOW_NUMBER："+resultSet.getInt("HITS"));*/
				//System.out.println("CONTENT_FROM："+resultSet.getString("出处"));
				
				//调用附件读取方法，一个文档读取一次数据附件。
				//connect=DBConnection.getConnection();
				//content.setContentFiles(readContentFile(connect,content.getContentID()));
				//System.out.println("附件个数："+content.getContentFiles().size());
				
				System.out.println("==============文档读取完毕,,,,,,,,,,,,");
				
				//调用WCM文档推送方法，将封装好后的文档推送到新数据库中。
				
				try {
				   testSaveAll(content);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				System.out.println("当前处理记录是第："+countIndex);
				countIndex++;
			}
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			try {
				resultSet.close();
				statement.close();
				//connect.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		System.out.println("好无聊！");
	}

	
	/*
	 * 根据旧数据库读取的文档，写入到新的数据库。
	 * */
	/**
	 * 保存文档及其所有相关项，包括引用栏目，附件，相关文档
	 * 
	 * @param _oDocument
	 * @throws Exception 
	 */
	private static void saveAll(WDocument _oDocument) throws Exception {
		long recordId=WDocumentMgr.save(_oDocument);
		WDocumentMgr.quoteTo(_oDocument);
		
		if(appendixFlag==true){
			try {
				WDocumentMgr.saveAppendixs(_oDocument);
				//WDocumentMgr.saveRelations(_oDocument);
			} catch (Exception e) {
				// TODO: handle exception
				System.out.println("妈的，附件后缀格式又不支持了！");
			}
			
		}
		//WDocumentMgr.saveAppendixs(_oDocument);
		//WDocumentMgr.saveRelations(_oDocument);
		
		
	}
	
	/** 附件类型标识：文档附件 */
    public final static int FLAG_DOCAPD = 10; // 文件:Document Appendix

    /** 附件类型标识：文档图片 */
    public final static int FLAG_DOCPIC = 20; // 图片:Document Picture

    /** 附件类型标识：HTML */
    public final static int FLAG_HTMLPIC = 30; // HTML图片:Html Picture

    /** 附件类型标识：链接附件 */
    public final static int FLAG_LINK = 40; // 链接:Link
    
    
	public static void testSaveAll(Content content) throws Exception {
		   System.out.println("=========进入文档写方法======");
		  WDocument oDocument = new WDocument();
		
		  oDocument.setFieldValue("ChannelId", x_channel);
		  oDocument.setFieldValue("ObjectId", new Integer(0));
		
		  oDocument.setFieldValue("DocTitle", content.getTitle_1());
		  oDocument.setFieldValue("DOCTYPE", new Integer(20));
		  System.out.println("content.getWrite_date()"+content.getWrite_date());
		  oDocument.setFieldValue("CRTIME", content.getWrite_date());
		  oDocument.setFieldValue("DOCRELTIME", content.getWrite_date());
		  oDocument.setFieldValue("DOCCONTENT", AppContentUtil.replaceSpecialCharacter(content.getItem_text()));
		  System.out.println("打印内容长度："+content.getItem_text().length());
		  oDocument.setFieldValue("DocHtmlCon", content.getItem_text());
		  oDocument.setFieldValue("DOCSOURCENAME ", content.getContent_from()==null?city_name:content.getContent_from());
		  oDocument.setFieldValue("HITSCOUNT ", content.getShow_number());
		  
		  
		  System.out.println("创建时间："+content.getWrite_date());
		  //System.out.println("发布时间："+content.getIssue_date());
		  System.out.println("撰写时间："+content.getWrite_date());
		  
		
		  /*if(content.getContentFiles().size()!=0||content.getContentFiles()!=null){
			  for(int i=0;i<content.getContentFiles().size();i++){
				  Map<String,Object> oAppendixMore=new HashMap<String, Object>();
				  oAppendixMore.put("APPDESC", content.getContentFiles().get(i).getFile_info());
				  String localFile="G://AppData/SCDS/Appendix/"+content.getContentFiles().get(i).getFile_name();
				  //oDocument.addAppendix(FLAG_DOCAPD, localFile);
				  oDocument.addAppendix(FLAG_DOCAPD, localFile, oAppendixMore);
				  //oDocument.addRelation(898);
				  //oDocument.addRelation(884);
			  }
			oDocument.uploadAllAppendixs();
			appendixFlag=true;
		  }*/
		
		  saveAll(oDocument);
	}
	
	private String ImagePathReplace(String src){
		src=src.replace("src=\"/UserFiles", "src=\"http://155.80.16.21/UserFiles");
		//src=src.replace("src=\"/", "src=\"http://155.16.16.60:7001/");
		//src=src.replace("src=\"/", "src=\"http://155.16.161.140/");
		
		/*巴中地税内网图片途径替换*/
		src=src.replace("src=\"/", "src=\"http://155.80.16.21/");
		
		return src;
	}
}
