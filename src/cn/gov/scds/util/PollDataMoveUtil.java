package cn.gov.scds.util;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import cn.gov.scds.entity.Poll;
import cn.gov.scds.jdbc.DBConnection;
import cn.gov.scds.jdbc.DBInteractionConnection;
import cn.gov.scds.jdbc.DBUitl;

public class PollDataMoveUtil {
    /*
     * 问卷调查模块的数据迁移
     * */
	public static Connection connect=null;
	public static Statement statement=null;
	public static ResultSet resultSet=null;
	private static int recordId=2148;
	
	/*
	 * 数据迁移方法
	 * */
	public void dataMove(){
		readDc_projectByJDBC();
	}
	
	/*
	 * 读取调查模块中的调查项目表
	 * */
	private void readDc_projectByJDBC(){
		connect=DBConnection.getConnection();
		statement=DBUitl.getStatement(connect);
		
		Connection iconnection=DBInteractionConnection.getConnection();
		Statement iStatement=DBUitl.getStatement(iconnection);
		
		String sqlStr="select * from DC_PROJECT where ZF=0 order by SHOW_ORDER";
		ResultSet resultSet=DBUitl.getResultSet(statement,sqlStr);
		try {
			while(resultSet.next()){
				Poll poll=new Poll();
				poll.setPollId(resultSet.getInt("PROJECT_ID")+2148);
				poll.setPollTitle(resultSet.getString("PROJECT_NAME"));
				poll.setPollMessage(resultSet.getString("PROJECT_DESC"));
				poll.setChannelId(1);
				
				System.out.println("调查项目编号："+poll.getPollId());
				System.out.println("调查项目标题："+poll.getPollTitle());
				System.out.println("调查项目描述："+poll.getPollMessage());
				System.out.println("===========分割线========");
				
				writeDc_projectByJDBC(poll,iStatement);
			  }
			}catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	}
	
	/*
	 * 写入调查模块的调查项目表
	 * */
	private void writeDc_projectByJDBC(Poll poll,Statement iStatement){
		String sqlStr="insert into Poll (pollid,polltitle,pollmessage,channelid) values ("+poll.getPollId()+",'"+poll.getPollTitle()+"','"+poll.getPollMessage()+"',"+poll.getChannelId()+")";
		try {
			iStatement.executeUpdate(sqlStr);
			System.out.println("=========数据导入成功.......");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		recordId++;
	}
}
