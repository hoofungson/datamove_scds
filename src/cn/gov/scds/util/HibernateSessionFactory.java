package cn.gov.scds.util;

import org.hibernate.cfg.Configuration;

public class HibernateSessionFactory {
	private static Configuration configuration=new Configuration().configure();

	public static Configuration getConfiguration() {
		return configuration;
	}
}
