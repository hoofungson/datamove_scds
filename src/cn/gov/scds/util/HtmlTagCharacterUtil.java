package cn.gov.scds.util;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/*
 * 字符串过滤工具
 * */
public class HtmlTagCharacterUtil {
	
	private static List<String> htmlTags;
	private static HtmlRegexpUtil1 htmlRegexpUtil;
     /*
      * 过滤方法
      * */
	
	public static List<String> getHtmlTags() {
		return htmlTags;
	}


	public static void setHtmlTags(List<String> htmlTags) {
		HtmlTagCharacterUtil.htmlTags = htmlTags;
	}


	public static String filterHtmlTagCharacter(String content){
		htmlRegexpUtil=new HtmlRegexpUtil1();
		/*content=content.replace("<p", "&lt;p");
		content=content.replace("</p>", "&lt;/p&gt");
		content=content.replace("<img", "&lt;img");
		content=content.replace("/>", "/&gt");
		content=content.replace("<a", "&lt;a");
		content=content.replace("</a>", "&lt;a&gt");
		content=content.replace("<embed", "&lt;embed");*/
		//content=htmlRegexpUtil.filterHtml(content);
		
		/*content=content.replace("&lt;p", "<p");
		content=content.replace("&lt;/p&gt", "</p>");
		content=content.replace("&lt;img", "<img");
		content=content.replace("/&gt", "/>");
		content=content.replace("&lt;a", "<a");
		content=content.replace("&lt;a&gt", "</a>");
		content=content.replace("&lt;embed", "<embed");*/
		
		//content=HtmlTagCharacterUtil.filterHtmlTagCharacter(content);
		//content=htmlRegexpUtil.replaceHtmlTag2(content, "p", "style", "<p>", "");
		
		return content;
	}
	
	public static String replaceStr(String content,String regex,String rStr,String brStr){
		htmlRegexpUtil=new HtmlRegexpUtil1();
		try {
			if(!rStr.isEmpty()){
				content=content.replace(brStr, rStr);
			}
			//Pattern pat = Pattern.compile(regex);
			Pattern pat = Pattern.compile(regex, Pattern.UNICODE_CASE);
			Matcher matcher = pat.matcher(content);
			List<String> reStrings=new ArrayList<String>();
			while (matcher.find()) {
				String temp = content.substring(matcher.start(),matcher.end());
				reStrings.add(matcher.group());
			}
			for(String string:reStrings){
				content=content.replace(string, "");
			}
			//content=htmlRegexpUtil.replaceHtmlTag2(content, "font", "", "", "");
			/*content=htmlRegexpUtil.fiterHtmlTag(content, "font");
			content=content.replace("</font>", "");
			content=htmlRegexpUtil.fiterHtmlTag(content, "FONT");
			content=content.replace("</FONT>", "");
			content=fun(content);*/
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("打印异常了=============");
		}
		
		
		return content;
	}
	
	private static String fun(String content){
		int flagIndex=0;
		   String regex = "^<p>(&nbsp;)*+</p>";
		   String str = content;
		   Pattern pat = Pattern.compile(regex);
		   Matcher matcher = pat.matcher(str);
		   //System.out.println(matcher.groupCount());
		   int i=1;
		   List<String> reStrings=new ArrayList<String>();
		   while (matcher.find()&&flagIndex==0) {
			   
			 //System.out.println("第："+(flagIndex+1)+"次替换");
		     String temp = str.substring(matcher.start(),matcher.end());
		     System.out.println(matcher.group());
		     if(flagIndex==0){
		    	 reStrings.add(matcher.group(0));
		     }
		     flagIndex++;
		     //i++;
		   }
		   
		   for(String string:reStrings){
			   str=str.replace(string, "");
		   }
		
		return str;
	}
}
