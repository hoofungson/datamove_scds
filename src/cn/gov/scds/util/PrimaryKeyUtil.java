package cn.gov.scds.util;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import cn.gov.scds.jdbc.DBConnection;
import cn.gov.scds.jdbc.DBInteractionConnection;
import cn.gov.scds.jdbc.DBUitl;

public class PrimaryKeyUtil {
    /*
     * 主键工具
     * */
	
	/*主键子增长*/
	public int getProgressivelyKey(String sql){
		int key=0;
		Connection connection=null;
		Statement statement=null;
		ResultSet resultSet=null;
		try {
		    /*Connection connect=DBConnection.getConnection();*/
			connection=DBInteractionConnection.getConnection();
			statement = connection.createStatement();
			resultSet=statement.executeQuery(sql);
			while (resultSet.next()) {
				key=resultSet.getInt(1);
				
			}
		
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			try {
				resultSet.close();
				statement.close();
				connection.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
			
		return key+1;
	}
}
