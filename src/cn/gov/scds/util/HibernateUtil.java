package cn.gov.scds.util;

import org.hibernate.SessionFactory;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.ServiceRegistryBuilder;

public class HibernateUtil {
	private static final SessionFactory sessionFactory;
	
	static{
		try {
			ServiceRegistry  sr = new ServiceRegistryBuilder().applySettings(HibernateSessionFactory.getConfiguration().getProperties()).buildServiceRegistry();    
	        sessionFactory = HibernateSessionFactory.getConfiguration().buildSessionFactory(sr);
		} catch (Throwable ex) {
			// TODO: handle exception
			System.out.println("会话创建失败！");
			throw new ExceptionInInitializerError(ex);
		}
	}

	public static SessionFactory getSessionFactory() {
		
		return sessionFactory;
	}
}
