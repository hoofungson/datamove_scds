package cn.gov.scds.util;

import java.util.ArrayList;
import java.util.List;

/*
 * 正文链接部分替换工具
 * */
public class LinkUtil {
	 /*
	  * 声明链接替换字符串列表
	  * */
    private static List<String> links;

	public static List<String> getLinks() {
		return links;
	}

	public static void setLinks(List<String> links) {
		LinkUtil.links = links;
	}
     
	/*
	 * 初始化链接替换字符串列表
	 * */
    private static boolean initLinks(){
    	List<String> lStrings=new ArrayList<String>();
    	lStrings.add("href=\"/");
    	lStrings.add("href=\"http://www.chinatax.gov.cn/");
    	setLinks(lStrings);
    	
    	return true;
    }
    
    /*
     * 替换链接字符串列表
     * */
    public static String replaceLinkCharacter(String content){
    	if(initLinks()){
    		for(String hrefStr:links){
        		content=content.replace(hrefStr, "href=\"http://155.16.16.60:7001/");
        	}
    	}
    	
    	
    	return content;
    }
}
