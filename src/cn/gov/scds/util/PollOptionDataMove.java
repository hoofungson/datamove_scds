package cn.gov.scds.util;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import cn.gov.scds.entity.Poll;
import cn.gov.scds.entity.PollOption;
import cn.gov.scds.jdbc.DBConnection;
import cn.gov.scds.jdbc.DBInteractionConnection;
import cn.gov.scds.jdbc.DBUitl;

public class PollOptionDataMove {
	/*
     * 问卷调查模块的数据迁移
     * */
	public static Connection connect=null;
	public static Statement statement=null;
	public static ResultSet resultSet=null;
	private static int recordId=3000;
	
	/*
	 * 数据迁移方法
	 * */
	public void dataMove(){
		readDc_projectByJDBC();
	}
	
	/*
	 * 读取调查模块中的调查项目表
	 * */
	private void readDc_projectByJDBC(){
		connect=DBConnection.getConnection();
		statement=DBUitl.getStatement(connect);
		
		Connection iconnection=DBInteractionConnection.getConnection();
		Statement iStatement=DBUitl.getStatement(iconnection);
		
		String sqlStr="select * from dc_voteitem where ZF=0";
		ResultSet resultSet=DBUitl.getResultSet(statement,sqlStr);
		try {
			while(resultSet.next()){
				PollOption poll=new PollOption();
				poll.setPollOptionID(resultSet.getInt("ITEM_ID")+4600);
				poll.setPollGroupID(resultSet.getInt("VOTE_ID")+2148);
				poll.setOptionTitle(resultSet.getString("ITEM_TITLE"));
				
				System.out.println("调查项目编号："+poll.getPollOptionID());
				System.out.println("调查项目标题："+poll.getPollGroupID());
				System.out.println("调查项目描述："+poll.getOptionTitle());
				System.out.println("===========分割线========");
				
				writeDc_projectByJDBC(poll,iStatement);
			  }
			}catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	}
	
	/*
	 * 写入调查模块的调查项目表
	 * */
	private void writeDc_projectByJDBC(PollOption poll,Statement iStatement){
		String sqlStr="insert into PollOption (polloptionid,pollgroupid,optiontitle) values ("+poll.getPollOptionID()+","+poll.getPollGroupID()+",'"+poll.getOptionTitle()+"')";
		try {
			iStatement.executeUpdate(sqlStr);
			System.out.println("=========数据导入成功.......");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		recordId++;
	}
}
