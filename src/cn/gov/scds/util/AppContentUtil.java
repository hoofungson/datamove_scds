package cn.gov.scds.util;

import java.util.ArrayList;
import java.util.List;

/*
 * APP正文内容，替换工具。
 * */
public class AppContentUtil {
	 /*
	  * 声明特殊字符串列表。
	  * */
	 private static List<String> specialCharacters;
	
     public static List<String> getSpecialCharacters() {
		return specialCharacters;
	}
	public static void setSpecialCharacters(List<String> specialCharacters) {
		AppContentUtil.specialCharacters = specialCharacters;
	}

	/*
	 * 初始化特殊字符串列表。
	 * */
    private static boolean initCharacter(){
    	List<String> mStrings=new ArrayList<String>();
    	mStrings.add("&nbsp;");
    	mStrings.add("&mdash;");
    	setSpecialCharacters(mStrings);
    	
    	return true;
    }

    /*
     * 替换特殊字符串。
     * */
	public static String replaceSpecialCharacter(String content){
    	content=HtmlRegexpUtil.filterHtml(content);
    	if(initCharacter()){
    		for(String mcontent:getSpecialCharacters()){
        		content=content.replace(mcontent, "");
        	}
    	}
    	
		
		return content;
     }
}
