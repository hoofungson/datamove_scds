package cn.gov.scds.util;

public class TreeNode {
      private int nodeId;
      private int parentId;
      private String nodeName;
      
	public int getNodeId() {
		return nodeId;
	}
	public void setNodeId(int nodeId) {
		this.nodeId = nodeId;
	}
	public int getParentId() {
		return parentId;
	}
	public void setParentId(int parentId) {
		this.parentId = parentId;
	}
	public String getNodeName() {
		return nodeName;
	}
	public void setNodeName(String nodeName) {
		this.nodeName = nodeName;
	}
	public TreeNode(int nodeId) {
		super();
		this.nodeId = nodeId;
	}
	public TreeNode(int nodeId, int parentId) {
		super();
		this.nodeId = nodeId;
		this.parentId = parentId;
	}
	
      
      
}
