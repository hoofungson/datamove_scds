package cn.gov.scds.entity;

import java.util.Date;

public class JEmialBox {
     private int mailId;//信件编号
     private String comemMails;//来信人
     private Date comeMailDate;//来信日期
     private String mailContent;//信件内容
     private String mailEmail;//信件邮箱
     private String mailTitle;//信件标题
     private Date replyDate;
     private String replyIdea;
     
	public JEmialBox() {
		super();
		// TODO Auto-generated constructor stub
	}

	public JEmialBox(int mailId, String comemMails, Date comeMailDate,
			String mailContent, String mailEmail, String mailTitle,
			Date replyDate, String replyIdea) {
		super();
		this.mailId = mailId;
		this.comemMails = comemMails;
		this.comeMailDate = comeMailDate;
		this.mailContent = mailContent;
		this.mailEmail = mailEmail;
		this.mailTitle = mailTitle;
		this.replyDate = replyDate;
		this.replyIdea = replyIdea;
	}

	public int getMailId() {
		return mailId;
	}

	public void setMailId(int mailId) {
		this.mailId = mailId;
	}

	public String getComemMails() {
		return comemMails;
	}

	public void setComemMails(String comemMails) {
		this.comemMails = comemMails;
	}

	public Date getComeMailDate() {
		return comeMailDate;
	}

	public void setComeMailDate(Date comeMailDate) {
		this.comeMailDate = comeMailDate;
	}

	public String getMailContent() {
		return mailContent;
	}

	public void setMailContent(String mailContent) {
		this.mailContent = mailContent;
	}

	public String getMailEmail() {
		return mailEmail;
	}

	public void setMailEmail(String mailEmail) {
		this.mailEmail = mailEmail;
	}

	public String getMailTitle() {
		return mailTitle;
	}

	public void setMailTitle(String mailTitle) {
		this.mailTitle = mailTitle;
	}

	public Date getReplyDate() {
		return replyDate;
	}

	public void setReplyDate(Date replyDate) {
		this.replyDate = replyDate;
	}

	public String getReplyIdea() {
		return replyIdea;
	}

	public void setReplyIdea(String replyIdea) {
		this.replyIdea = replyIdea;
	}
     
     
}
