package cn.gov.scds.entity;

import java.io.Serializable;

public class ContentFile implements Serializable{
     private int file_id;
     private int content_id;
     private String file_name;
     private String file_info;
     private int file_size;
     private String file_text;
     
     
	public int getFile_id() {
		return file_id;
	}
	public void setFile_id(int file_id) {
		this.file_id = file_id;
	}
	public int getContent_id() {
		return content_id;
	}
	public void setContent_id(int content_id) {
		this.content_id = content_id;
	}
	public String getFile_name() {
		return file_name;
	}
	public void setFile_name(String file_name) {
		this.file_name = file_name;
	}
	public String getFile_info() {
		return file_info;
	}
	public void setFile_info(String file_info) {
		this.file_info = file_info;
	}
	public int getFile_size() {
		return file_size;
	}
	public void setFile_size(int file_size) {
		this.file_size = file_size;
	}
	public String getFile_text() {
		return file_text;
	}
	public void setFile_text(String file_text) {
		this.file_text = file_text;
	}
	public ContentFile() {
		super();
		// TODO Auto-generated constructor stub
	}
	public ContentFile(int file_id, int content_id, String file_name,
			String file_info, int file_size, String file_text) {
		super();
		this.file_id = file_id;
		this.content_id = content_id;
		this.file_name = file_name;
		this.file_info = file_info;
		this.file_size = file_size;
		this.file_text = file_text;
	}
     
     
}
