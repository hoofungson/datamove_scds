package cn.gov.scds.entity;

public class PollOption {
     private int pollOptionID;
     private int pollGroupID;
     private String optionTitle;
     
	public int getPollOptionID() {
		return pollOptionID;
	}
	public void setPollOptionID(int pollOptionID) {
		this.pollOptionID = pollOptionID;
	}
	public int getPollGroupID() {
		return pollGroupID;
	}
	public void setPollGroupID(int pollGroupID) {
		this.pollGroupID = pollGroupID;
	}
	public String getOptionTitle() {
		return optionTitle;
	}
	public void setOptionTitle(String optionTitle) {
		this.optionTitle = optionTitle;
	}
     
     
}
