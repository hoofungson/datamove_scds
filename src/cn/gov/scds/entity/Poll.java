package cn.gov.scds.entity;

public class Poll {
    private int pollId;
    private String pollTitle;
    private String pollMessage;
    private int channelId;
	public Poll() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Poll(int pollId, String pollTitle, String pollMessage, int channelId) {
		super();
		this.pollId = pollId;
		this.pollTitle = pollTitle;
		this.pollMessage = pollMessage;
		this.channelId = channelId;
	}
	public int getPollId() {
		return pollId;
	}
	public void setPollId(int pollId) {
		this.pollId = pollId;
	}
	public String getPollTitle() {
		return pollTitle;
	}
	public void setPollTitle(String pollTitle) {
		this.pollTitle = pollTitle;
	}
	public String getPollMessage() {
		return pollMessage;
	}
	public void setPollMessage(String pollMessage) {
		this.pollMessage = pollMessage;
	}
	public int getChannelId() {
		return channelId;
	}
	public void setChannelId(int channelId) {
		this.channelId = channelId;
	}
    
    
}
