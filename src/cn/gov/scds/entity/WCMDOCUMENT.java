/**
 * 
 */
package cn.gov.scds.entity;

import java.io.Serializable;
import java.util.Date;


/**
 * @ClassName WCMDOCUMENT
 * @author hoofungson
 * @Description TODO
 * @Date 2015年5月27日上午11:28:44
 */
public class WCMDOCUMENT implements Serializable{
    /*
     * 文档实体。
     * */
	
	/*成员属性声明*/
	private Integer DOCID;
	private Integer DOCCHANNEL;
	private Integer DOCVERSION;
	private Integer DOCTYPE;
	private String DOCTITLE;
	private Integer DOCSOURCE;
	private Integer DOCSECURITY;
	private Integer DOCSTATUS;
	private Long DOCKIND;
	private String DOCCONTENT;
	private String DOCHTMLCON;
	private String DOCABSTRACT;
	private String DOCKEYWORDS;
	private String DOCRELWORDS;
	private String DOCPEOPLE;
	private String DOCPLACE;
	private String DOCAUTHOR;
	private String DOCEDITOR;
	private String DOCAUDITOR;
	private Integer DOCOUTUPID;
	private Date DOCVALID;
	private String DOCPUBURL;
	private Date DOCPUBTIME;
	private Date DOCRELTIME;
	private String CRUSER;
	private Date CRTIME;
	private Integer DOCWORDSCOUNT;
	private Integer DOCPRO;
	private Integer RIGHTDEFINED;
	private String TITLECOLOR;
	private Integer TEMPLATEID;
	private Integer SCHEDULE;
	private String DOCNO;
	private Integer DOCFLAG;
	private String EDITOR;
	private String ATTRIBUTE;
	private Integer HITSCOUNT;
	private String DOCPUBHTMLCON;
	private String SUBDOCTITLE;
	private Integer ATTACHPIC;
	private String DOCLINK;
	private String DOCFILENAME;
	private Integer DOCFROMVERSION;
	private Date OPERTIME;
	private String OPERUSER;
	private String FLOWOPERATIONMARK;
	private String FLOWPREOPERATIONMARK;
	private String FLOWOPERATIONMASKENUM;
	private String DOCSOURCENAME;
	private String DOCLINKTO;
	private String DOCMIRRORTO;
	private String RANDOMSERIAL;
	private String POSTUSER;
	private Integer ISPAGEIMG;
	private String PUBLISHDATE;
	private String PAGENUM;
	private String PAGENAME;
	private String PDFFILENAME;
	private String PAGEIMAGEFILENAME;
	private String MAP;
	private String YINTI;
	private Integer SITEID;
	private Integer SRCSITEID;
	private Date DOCFIRSTPUBTIME;
	private Integer NODEID;
	private Integer ORDERID;
	private String CRDEPT;
	private Integer DOCFORM;
	private Integer DOCLEVEL;
	private String OLDDOCPUBURL;
	private String TALKING_TITLE;
	private Date TALKING_TIME;
	private String TALKING_MAN;
	private String TALKING_ADD;
	/**
	 * @return the dOCID
	 */
	public Integer getDOCID() {
		return DOCID;
	}
	/**
	 * @param dOCID the dOCID to set
	 */
	public void setDOCID(Integer dOCID) {
		DOCID = dOCID;
	}
	/**
	 * @return the dOCCHANNEL
	 */
	public Integer getDOCCHANNEL() {
		return DOCCHANNEL;
	}
	/**
	 * @param dOCCHANNEL the dOCCHANNEL to set
	 */
	public void setDOCCHANNEL(Integer dOCCHANNEL) {
		DOCCHANNEL = dOCCHANNEL;
	}
	/**
	 * @return the dOCVERSION
	 */
	public Integer getDOCVERSION() {
		return DOCVERSION;
	}
	/**
	 * @param dOCVERSION the dOCVERSION to set
	 */
	public void setDOCVERSION(Integer dOCVERSION) {
		DOCVERSION = dOCVERSION;
	}
	/**
	 * @return the dOCTYPE
	 */
	public Integer getDOCTYPE() {
		return DOCTYPE;
	}
	/**
	 * @param dOCTYPE the dOCTYPE to set
	 */
	public void setDOCTYPE(Integer dOCTYPE) {
		DOCTYPE = dOCTYPE;
	}
	/**
	 * @return the dOCTITLE
	 */
	public String getDOCTITLE() {
		return DOCTITLE;
	}
	/**
	 * @param dOCTITLE the dOCTITLE to set
	 */
	public void setDOCTITLE(String dOCTITLE) {
		DOCTITLE = dOCTITLE;
	}
	/**
	 * @return the dOCSOURCE
	 */
	public Integer getDOCSOURCE() {
		return DOCSOURCE;
	}
	/**
	 * @param dOCSOURCE the dOCSOURCE to set
	 */
	public void setDOCSOURCE(Integer dOCSOURCE) {
		DOCSOURCE = dOCSOURCE;
	}
	/**
	 * @return the dOCSECURITY
	 */
	public Integer getDOCSECURITY() {
		return DOCSECURITY;
	}
	/**
	 * @param dOCSECURITY the dOCSECURITY to set
	 */
	public void setDOCSECURITY(Integer dOCSECURITY) {
		DOCSECURITY = dOCSECURITY;
	}
	/**
	 * @return the dOCSTATUS
	 */
	public Integer getDOCSTATUS() {
		return DOCSTATUS;
	}
	/**
	 * @param dOCSTATUS the dOCSTATUS to set
	 */
	public void setDOCSTATUS(Integer dOCSTATUS) {
		DOCSTATUS = dOCSTATUS;
	}
	/**
	 * @return the dOCKIND
	 */
	public Long getDOCKIND() {
		return DOCKIND;
	}
	/**
	 * @param dOCKIND the dOCKIND to set
	 */
	public void setDOCKIND(Long dOCKIND) {
		DOCKIND = dOCKIND;
	}
	/**
	 * @return the dOCCONTENT
	 */
	public String getDOCCONTENT() {
		return DOCCONTENT;
	}
	/**
	 * @param dOCCONTENT the dOCCONTENT to set
	 */
	public void setDOCCONTENT(String dOCCONTENT) {
		DOCCONTENT = dOCCONTENT;
	}
	/**
	 * @return the dOCHTMLCON
	 */
	public String getDOCHTMLCON() {
		return DOCHTMLCON;
	}
	/**
	 * @param dOCHTMLCON the dOCHTMLCON to set
	 */
	public void setDOCHTMLCON(String dOCHTMLCON) {
		DOCHTMLCON = dOCHTMLCON;
	}
	/**
	 * @return the dOCABSTRACT
	 */
	public String getDOCABSTRACT() {
		return DOCABSTRACT;
	}
	/**
	 * @param dOCABSTRACT the dOCABSTRACT to set
	 */
	public void setDOCABSTRACT(String dOCABSTRACT) {
		DOCABSTRACT = dOCABSTRACT;
	}
	/**
	 * @return the dOCKEYWORDS
	 */
	public String getDOCKEYWORDS() {
		return DOCKEYWORDS;
	}
	/**
	 * @param dOCKEYWORDS the dOCKEYWORDS to set
	 */
	public void setDOCKEYWORDS(String dOCKEYWORDS) {
		DOCKEYWORDS = dOCKEYWORDS;
	}
	/**
	 * @return the dOCRELWORDS
	 */
	public String getDOCRELWORDS() {
		return DOCRELWORDS;
	}
	/**
	 * @param dOCRELWORDS the dOCRELWORDS to set
	 */
	public void setDOCRELWORDS(String dOCRELWORDS) {
		DOCRELWORDS = dOCRELWORDS;
	}
	/**
	 * @return the dOCPEOPLE
	 */
	public String getDOCPEOPLE() {
		return DOCPEOPLE;
	}
	/**
	 * @param dOCPEOPLE the dOCPEOPLE to set
	 */
	public void setDOCPEOPLE(String dOCPEOPLE) {
		DOCPEOPLE = dOCPEOPLE;
	}
	/**
	 * @return the dOCPLACE
	 */
	public String getDOCPLACE() {
		return DOCPLACE;
	}
	/**
	 * @param dOCPLACE the dOCPLACE to set
	 */
	public void setDOCPLACE(String dOCPLACE) {
		DOCPLACE = dOCPLACE;
	}
	/**
	 * @return the dOCAUTHOR
	 */
	public String getDOCAUTHOR() {
		return DOCAUTHOR;
	}
	/**
	 * @param dOCAUTHOR the dOCAUTHOR to set
	 */
	public void setDOCAUTHOR(String dOCAUTHOR) {
		DOCAUTHOR = dOCAUTHOR;
	}
	/**
	 * @return the dOCEDITOR
	 */
	public String getDOCEDITOR() {
		return DOCEDITOR;
	}
	/**
	 * @param dOCEDITOR the dOCEDITOR to set
	 */
	public void setDOCEDITOR(String dOCEDITOR) {
		DOCEDITOR = dOCEDITOR;
	}
	/**
	 * @return the dOCAUDITOR
	 */
	public String getDOCAUDITOR() {
		return DOCAUDITOR;
	}
	/**
	 * @param dOCAUDITOR the dOCAUDITOR to set
	 */
	public void setDOCAUDITOR(String dOCAUDITOR) {
		DOCAUDITOR = dOCAUDITOR;
	}
	/**
	 * @return the dOCOUTUPID
	 */
	public Integer getDOCOUTUPID() {
		return DOCOUTUPID;
	}
	/**
	 * @param dOCOUTUPID the dOCOUTUPID to set
	 */
	public void setDOCOUTUPID(Integer dOCOUTUPID) {
		DOCOUTUPID = dOCOUTUPID;
	}
	/**
	 * @return the dOCVALID
	 */
	public Date getDOCVALID() {
		return DOCVALID;
	}
	/**
	 * @param dOCVALID the dOCVALID to set
	 */
	public void setDOCVALID(Date dOCVALID) {
		DOCVALID = dOCVALID;
	}
	/**
	 * @return the dOCPUBURL
	 */
	public String getDOCPUBURL() {
		return DOCPUBURL;
	}
	/**
	 * @param dOCPUBURL the dOCPUBURL to set
	 */
	public void setDOCPUBURL(String dOCPUBURL) {
		DOCPUBURL = dOCPUBURL;
	}
	/**
	 * @return the dOCPUBTIME
	 */
	public Date getDOCPUBTIME() {
		return DOCPUBTIME;
	}
	/**
	 * @param dOCPUBTIME the dOCPUBTIME to set
	 */
	public void setDOCPUBTIME(Date dOCPUBTIME) {
		DOCPUBTIME = dOCPUBTIME;
	}
	/**
	 * @return the dOCRELTIME
	 */
	public Date getDOCRELTIME() {
		return DOCRELTIME;
	}
	/**
	 * @param dOCRELTIME the dOCRELTIME to set
	 */
	public void setDOCRELTIME(Date dOCRELTIME) {
		DOCRELTIME = dOCRELTIME;
	}
	/**
	 * @return the cRUSER
	 */
	public String getCRUSER() {
		return CRUSER;
	}
	/**
	 * @param cRUSER the cRUSER to set
	 */
	public void setCRUSER(String cRUSER) {
		CRUSER = cRUSER;
	}
	/**
	 * @return the cRTIME
	 */
	public Date getCRTIME() {
		return CRTIME;
	}
	/**
	 * @param cRTIME the cRTIME to set
	 */
	public void setCRTIME(Date cRTIME) {
		CRTIME = cRTIME;
	}
	/**
	 * @return the dOCWORDSCOUNT
	 */
	public Integer getDOCWORDSCOUNT() {
		return DOCWORDSCOUNT;
	}
	/**
	 * @param dOCWORDSCOUNT the dOCWORDSCOUNT to set
	 */
	public void setDOCWORDSCOUNT(Integer dOCWORDSCOUNT) {
		DOCWORDSCOUNT = dOCWORDSCOUNT;
	}
	/**
	 * @return the dOCPRO
	 */
	public Integer getDOCPRO() {
		return DOCPRO;
	}
	/**
	 * @param dOCPRO the dOCPRO to set
	 */
	public void setDOCPRO(Integer dOCPRO) {
		DOCPRO = dOCPRO;
	}
	/**
	 * @return the rIGHTDEFINED
	 */
	public Integer getRIGHTDEFINED() {
		return RIGHTDEFINED;
	}
	/**
	 * @param rIGHTDEFINED the rIGHTDEFINED to set
	 */
	public void setRIGHTDEFINED(Integer rIGHTDEFINED) {
		RIGHTDEFINED = rIGHTDEFINED;
	}
	/**
	 * @return the tITLECOLOR
	 */
	public String getTITLECOLOR() {
		return TITLECOLOR;
	}
	/**
	 * @param tITLECOLOR the tITLECOLOR to set
	 */
	public void setTITLECOLOR(String tITLECOLOR) {
		TITLECOLOR = tITLECOLOR;
	}
	/**
	 * @return the tEMPLATEID
	 */
	public Integer getTEMPLATEID() {
		return TEMPLATEID;
	}
	/**
	 * @param tEMPLATEID the tEMPLATEID to set
	 */
	public void setTEMPLATEID(Integer tEMPLATEID) {
		TEMPLATEID = tEMPLATEID;
	}
	/**
	 * @return the sCHEDULE
	 */
	public Integer getSCHEDULE() {
		return SCHEDULE;
	}
	/**
	 * @param sCHEDULE the sCHEDULE to set
	 */
	public void setSCHEDULE(Integer sCHEDULE) {
		SCHEDULE = sCHEDULE;
	}
	/**
	 * @return the dOCNO
	 */
	public String getDOCNO() {
		return DOCNO;
	}
	/**
	 * @param dOCNO the dOCNO to set
	 */
	public void setDOCNO(String dOCNO) {
		DOCNO = dOCNO;
	}
	/**
	 * @return the dOCFLAG
	 */
	public Integer getDOCFLAG() {
		return DOCFLAG;
	}
	/**
	 * @param dOCFLAG the dOCFLAG to set
	 */
	public void setDOCFLAG(Integer dOCFLAG) {
		DOCFLAG = dOCFLAG;
	}
	/**
	 * @return the eDITOR
	 */
	public String getEDITOR() {
		return EDITOR;
	}
	/**
	 * @param eDITOR the eDITOR to set
	 */
	public void setEDITOR(String eDITOR) {
		EDITOR = eDITOR;
	}
	/**
	 * @return the aTTRIBUTE
	 */
	public String getATTRIBUTE() {
		return ATTRIBUTE;
	}
	/**
	 * @param aTTRIBUTE the aTTRIBUTE to set
	 */
	public void setATTRIBUTE(String aTTRIBUTE) {
		ATTRIBUTE = aTTRIBUTE;
	}
	/**
	 * @return the hITSCOUNT
	 */
	public Integer getHITSCOUNT() {
		return HITSCOUNT;
	}
	/**
	 * @param hITSCOUNT the hITSCOUNT to set
	 */
	public void setHITSCOUNT(Integer hITSCOUNT) {
		HITSCOUNT = hITSCOUNT;
	}
	/**
	 * @return the dOCPUBHTMLCON
	 */
	public String getDOCPUBHTMLCON() {
		return DOCPUBHTMLCON;
	}
	/**
	 * @param dOCPUBHTMLCON the dOCPUBHTMLCON to set
	 */
	public void setDOCPUBHTMLCON(String dOCPUBHTMLCON) {
		DOCPUBHTMLCON = dOCPUBHTMLCON;
	}
	/**
	 * @return the sUBDOCTITLE
	 */
	public String getSUBDOCTITLE() {
		return SUBDOCTITLE;
	}
	/**
	 * @param sUBDOCTITLE the sUBDOCTITLE to set
	 */
	public void setSUBDOCTITLE(String sUBDOCTITLE) {
		SUBDOCTITLE = sUBDOCTITLE;
	}
	/**
	 * @return the aTTACHPIC
	 */
	public Integer getATTACHPIC() {
		return ATTACHPIC;
	}
	/**
	 * @param aTTACHPIC the aTTACHPIC to set
	 */
	public void setATTACHPIC(Integer aTTACHPIC) {
		ATTACHPIC = aTTACHPIC;
	}
	/**
	 * @return the dOCLINK
	 */
	public String getDOCLINK() {
		return DOCLINK;
	}
	/**
	 * @param dOCLINK the dOCLINK to set
	 */
	public void setDOCLINK(String dOCLINK) {
		DOCLINK = dOCLINK;
	}
	/**
	 * @return the dOCFILENAME
	 */
	public String getDOCFILENAME() {
		return DOCFILENAME;
	}
	/**
	 * @param dOCFILENAME the dOCFILENAME to set
	 */
	public void setDOCFILENAME(String dOCFILENAME) {
		DOCFILENAME = dOCFILENAME;
	}
	/**
	 * @return the dOCFROMVERSION
	 */
	public Integer getDOCFROMVERSION() {
		return DOCFROMVERSION;
	}
	/**
	 * @param dOCFROMVERSION the dOCFROMVERSION to set
	 */
	public void setDOCFROMVERSION(Integer dOCFROMVERSION) {
		DOCFROMVERSION = dOCFROMVERSION;
	}
	/**
	 * @return the oPERTIME
	 */
	public Date getOPERTIME() {
		return OPERTIME;
	}
	/**
	 * @param oPERTIME the oPERTIME to set
	 */
	public void setOPERTIME(Date oPERTIME) {
		OPERTIME = oPERTIME;
	}
	/**
	 * @return the oPERUSER
	 */
	public String getOPERUSER() {
		return OPERUSER;
	}
	/**
	 * @param oPERUSER the oPERUSER to set
	 */
	public void setOPERUSER(String oPERUSER) {
		OPERUSER = oPERUSER;
	}
	/**
	 * @return the fLOWOPERATIONMARK
	 */
	public String getFLOWOPERATIONMARK() {
		return FLOWOPERATIONMARK;
	}
	/**
	 * @param fLOWOPERATIONMARK the fLOWOPERATIONMARK to set
	 */
	public void setFLOWOPERATIONMARK(String fLOWOPERATIONMARK) {
		FLOWOPERATIONMARK = fLOWOPERATIONMARK;
	}
	/**
	 * @return the fLOWPREOPERATIONMARK
	 */
	public String getFLOWPREOPERATIONMARK() {
		return FLOWPREOPERATIONMARK;
	}
	/**
	 * @param fLOWPREOPERATIONMARK the fLOWPREOPERATIONMARK to set
	 */
	public void setFLOWPREOPERATIONMARK(String fLOWPREOPERATIONMARK) {
		FLOWPREOPERATIONMARK = fLOWPREOPERATIONMARK;
	}
	/**
	 * @return the fLOWOPERATIONMASKENUM
	 */
	public String getFLOWOPERATIONMASKENUM() {
		return FLOWOPERATIONMASKENUM;
	}
	/**
	 * @param fLOWOPERATIONMASKENUM the fLOWOPERATIONMASKENUM to set
	 */
	public void setFLOWOPERATIONMASKENUM(String fLOWOPERATIONMASKENUM) {
		FLOWOPERATIONMASKENUM = fLOWOPERATIONMASKENUM;
	}
	/**
	 * @return the dOCSOURCENAME
	 */
	public String getDOCSOURCENAME() {
		return DOCSOURCENAME;
	}
	/**
	 * @param dOCSOURCENAME the dOCSOURCENAME to set
	 */
	public void setDOCSOURCENAME(String dOCSOURCENAME) {
		DOCSOURCENAME = dOCSOURCENAME;
	}
	/**
	 * @return the dOCLINKTO
	 */
	public String getDOCLINKTO() {
		return DOCLINKTO;
	}
	/**
	 * @param dOCLINKTO the dOCLINKTO to set
	 */
	public void setDOCLINKTO(String dOCLINKTO) {
		DOCLINKTO = dOCLINKTO;
	}
	/**
	 * @return the dOCMIRRORTO
	 */
	public String getDOCMIRRORTO() {
		return DOCMIRRORTO;
	}
	/**
	 * @param dOCMIRRORTO the dOCMIRRORTO to set
	 */
	public void setDOCMIRRORTO(String dOCMIRRORTO) {
		DOCMIRRORTO = dOCMIRRORTO;
	}
	/**
	 * @return the rANDOMSERIAL
	 */
	public String getRANDOMSERIAL() {
		return RANDOMSERIAL;
	}
	/**
	 * @param rANDOMSERIAL the rANDOMSERIAL to set
	 */
	public void setRANDOMSERIAL(String rANDOMSERIAL) {
		RANDOMSERIAL = rANDOMSERIAL;
	}
	/**
	 * @return the pOSTUSER
	 */
	public String getPOSTUSER() {
		return POSTUSER;
	}
	/**
	 * @param pOSTUSER the pOSTUSER to set
	 */
	public void setPOSTUSER(String pOSTUSER) {
		POSTUSER = pOSTUSER;
	}
	/**
	 * @return the iSPAGEIMG
	 */
	public Integer getISPAGEIMG() {
		return ISPAGEIMG;
	}
	/**
	 * @param iSPAGEIMG the iSPAGEIMG to set
	 */
	public void setISPAGEIMG(Integer iSPAGEIMG) {
		ISPAGEIMG = iSPAGEIMG;
	}
	/**
	 * @return the pUBLISHDATE
	 */
	public String getPUBLISHDATE() {
		return PUBLISHDATE;
	}
	/**
	 * @param pUBLISHDATE the pUBLISHDATE to set
	 */
	public void setPUBLISHDATE(String pUBLISHDATE) {
		PUBLISHDATE = pUBLISHDATE;
	}
	/**
	 * @return the pAGENUM
	 */
	public String getPAGENUM() {
		return PAGENUM;
	}
	/**
	 * @param pAGENUM the pAGENUM to set
	 */
	public void setPAGENUM(String pAGENUM) {
		PAGENUM = pAGENUM;
	}
	/**
	 * @return the pAGENAME
	 */
	public String getPAGENAME() {
		return PAGENAME;
	}
	/**
	 * @param pAGENAME the pAGENAME to set
	 */
	public void setPAGENAME(String pAGENAME) {
		PAGENAME = pAGENAME;
	}
	/**
	 * @return the pDFFILENAME
	 */
	public String getPDFFILENAME() {
		return PDFFILENAME;
	}
	/**
	 * @param pDFFILENAME the pDFFILENAME to set
	 */
	public void setPDFFILENAME(String pDFFILENAME) {
		PDFFILENAME = pDFFILENAME;
	}
	/**
	 * @return the pAGEIMAGEFILENAME
	 */
	public String getPAGEIMAGEFILENAME() {
		return PAGEIMAGEFILENAME;
	}
	/**
	 * @param pAGEIMAGEFILENAME the pAGEIMAGEFILENAME to set
	 */
	public void setPAGEIMAGEFILENAME(String pAGEIMAGEFILENAME) {
		PAGEIMAGEFILENAME = pAGEIMAGEFILENAME;
	}
	/**
	 * @return the mAP
	 */
	public String getMAP() {
		return MAP;
	}
	/**
	 * @param mAP the mAP to set
	 */
	public void setMAP(String mAP) {
		MAP = mAP;
	}
	/**
	 * @return the yINTI
	 */
	public String getYINTI() {
		return YINTI;
	}
	/**
	 * @param yINTI the yINTI to set
	 */
	public void setYINTI(String yINTI) {
		YINTI = yINTI;
	}
	/**
	 * @return the sITEID
	 */
	public Integer getSITEID() {
		return SITEID;
	}
	/**
	 * @param sITEID the sITEID to set
	 */
	public void setSITEID(Integer sITEID) {
		SITEID = sITEID;
	}
	/**
	 * @return the sRCSITEID
	 */
	public Integer getSRCSITEID() {
		return SRCSITEID;
	}
	/**
	 * @param sRCSITEID the sRCSITEID to set
	 */
	public void setSRCSITEID(Integer sRCSITEID) {
		SRCSITEID = sRCSITEID;
	}
	/**
	 * @return the dOCFIRSTPUBTIME
	 */
	public Date getDOCFIRSTPUBTIME() {
		return DOCFIRSTPUBTIME;
	}
	/**
	 * @param dOCFIRSTPUBTIME the dOCFIRSTPUBTIME to set
	 */
	public void setDOCFIRSTPUBTIME(Date dOCFIRSTPUBTIME) {
		DOCFIRSTPUBTIME = dOCFIRSTPUBTIME;
	}
	/**
	 * @return the nODEID
	 */
	public Integer getNODEID() {
		return NODEID;
	}
	/**
	 * @param nODEID the nODEID to set
	 */
	public void setNODEID(Integer nODEID) {
		NODEID = nODEID;
	}
	/**
	 * @return the oRDERID
	 */
	public Integer getORDERID() {
		return ORDERID;
	}
	/**
	 * @param oRDERID the oRDERID to set
	 */
	public void setORDERID(Integer oRDERID) {
		ORDERID = oRDERID;
	}
	/**
	 * @return the cRDEPT
	 */
	public String getCRDEPT() {
		return CRDEPT;
	}
	/**
	 * @param cRDEPT the cRDEPT to set
	 */
	public void setCRDEPT(String cRDEPT) {
		CRDEPT = cRDEPT;
	}
	/**
	 * @return the dOCFORM
	 */
	public Integer getDOCFORM() {
		return DOCFORM;
	}
	/**
	 * @param dOCFORM the dOCFORM to set
	 */
	public void setDOCFORM(Integer dOCFORM) {
		DOCFORM = dOCFORM;
	}
	/**
	 * @return the dOCLEVEL
	 */
	public Integer getDOCLEVEL() {
		return DOCLEVEL;
	}
	/**
	 * @param dOCLEVEL the dOCLEVEL to set
	 */
	public void setDOCLEVEL(Integer dOCLEVEL) {
		DOCLEVEL = dOCLEVEL;
	}
	/**
	 * @return the oLDDOCPUBURL
	 */
	public String getOLDDOCPUBURL() {
		return OLDDOCPUBURL;
	}
	/**
	 * @param oLDDOCPUBURL the oLDDOCPUBURL to set
	 */
	public void setOLDDOCPUBURL(String oLDDOCPUBURL) {
		OLDDOCPUBURL = oLDDOCPUBURL;
	}
	/**
	 * @return the tALKING_TITLE
	 */
	public String getTALKING_TITLE() {
		return TALKING_TITLE;
	}
	/**
	 * @param tALKING_TITLE the tALKING_TITLE to set
	 */
	public void setTALKING_TITLE(String tALKING_TITLE) {
		TALKING_TITLE = tALKING_TITLE;
	}
	/**
	 * @return the tALKING_TIME
	 */
	public Date getTALKING_TIME() {
		return TALKING_TIME;
	}
	/**
	 * @param tALKING_TIME the tALKING_TIME to set
	 */
	public void setTALKING_TIME(Date tALKING_TIME) {
		TALKING_TIME = tALKING_TIME;
	}
	/**
	 * @return the tALKING_MAN
	 */
	public String getTALKING_MAN() {
		return TALKING_MAN;
	}
	/**
	 * @param tALKING_MAN the tALKING_MAN to set
	 */
	public void setTALKING_MAN(String tALKING_MAN) {
		TALKING_MAN = tALKING_MAN;
	}
	/**
	 * @return the tALKING_ADD
	 */
	public String getTALKING_ADD() {
		return TALKING_ADD;
	}
	/**
	 * @param tALKING_ADD the tALKING_ADD to set
	 */
	public void setTALKING_ADD(String tALKING_ADD) {
		TALKING_ADD = tALKING_ADD;
	}
	/**
	 * 
	 */
	public WCMDOCUMENT() {
		super();
		// TODO Auto-generated constructor stub
	}
	/**
	 * @param dOCID
	 * @param dOCCHANNEL
	 * @param dOCVERSION
	 * @param dOCTYPE
	 * @param dOCTITLE
	 * @param dOCSOURCE
	 * @param dOCSECURITY
	 * @param dOCSTATUS
	 * @param dOCKIND
	 * @param dOCCONTENT
	 * @param dOCHTMLCON
	 * @param dOCABSTRACT
	 * @param dOCKEYWORDS
	 * @param dOCRELWORDS
	 * @param dOCPEOPLE
	 * @param dOCPLACE
	 * @param dOCAUTHOR
	 * @param dOCEDITOR
	 * @param dOCAUDITOR
	 * @param dOCOUTUPID
	 * @param dOCVALID
	 * @param dOCPUBURL
	 * @param dOCPUBTIME
	 * @param dOCRELTIME
	 * @param cRUSER
	 * @param cRTIME
	 * @param dOCWORDSCOUNT
	 * @param dOCPRO
	 * @param rIGHTDEFINED
	 * @param tITLECOLOR
	 * @param tEMPLATEID
	 * @param sCHEDULE
	 * @param dOCNO
	 * @param dOCFLAG
	 * @param eDITOR
	 * @param aTTRIBUTE
	 * @param hITSCOUNT
	 * @param dOCPUBHTMLCON
	 * @param sUBDOCTITLE
	 * @param aTTACHPIC
	 * @param dOCLINK
	 * @param dOCFILENAME
	 * @param dOCFROMVERSION
	 * @param oPERTIME
	 * @param oPERUSER
	 * @param fLOWOPERATIONMARK
	 * @param fLOWPREOPERATIONMARK
	 * @param fLOWOPERATIONMASKENUM
	 * @param dOCSOURCENAME
	 * @param dOCLINKTO
	 * @param dOCMIRRORTO
	 * @param rANDOMSERIAL
	 * @param pOSTUSER
	 * @param iSPAGEIMG
	 * @param pUBLISHDATE
	 * @param pAGENUM
	 * @param pAGENAME
	 * @param pDFFILENAME
	 * @param pAGEIMAGEFILENAME
	 * @param mAP
	 * @param yINTI
	 * @param sITEID
	 * @param sRCSITEID
	 * @param dOCFIRSTPUBTIME
	 * @param nODEID
	 * @param oRDERID
	 * @param cRDEPT
	 * @param dOCFORM
	 * @param dOCLEVEL
	 * @param oLDDOCPUBURL
	 * @param tALKING_TITLE
	 * @param tALKING_TIME
	 * @param tALKING_MAN
	 * @param tALKING_ADD
	 */
	public WCMDOCUMENT(Integer dOCID, Integer dOCCHANNEL, Integer dOCVERSION,
			Integer dOCTYPE, String dOCTITLE, Integer dOCSOURCE,
			Integer dOCSECURITY, Integer dOCSTATUS, Long dOCKIND,
			String dOCCONTENT, String dOCHTMLCON, String dOCABSTRACT,
			String dOCKEYWORDS, String dOCRELWORDS, String dOCPEOPLE,
			String dOCPLACE, String dOCAUTHOR, String dOCEDITOR,
			String dOCAUDITOR, Integer dOCOUTUPID, Date dOCVALID,
			String dOCPUBURL, Date dOCPUBTIME, Date dOCRELTIME, String cRUSER,
			Date cRTIME, Integer dOCWORDSCOUNT, Integer dOCPRO,
			Integer rIGHTDEFINED, String tITLECOLOR, Integer tEMPLATEID,
			Integer sCHEDULE, String dOCNO, Integer dOCFLAG, String eDITOR,
			String aTTRIBUTE, Integer hITSCOUNT, String dOCPUBHTMLCON,
			String sUBDOCTITLE, Integer aTTACHPIC, String dOCLINK,
			String dOCFILENAME, Integer dOCFROMVERSION, Date oPERTIME,
			String oPERUSER, String fLOWOPERATIONMARK,
			String fLOWPREOPERATIONMARK, String fLOWOPERATIONMASKENUM,
			String dOCSOURCENAME, String dOCLINKTO, String dOCMIRRORTO,
			String rANDOMSERIAL, String pOSTUSER, Integer iSPAGEIMG,
			String pUBLISHDATE, String pAGENUM, String pAGENAME,
			String pDFFILENAME, String pAGEIMAGEFILENAME, String mAP,
			String yINTI, Integer sITEID, Integer sRCSITEID,
			Date dOCFIRSTPUBTIME, Integer nODEID, Integer oRDERID,
			String cRDEPT, Integer dOCFORM, Integer dOCLEVEL,
			String oLDDOCPUBURL, String tALKING_TITLE, Date tALKING_TIME,
			String tALKING_MAN, String tALKING_ADD) {
		super();
		DOCID = dOCID;
		DOCCHANNEL = dOCCHANNEL;
		DOCVERSION = dOCVERSION;
		DOCTYPE = dOCTYPE;
		DOCTITLE = dOCTITLE;
		DOCSOURCE = dOCSOURCE;
		DOCSECURITY = dOCSECURITY;
		DOCSTATUS = dOCSTATUS;
		DOCKIND = dOCKIND;
		DOCCONTENT = dOCCONTENT;
		DOCHTMLCON = dOCHTMLCON;
		DOCABSTRACT = dOCABSTRACT;
		DOCKEYWORDS = dOCKEYWORDS;
		DOCRELWORDS = dOCRELWORDS;
		DOCPEOPLE = dOCPEOPLE;
		DOCPLACE = dOCPLACE;
		DOCAUTHOR = dOCAUTHOR;
		DOCEDITOR = dOCEDITOR;
		DOCAUDITOR = dOCAUDITOR;
		DOCOUTUPID = dOCOUTUPID;
		DOCVALID = dOCVALID;
		DOCPUBURL = dOCPUBURL;
		DOCPUBTIME = dOCPUBTIME;
		DOCRELTIME = dOCRELTIME;
		CRUSER = cRUSER;
		CRTIME = cRTIME;
		DOCWORDSCOUNT = dOCWORDSCOUNT;
		DOCPRO = dOCPRO;
		RIGHTDEFINED = rIGHTDEFINED;
		TITLECOLOR = tITLECOLOR;
		TEMPLATEID = tEMPLATEID;
		SCHEDULE = sCHEDULE;
		DOCNO = dOCNO;
		DOCFLAG = dOCFLAG;
		EDITOR = eDITOR;
		ATTRIBUTE = aTTRIBUTE;
		HITSCOUNT = hITSCOUNT;
		DOCPUBHTMLCON = dOCPUBHTMLCON;
		SUBDOCTITLE = sUBDOCTITLE;
		ATTACHPIC = aTTACHPIC;
		DOCLINK = dOCLINK;
		DOCFILENAME = dOCFILENAME;
		DOCFROMVERSION = dOCFROMVERSION;
		OPERTIME = oPERTIME;
		OPERUSER = oPERUSER;
		FLOWOPERATIONMARK = fLOWOPERATIONMARK;
		FLOWPREOPERATIONMARK = fLOWPREOPERATIONMARK;
		FLOWOPERATIONMASKENUM = fLOWOPERATIONMASKENUM;
		DOCSOURCENAME = dOCSOURCENAME;
		DOCLINKTO = dOCLINKTO;
		DOCMIRRORTO = dOCMIRRORTO;
		RANDOMSERIAL = rANDOMSERIAL;
		POSTUSER = pOSTUSER;
		ISPAGEIMG = iSPAGEIMG;
		PUBLISHDATE = pUBLISHDATE;
		PAGENUM = pAGENUM;
		PAGENAME = pAGENAME;
		PDFFILENAME = pDFFILENAME;
		PAGEIMAGEFILENAME = pAGEIMAGEFILENAME;
		MAP = mAP;
		YINTI = yINTI;
		SITEID = sITEID;
		SRCSITEID = sRCSITEID;
		DOCFIRSTPUBTIME = dOCFIRSTPUBTIME;
		NODEID = nODEID;
		ORDERID = oRDERID;
		CRDEPT = cRDEPT;
		DOCFORM = dOCFORM;
		DOCLEVEL = dOCLEVEL;
		OLDDOCPUBURL = oLDDOCPUBURL;
		TALKING_TITLE = tALKING_TITLE;
		TALKING_TIME = tALKING_TIME;
		TALKING_MAN = tALKING_MAN;
		TALKING_ADD = tALKING_ADD;
	}
	
	
	
	
}
