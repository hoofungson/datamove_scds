/**
 * 
 */
package cn.gov.scds.entity;

import java.io.Serializable;
import java.util.Date;

/**
 * @ClassName WCMCHANNE
 * @author hoofungson
 * @Description TODO
 * @Date 2015年5月27日下午4:25:44
 */
public class WCMCHANNEL implements Serializable{
   /*
    * 栏目实体
    * */
	
	/*成员变量声明*/
	private int CHANNELID;
	private int SITEID;
	private String CHNLNAME;
	private String CHNLDESC;
	private String CHNLTABLE;
	private String CHNLQUERY;
	private int CHNLPROP;
	private int PARENTID;
	private int CHNLORDER;
	private Long SCHEDULE;
	private String CHNLDATAPATH;
	private String CHNLORDERBY;
	private String ATTRIBUTE;
	private String CRUSER;
	private Date CRTIME;
	private int STATUS;
	private int CHNLTYPE;
	private int CHNLOUTLINETEMP;
	private int CHNLDETAILTEMP;
	private int PUBLISHPRO;
	private String OPERUSER;
	private Date OPERTIME;
	private String LINKURL;
	private String CONTENTADDEDITPAGE;
	private String CONTENTLISTPAGE;
	private String CONTENTSHOWPAGE;
	private String OUTLINEFILEDS;
	private String OUTLINEDBFIELDS;
	private int ISCLUSTER;
	private String SHOWFIELDS;
	private String FIELDSWIDTH;
	private String BASEPROPS;
	private String OTHERPROPS;
	private String ADVANCEPROPS;
	private String TOOLBAR;
	private String ADVTOOLBAR;
	private int ISCONTAINSCHILDREN;
	private String METASYNFIELDS;
	private String FLUENCEXML;
	private Long USEDOCLEVEL;
	private Date LASTMODIFYTIME;
	private String VIEWPROPS;
	private String NEEDEDPROPS;
	private int INHERIT;
	/**
	 * @return the cHANNELID
	 */
	public int getCHANNELID() {
		return CHANNELID;
	}
	/**
	 * @param cHANNELID the cHANNELID to set
	 */
	public void setCHANNELID(int cHANNELID) {
		CHANNELID = cHANNELID;
	}
	/**
	 * @return the sITEID
	 */
	public int getSITEID() {
		return SITEID;
	}
	/**
	 * @param sITEID the sITEID to set
	 */
	public void setSITEID(int sITEID) {
		SITEID = sITEID;
	}
	/**
	 * @return the cHNLNAME
	 */
	public String getCHNLNAME() {
		return CHNLNAME;
	}
	/**
	 * @param cHNLNAME the cHNLNAME to set
	 */
	public void setCHNLNAME(String cHNLNAME) {
		CHNLNAME = cHNLNAME;
	}
	/**
	 * @return the cHNLDESC
	 */
	public String getCHNLDESC() {
		return CHNLDESC;
	}
	/**
	 * @param cHNLDESC the cHNLDESC to set
	 */
	public void setCHNLDESC(String cHNLDESC) {
		CHNLDESC = cHNLDESC;
	}
	/**
	 * @return the cHNLTABLE
	 */
	public String getCHNLTABLE() {
		return CHNLTABLE;
	}
	/**
	 * @param cHNLTABLE the cHNLTABLE to set
	 */
	public void setCHNLTABLE(String cHNLTABLE) {
		CHNLTABLE = cHNLTABLE;
	}
	/**
	 * @return the cHNLQUERY
	 */
	public String getCHNLQUERY() {
		return CHNLQUERY;
	}
	/**
	 * @param cHNLQUERY the cHNLQUERY to set
	 */
	public void setCHNLQUERY(String cHNLQUERY) {
		CHNLQUERY = cHNLQUERY;
	}
	/**
	 * @return the cHNLPROP
	 */
	public int getCHNLPROP() {
		return CHNLPROP;
	}
	/**
	 * @param cHNLPROP the cHNLPROP to set
	 */
	public void setCHNLPROP(int cHNLPROP) {
		CHNLPROP = cHNLPROP;
	}
	/**
	 * @return the pARENTID
	 */
	public int getPARENTID() {
		return PARENTID;
	}
	/**
	 * @param pARENTID the pARENTID to set
	 */
	public void setPARENTID(int pARENTID) {
		PARENTID = pARENTID;
	}
	/**
	 * @return the cHNLORDER
	 */
	public int getCHNLORDER() {
		return CHNLORDER;
	}
	/**
	 * @param cHNLORDER the cHNLORDER to set
	 */
	public void setCHNLORDER(int cHNLORDER) {
		CHNLORDER = cHNLORDER;
	}
	/**
	 * @return the sCHEDULE
	 */
	public Long getSCHEDULE() {
		return SCHEDULE;
	}
	/**
	 * @param sCHEDULE the sCHEDULE to set
	 */
	public void setSCHEDULE(Long sCHEDULE) {
		SCHEDULE = sCHEDULE;
	}
	/**
	 * @return the cHNLDATAPATH
	 */
	public String getCHNLDATAPATH() {
		return CHNLDATAPATH;
	}
	/**
	 * @param cHNLDATAPATH the cHNLDATAPATH to set
	 */
	public void setCHNLDATAPATH(String cHNLDATAPATH) {
		CHNLDATAPATH = cHNLDATAPATH;
	}
	/**
	 * @return the cHNLORDERBY
	 */
	public String getCHNLORDERBY() {
		return CHNLORDERBY;
	}
	/**
	 * @param cHNLORDERBY the cHNLORDERBY to set
	 */
	public void setCHNLORDERBY(String cHNLORDERBY) {
		CHNLORDERBY = cHNLORDERBY;
	}
	/**
	 * @return the aTTRIBUTE
	 */
	public String getATTRIBUTE() {
		return ATTRIBUTE;
	}
	/**
	 * @param aTTRIBUTE the aTTRIBUTE to set
	 */
	public void setATTRIBUTE(String aTTRIBUTE) {
		ATTRIBUTE = aTTRIBUTE;
	}
	/**
	 * @return the cRUSER
	 */
	public String getCRUSER() {
		return CRUSER;
	}
	/**
	 * @param cRUSER the cRUSER to set
	 */
	public void setCRUSER(String cRUSER) {
		CRUSER = cRUSER;
	}
	/**
	 * @return the cRTIME
	 */
	public Date getCRTIME() {
		return CRTIME;
	}
	/**
	 * @param cRTIME the cRTIME to set
	 */
	public void setCRTIME(Date cRTIME) {
		CRTIME = cRTIME;
	}
	/**
	 * @return the sTATUS
	 */
	public int getSTATUS() {
		return STATUS;
	}
	/**
	 * @param sTATUS the sTATUS to set
	 */
	public void setSTATUS(int sTATUS) {
		STATUS = sTATUS;
	}
	/**
	 * @return the cHNLTYPE
	 */
	public int getCHNLTYPE() {
		return CHNLTYPE;
	}
	/**
	 * @param cHNLTYPE the cHNLTYPE to set
	 */
	public void setCHNLTYPE(int cHNLTYPE) {
		CHNLTYPE = cHNLTYPE;
	}
	/**
	 * @return the cHNLOUTLINETEMP
	 */
	public int getCHNLOUTLINETEMP() {
		return CHNLOUTLINETEMP;
	}
	/**
	 * @param cHNLOUTLINETEMP the cHNLOUTLINETEMP to set
	 */
	public void setCHNLOUTLINETEMP(int cHNLOUTLINETEMP) {
		CHNLOUTLINETEMP = cHNLOUTLINETEMP;
	}
	/**
	 * @return the cHNLDETAILTEMP
	 */
	public int getCHNLDETAILTEMP() {
		return CHNLDETAILTEMP;
	}
	/**
	 * @param cHNLDETAILTEMP the cHNLDETAILTEMP to set
	 */
	public void setCHNLDETAILTEMP(int cHNLDETAILTEMP) {
		CHNLDETAILTEMP = cHNLDETAILTEMP;
	}
	/**
	 * @return the pUBLISHPRO
	 */
	public int getPUBLISHPRO() {
		return PUBLISHPRO;
	}
	/**
	 * @param pUBLISHPRO the pUBLISHPRO to set
	 */
	public void setPUBLISHPRO(int pUBLISHPRO) {
		PUBLISHPRO = pUBLISHPRO;
	}
	/**
	 * @return the oPERUSER
	 */
	public String getOPERUSER() {
		return OPERUSER;
	}
	/**
	 * @param oPERUSER the oPERUSER to set
	 */
	public void setOPERUSER(String oPERUSER) {
		OPERUSER = oPERUSER;
	}
	/**
	 * @return the oPERTIME
	 */
	public Date getOPERTIME() {
		return OPERTIME;
	}
	/**
	 * @param oPERTIME the oPERTIME to set
	 */
	public void setOPERTIME(Date oPERTIME) {
		OPERTIME = oPERTIME;
	}
	/**
	 * @return the lINKURL
	 */
	public String getLINKURL() {
		return LINKURL;
	}
	/**
	 * @param lINKURL the lINKURL to set
	 */
	public void setLINKURL(String lINKURL) {
		LINKURL = lINKURL;
	}
	/**
	 * @return the cONTENTADDEDITPAGE
	 */
	public String getCONTENTADDEDITPAGE() {
		return CONTENTADDEDITPAGE;
	}
	/**
	 * @param cONTENTADDEDITPAGE the cONTENTADDEDITPAGE to set
	 */
	public void setCONTENTADDEDITPAGE(String cONTENTADDEDITPAGE) {
		CONTENTADDEDITPAGE = cONTENTADDEDITPAGE;
	}
	/**
	 * @return the cONTENTLISTPAGE
	 */
	public String getCONTENTLISTPAGE() {
		return CONTENTLISTPAGE;
	}
	/**
	 * @param cONTENTLISTPAGE the cONTENTLISTPAGE to set
	 */
	public void setCONTENTLISTPAGE(String cONTENTLISTPAGE) {
		CONTENTLISTPAGE = cONTENTLISTPAGE;
	}
	/**
	 * @return the cONTENTSHOWPAGE
	 */
	public String getCONTENTSHOWPAGE() {
		return CONTENTSHOWPAGE;
	}
	/**
	 * @param cONTENTSHOWPAGE the cONTENTSHOWPAGE to set
	 */
	public void setCONTENTSHOWPAGE(String cONTENTSHOWPAGE) {
		CONTENTSHOWPAGE = cONTENTSHOWPAGE;
	}
	/**
	 * @return the oUTLINEFILEDS
	 */
	public String getOUTLINEFILEDS() {
		return OUTLINEFILEDS;
	}
	/**
	 * @param oUTLINEFILEDS the oUTLINEFILEDS to set
	 */
	public void setOUTLINEFILEDS(String oUTLINEFILEDS) {
		OUTLINEFILEDS = oUTLINEFILEDS;
	}
	/**
	 * @return the oUTLINEDBFIELDS
	 */
	public String getOUTLINEDBFIELDS() {
		return OUTLINEDBFIELDS;
	}
	/**
	 * @param oUTLINEDBFIELDS the oUTLINEDBFIELDS to set
	 */
	public void setOUTLINEDBFIELDS(String oUTLINEDBFIELDS) {
		OUTLINEDBFIELDS = oUTLINEDBFIELDS;
	}
	/**
	 * @return the iSCLUSTER
	 */
	public int getISCLUSTER() {
		return ISCLUSTER;
	}
	/**
	 * @param iSCLUSTER the iSCLUSTER to set
	 */
	public void setISCLUSTER(int iSCLUSTER) {
		ISCLUSTER = iSCLUSTER;
	}
	/**
	 * @return the sHOWFIELDS
	 */
	public String getSHOWFIELDS() {
		return SHOWFIELDS;
	}
	/**
	 * @param sHOWFIELDS the sHOWFIELDS to set
	 */
	public void setSHOWFIELDS(String sHOWFIELDS) {
		SHOWFIELDS = sHOWFIELDS;
	}
	/**
	 * @return the fIELDSWIDTH
	 */
	public String getFIELDSWIDTH() {
		return FIELDSWIDTH;
	}
	/**
	 * @param fIELDSWIDTH the fIELDSWIDTH to set
	 */
	public void setFIELDSWIDTH(String fIELDSWIDTH) {
		FIELDSWIDTH = fIELDSWIDTH;
	}
	/**
	 * @return the bASEPROPS
	 */
	public String getBASEPROPS() {
		return BASEPROPS;
	}
	/**
	 * @param bASEPROPS the bASEPROPS to set
	 */
	public void setBASEPROPS(String bASEPROPS) {
		BASEPROPS = bASEPROPS;
	}
	/**
	 * @return the oTHERPROPS
	 */
	public String getOTHERPROPS() {
		return OTHERPROPS;
	}
	/**
	 * @param oTHERPROPS the oTHERPROPS to set
	 */
	public void setOTHERPROPS(String oTHERPROPS) {
		OTHERPROPS = oTHERPROPS;
	}
	/**
	 * @return the aDVANCEPROPS
	 */
	public String getADVANCEPROPS() {
		return ADVANCEPROPS;
	}
	/**
	 * @param aDVANCEPROPS the aDVANCEPROPS to set
	 */
	public void setADVANCEPROPS(String aDVANCEPROPS) {
		ADVANCEPROPS = aDVANCEPROPS;
	}
	/**
	 * @return the tOOLBAR
	 */
	public String getTOOLBAR() {
		return TOOLBAR;
	}
	/**
	 * @param tOOLBAR the tOOLBAR to set
	 */
	public void setTOOLBAR(String tOOLBAR) {
		TOOLBAR = tOOLBAR;
	}
	/**
	 * @return the aDVTOOLBAR
	 */
	public String getADVTOOLBAR() {
		return ADVTOOLBAR;
	}
	/**
	 * @param aDVTOOLBAR the aDVTOOLBAR to set
	 */
	public void setADVTOOLBAR(String aDVTOOLBAR) {
		ADVTOOLBAR = aDVTOOLBAR;
	}
	/**
	 * @return the iSCONTAINSCHILDREN
	 */
	public int getISCONTAINSCHILDREN() {
		return ISCONTAINSCHILDREN;
	}
	/**
	 * @param iSCONTAINSCHILDREN the iSCONTAINSCHILDREN to set
	 */
	public void setISCONTAINSCHILDREN(int iSCONTAINSCHILDREN) {
		ISCONTAINSCHILDREN = iSCONTAINSCHILDREN;
	}
	/**
	 * @return the mETASYNFIELDS
	 */
	public String getMETASYNFIELDS() {
		return METASYNFIELDS;
	}
	/**
	 * @param mETASYNFIELDS the mETASYNFIELDS to set
	 */
	public void setMETASYNFIELDS(String mETASYNFIELDS) {
		METASYNFIELDS = mETASYNFIELDS;
	}
	/**
	 * @return the fLUENCEXML
	 */
	public String getFLUENCEXML() {
		return FLUENCEXML;
	}
	/**
	 * @param fLUENCEXML the fLUENCEXML to set
	 */
	public void setFLUENCEXML(String fLUENCEXML) {
		FLUENCEXML = fLUENCEXML;
	}
	/**
	 * @return the uSEDOCLEVEL
	 */
	public Long getUSEDOCLEVEL() {
		return USEDOCLEVEL;
	}
	/**
	 * @param uSEDOCLEVEL the uSEDOCLEVEL to set
	 */
	public void setUSEDOCLEVEL(Long uSEDOCLEVEL) {
		USEDOCLEVEL = uSEDOCLEVEL;
	}
	/**
	 * @return the lASTMODIFYTIME
	 */
	public Date getLASTMODIFYTIME() {
		return LASTMODIFYTIME;
	}
	/**
	 * @param lASTMODIFYTIME the lASTMODIFYTIME to set
	 */
	public void setLASTMODIFYTIME(Date lASTMODIFYTIME) {
		LASTMODIFYTIME = lASTMODIFYTIME;
	}
	/**
	 * @return the vIEWPROPS
	 */
	public String getVIEWPROPS() {
		return VIEWPROPS;
	}
	/**
	 * @param vIEWPROPS the vIEWPROPS to set
	 */
	public void setVIEWPROPS(String vIEWPROPS) {
		VIEWPROPS = vIEWPROPS;
	}
	/**
	 * @return the nEEDEDPROPS
	 */
	public String getNEEDEDPROPS() {
		return NEEDEDPROPS;
	}
	/**
	 * @param nEEDEDPROPS the nEEDEDPROPS to set
	 */
	public void setNEEDEDPROPS(String nEEDEDPROPS) {
		NEEDEDPROPS = nEEDEDPROPS;
	}
	/**
	 * @return the iNHERIT
	 */
	public int getINHERIT() {
		return INHERIT;
	}
	/**
	 * @param iNHERIT the iNHERIT to set
	 */
	public void setINHERIT(int iNHERIT) {
		INHERIT = iNHERIT;
	}
	/**
	 * 
	 */
	public WCMCHANNEL() {
		super();
		// TODO Auto-generated constructor stub
	}
	/**
	 * @param cHANNELID
	 * @param sITEID
	 * @param cHNLNAME
	 * @param cHNLDESC
	 * @param cHNLTABLE
	 * @param cHNLQUERY
	 * @param cHNLPROP
	 * @param pARENTID
	 * @param cHNLORDER
	 * @param sCHEDULE
	 * @param cHNLDATAPATH
	 * @param cHNLORDERBY
	 * @param aTTRIBUTE
	 * @param cRUSER
	 * @param cRTIME
	 * @param sTATUS
	 * @param cHNLTYPE
	 * @param cHNLOUTLINETEMP
	 * @param cHNLDETAILTEMP
	 * @param pUBLISHPRO
	 * @param oPERUSER
	 * @param oPERTIME
	 * @param lINKURL
	 * @param cONTENTADDEDITPAGE
	 * @param cONTENTLISTPAGE
	 * @param cONTENTSHOWPAGE
	 * @param oUTLINEFILEDS
	 * @param oUTLINEDBFIELDS
	 * @param iSCLUSTER
	 * @param sHOWFIELDS
	 * @param fIELDSWIDTH
	 * @param bASEPROPS
	 * @param oTHERPROPS
	 * @param aDVANCEPROPS
	 * @param tOOLBAR
	 * @param aDVTOOLBAR
	 * @param iSCONTAINSCHILDREN
	 * @param mETASYNFIELDS
	 * @param fLUENCEXML
	 * @param uSEDOCLEVEL
	 * @param lASTMODIFYTIME
	 * @param vIEWPROPS
	 * @param nEEDEDPROPS
	 * @param iNHERIT
	 */
	public WCMCHANNEL(int cHANNELID, int sITEID, String cHNLNAME,
			String cHNLDESC, String cHNLTABLE, String cHNLQUERY, int cHNLPROP,
			int pARENTID, int cHNLORDER, Long sCHEDULE, String cHNLDATAPATH,
			String cHNLORDERBY, String aTTRIBUTE, String cRUSER, Date cRTIME,
			int sTATUS, int cHNLTYPE, int cHNLOUTLINETEMP, int cHNLDETAILTEMP,
			int pUBLISHPRO, String oPERUSER, Date oPERTIME, String lINKURL,
			String cONTENTADDEDITPAGE, String cONTENTLISTPAGE,
			String cONTENTSHOWPAGE, String oUTLINEFILEDS,
			String oUTLINEDBFIELDS, int iSCLUSTER, String sHOWFIELDS,
			String fIELDSWIDTH, String bASEPROPS, String oTHERPROPS,
			String aDVANCEPROPS, String tOOLBAR, String aDVTOOLBAR,
			int iSCONTAINSCHILDREN, String mETASYNFIELDS, String fLUENCEXML,
			Long uSEDOCLEVEL, Date lASTMODIFYTIME, String vIEWPROPS,
			String nEEDEDPROPS, int iNHERIT) {
		super();
		CHANNELID = cHANNELID;
		SITEID = sITEID;
		CHNLNAME = cHNLNAME;
		CHNLDESC = cHNLDESC;
		CHNLTABLE = cHNLTABLE;
		CHNLQUERY = cHNLQUERY;
		CHNLPROP = cHNLPROP;
		PARENTID = pARENTID;
		CHNLORDER = cHNLORDER;
		SCHEDULE = sCHEDULE;
		CHNLDATAPATH = cHNLDATAPATH;
		CHNLORDERBY = cHNLORDERBY;
		ATTRIBUTE = aTTRIBUTE;
		CRUSER = cRUSER;
		CRTIME = cRTIME;
		STATUS = sTATUS;
		CHNLTYPE = cHNLTYPE;
		CHNLOUTLINETEMP = cHNLOUTLINETEMP;
		CHNLDETAILTEMP = cHNLDETAILTEMP;
		PUBLISHPRO = pUBLISHPRO;
		OPERUSER = oPERUSER;
		OPERTIME = oPERTIME;
		LINKURL = lINKURL;
		CONTENTADDEDITPAGE = cONTENTADDEDITPAGE;
		CONTENTLISTPAGE = cONTENTLISTPAGE;
		CONTENTSHOWPAGE = cONTENTSHOWPAGE;
		OUTLINEFILEDS = oUTLINEFILEDS;
		OUTLINEDBFIELDS = oUTLINEDBFIELDS;
		ISCLUSTER = iSCLUSTER;
		SHOWFIELDS = sHOWFIELDS;
		FIELDSWIDTH = fIELDSWIDTH;
		BASEPROPS = bASEPROPS;
		OTHERPROPS = oTHERPROPS;
		ADVANCEPROPS = aDVANCEPROPS;
		TOOLBAR = tOOLBAR;
		ADVTOOLBAR = aDVTOOLBAR;
		ISCONTAINSCHILDREN = iSCONTAINSCHILDREN;
		METASYNFIELDS = mETASYNFIELDS;
		FLUENCEXML = fLUENCEXML;
		USEDOCLEVEL = uSEDOCLEVEL;
		LASTMODIFYTIME = lASTMODIFYTIME;
		VIEWPROPS = vIEWPROPS;
		NEEDEDPROPS = nEEDEDPROPS;
		INHERIT = iNHERIT;
	}
	
	
}
