package cn.gov.scds.entity;

public class PollGroup {
    private int pollgroupid;
    private int pollid;
    private String grouptitle;
	public PollGroup() {
		super();
		// TODO Auto-generated constructor stub
	}
	public PollGroup(int pollgroupid, int pollid, String grouptitle) {
		super();
		this.pollgroupid = pollgroupid;
		this.pollid = pollid;
		this.grouptitle = grouptitle;
	}
	public int getPollgroupid() {
		return pollgroupid;
	}
	public void setPollgroupid(int pollgroupid) {
		this.pollgroupid = pollgroupid;
	}
	public int getPollid() {
		return pollid;
	}
	public void setPollid(int pollid) {
		this.pollid = pollid;
	}
	public String getGrouptitle() {
		return grouptitle;
	}
	public void setGrouptitle(String grouptitle) {
		this.grouptitle = grouptitle;
	}
    
    
}
