package cn.gov.scds.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class Content implements Serializable{
     private int contentID;
     private int item_ID;
     private String title_1;
     private String title_2;
     private String title_3;
     private String subject;
     private String keyword;
     private String item_text;
     private String inscribe;
     private String status;
     private Date write_date;
     private int write_user;
     private Date audit_date;
     private int audit_user;
     private Date issue_date;
     private int issue_user;
     private int show_number;
     private String content_from;
     private List<ContentFile> contentFiles;
     
     
     
	public int getContentID() {
		return contentID;
	}
	public void setContentID(int contentID) {
		this.contentID = contentID;
	}
	public int getItem_ID() {
		return item_ID;
	}
	public void setItem_ID(int item_ID) {
		this.item_ID = item_ID;
	}
	public String getTitle_1() {
		return title_1;
	}
	public void setTitle_1(String title_1) {
		this.title_1 = title_1;
	}
	public String getTitle_2() {
		return title_2;
	}
	public void setTitle_2(String title_2) {
		this.title_2 = title_2;
	}
	public String getTitle_3() {
		return title_3;
	}
	public void setTitle_3(String title_3) {
		this.title_3 = title_3;
	}
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String getKeyword() {
		return keyword;
	}
	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}
	public String getItem_text() {
		return item_text;
	}
	public void setItem_text(String item_text) {
		this.item_text = item_text;
	}
	public String getInscribe() {
		return inscribe;
	}
	public void setInscribe(String inscribe) {
		this.inscribe = inscribe;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Date getWrite_date() {
		return write_date;
	}
	public void setWrite_date(Date write_date) {
		this.write_date = write_date;
	}
	public int getWrite_user() {
		return write_user;
	}
	public void setWrite_user(int write_user) {
		this.write_user = write_user;
	}
	public Date getAudit_date() {
		return audit_date;
	}
	public void setAudit_date(Date audit_date) {
		this.audit_date = audit_date;
	}
	public int getAudit_user() {
		return audit_user;
	}
	public void setAudit_user(int audit_user) {
		this.audit_user = audit_user;
	}
	public Date getIssue_date() {
		return issue_date;
	}
	public void setIssue_date(Date issue_date) {
		this.issue_date = issue_date;
	}
	public int getIssue_user() {
		return issue_user;
	}
	public void setIssue_user(int issue_user) {
		this.issue_user = issue_user;
	}
	public int getShow_number() {
		return show_number;
	}
	public void setShow_number(int show_number) {
		this.show_number = show_number;
	}
	public String getContent_from() {
		return content_from;
	}
	public void setContent_from(String content_from) {
		this.content_from = content_from;
	}
	public List<ContentFile> getContentFiles() {
		return contentFiles;
	}
	public void setContentFiles(List<ContentFile> contentFiles) {
		this.contentFiles = contentFiles;
	}
	public Content() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Content(int contentID, int item_ID, String title_1, String title_2,
			String title_3, String subject, String keyword, String item_text,
			String inscribe, String status, Date write_date, int write_user,
			Date audit_date, int audit_user, Date issue_date, int issue_user,
			int show_number, String content_from, List<ContentFile> contentFiles) {
		super();
		this.contentID = contentID;
		this.item_ID = item_ID;
		this.title_1 = title_1;
		this.title_2 = title_2;
		this.title_3 = title_3;
		this.subject = subject;
		this.keyword = keyword;
		this.item_text = item_text;
		this.inscribe = inscribe;
		this.status = status;
		this.write_date = write_date;
		this.write_user = write_user;
		this.audit_date = audit_date;
		this.audit_user = audit_user;
		this.issue_date = issue_date;
		this.issue_user = issue_user;
		this.show_number = show_number;
		this.content_from = content_from;
		this.contentFiles = contentFiles;
	}
	
}
