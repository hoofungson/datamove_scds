package cn.gov.scds.entity;

import java.io.Serializable;

public class Item implements Serializable{
     private int item_id;
     private String item_name;
     private String item_desc;
     
	public int getItem_id() {
		return item_id;
	}
	public void setItem_id(int item_id) {
		this.item_id = item_id;
	}
	public String getItem_name() {
		return item_name;
	}
	public void setItem_name(String item_name) {
		this.item_name = item_name;
	}
	public String getItem_desc() {
		return item_desc;
	}
	public void setItem_desc(String item_desc) {
		this.item_desc = item_desc;
	}
	public Item() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Item(int item_id, String item_name, String item_desc) {
		super();
		this.item_id = item_id;
		this.item_name = item_name;
		this.item_desc = item_desc;
	}
     
     
}
