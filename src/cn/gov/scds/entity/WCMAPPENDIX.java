/**
 * 
 */
package cn.gov.scds.entity;

import java.io.Serializable;
import java.util.Date;


/**
 * @ClassName WCMAPPENDIX
 * @author hoofungson
 * @Description TODO
 * @Date 2015年5月27日下午3:06:42
 */
public class WCMAPPENDIX implements Serializable{
   /*
    * 附件实体。
    * */
	
	/*成员变量声明*/
	private int APPENDIXID;
	private int APPDOCID;
	private String APPFILE;
	private int APPFILETYPE;
	private String APPDESC;
	private String APPLINKALT;
	private int APPSERN;
	private int APPPROP;
	private int APPFLAG;
	private Date APPTIME;
	private String APPAUTHOR;
	private String APPEDITOR;
	private String ATTRIBUTE;
	private int USEDVERSIONS;
	private String CRUSER;
	private Date CRTIME;
	private String SRCFILE;
	private String FILEEXT;
	private int ISIMPORTTOIMAGELIB;
	private String RELATEDCOLUMN;
	private String RELATEPHOTOIDS;
	private String APPDESC2;
	private String ATTRIBUTES;
	
	/*
	 * 声明成员变量的Get和Set方法。
	 * */
	
	/**
	 * @return the aPPENDIXID
	 */
	public int getAPPENDIXID() {
		return APPENDIXID;
	}
	/**
	 * @param aPPENDIXID the aPPENDIXID to set
	 */
	public void setAPPENDIXID(int aPPENDIXID) {
		APPENDIXID = aPPENDIXID;
	}
	/**
	 * @return the aPPDOCID
	 */
	public int getAPPDOCID() {
		return APPDOCID;
	}
	/**
	 * @param aPPDOCID the aPPDOCID to set
	 */
	public void setAPPDOCID(int aPPDOCID) {
		APPDOCID = aPPDOCID;
	}
	/**
	 * @return the aPPFILE
	 */
	public String getAPPFILE() {
		return APPFILE;
	}
	/**
	 * @param aPPFILE the aPPFILE to set
	 */
	public void setAPPFILE(String aPPFILE) {
		APPFILE = aPPFILE;
	}
	/**
	 * @return the aPPFILETYPE
	 */
	public int getAPPFILETYPE() {
		return APPFILETYPE;
	}
	/**
	 * @param aPPFILETYPE the aPPFILETYPE to set
	 */
	public void setAPPFILETYPE(int aPPFILETYPE) {
		APPFILETYPE = aPPFILETYPE;
	}
	/**
	 * @return the aPPDESC
	 */
	public String getAPPDESC() {
		return APPDESC;
	}
	/**
	 * @param aPPDESC the aPPDESC to set
	 */
	public void setAPPDESC(String aPPDESC) {
		APPDESC = aPPDESC;
	}
	/**
	 * @return the aPPLINKALT
	 */
	public String getAPPLINKALT() {
		return APPLINKALT;
	}
	/**
	 * @param aPPLINKALT the aPPLINKALT to set
	 */
	public void setAPPLINKALT(String aPPLINKALT) {
		APPLINKALT = aPPLINKALT;
	}
	/**
	 * @return the aPPSERN
	 */
	public int getAPPSERN() {
		return APPSERN;
	}
	/**
	 * @param aPPSERN the aPPSERN to set
	 */
	public void setAPPSERN(int aPPSERN) {
		APPSERN = aPPSERN;
	}
	/**
	 * @return the aPPPROP
	 */
	public int getAPPPROP() {
		return APPPROP;
	}
	/**
	 * @param aPPPROP the aPPPROP to set
	 */
	public void setAPPPROP(int aPPPROP) {
		APPPROP = aPPPROP;
	}
	/**
	 * @return the aPPFLAG
	 */
	public int getAPPFLAG() {
		return APPFLAG;
	}
	/**
	 * @param aPPFLAG the aPPFLAG to set
	 */
	public void setAPPFLAG(int aPPFLAG) {
		APPFLAG = aPPFLAG;
	}
	/**
	 * @return the aPPTIME
	 */
	public Date getAPPTIME() {
		return APPTIME;
	}
	/**
	 * @param aPPTIME the aPPTIME to set
	 */
	public void setAPPTIME(Date aPPTIME) {
		APPTIME = aPPTIME;
	}
	/**
	 * @return the aPPAUTHOR
	 */
	public String getAPPAUTHOR() {
		return APPAUTHOR;
	}
	/**
	 * @param aPPAUTHOR the aPPAUTHOR to set
	 */
	public void setAPPAUTHOR(String aPPAUTHOR) {
		APPAUTHOR = aPPAUTHOR;
	}
	/**
	 * @return the aPPEDITOR
	 */
	public String getAPPEDITOR() {
		return APPEDITOR;
	}
	/**
	 * @param aPPEDITOR the aPPEDITOR to set
	 */
	public void setAPPEDITOR(String aPPEDITOR) {
		APPEDITOR = aPPEDITOR;
	}
	/**
	 * @return the aTTRIBUTE
	 */
	public String getATTRIBUTE() {
		return ATTRIBUTE;
	}
	/**
	 * @param aTTRIBUTE the aTTRIBUTE to set
	 */
	public void setATTRIBUTE(String aTTRIBUTE) {
		ATTRIBUTE = aTTRIBUTE;
	}
	/**
	 * @return the uSEDVERSIONS
	 */
	public int getUSEDVERSIONS() {
		return USEDVERSIONS;
	}
	/**
	 * @param uSEDVERSIONS the uSEDVERSIONS to set
	 */
	public void setUSEDVERSIONS(int uSEDVERSIONS) {
		USEDVERSIONS = uSEDVERSIONS;
	}
	/**
	 * @return the cRUSER
	 */
	public String getCRUSER() {
		return CRUSER;
	}
	/**
	 * @param cRUSER the cRUSER to set
	 */
	public void setCRUSER(String cRUSER) {
		CRUSER = cRUSER;
	}
	/**
	 * @return the cRTIME
	 */
	public Date getCRTIME() {
		return CRTIME;
	}
	/**
	 * @param cRTIME the cRTIME to set
	 */
	public void setCRTIME(Date cRTIME) {
		CRTIME = cRTIME;
	}
	/**
	 * @return the sRCFILE
	 */
	public String getSRCFILE() {
		return SRCFILE;
	}
	/**
	 * @param sRCFILE the sRCFILE to set
	 */
	public void setSRCFILE(String sRCFILE) {
		SRCFILE = sRCFILE;
	}
	/**
	 * @return the fILEEXT
	 */
	public String getFILEEXT() {
		return FILEEXT;
	}
	/**
	 * @param fILEEXT the fILEEXT to set
	 */
	public void setFILEEXT(String fILEEXT) {
		FILEEXT = fILEEXT;
	}
	/**
	 * @return the iSIMPORTTOIMAGELIB
	 */
	public int getISIMPORTTOIMAGELIB() {
		return ISIMPORTTOIMAGELIB;
	}
	/**
	 * @param iSIMPORTTOIMAGELIB the iSIMPORTTOIMAGELIB to set
	 */
	public void setISIMPORTTOIMAGELIB(int iSIMPORTTOIMAGELIB) {
		ISIMPORTTOIMAGELIB = iSIMPORTTOIMAGELIB;
	}
	/**
	 * @return the rELATEDCOLUMN
	 */
	public String getRELATEDCOLUMN() {
		return RELATEDCOLUMN;
	}
	/**
	 * @param rELATEDCOLUMN the rELATEDCOLUMN to set
	 */
	public void setRELATEDCOLUMN(String rELATEDCOLUMN) {
		RELATEDCOLUMN = rELATEDCOLUMN;
	}
	/**
	 * @return the rELATEPHOTOIDS
	 */
	public String getRELATEPHOTOIDS() {
		return RELATEPHOTOIDS;
	}
	/**
	 * @param rELATEPHOTOIDS the rELATEPHOTOIDS to set
	 */
	public void setRELATEPHOTOIDS(String rELATEPHOTOIDS) {
		RELATEPHOTOIDS = rELATEPHOTOIDS;
	}
	/**
	 * @return the aPPDESC2
	 */
	public String getAPPDESC2() {
		return APPDESC2;
	}
	/**
	 * @param aPPDESC2 the aPPDESC2 to set
	 */
	public void setAPPDESC2(String aPPDESC2) {
		APPDESC2 = aPPDESC2;
	}
	/**
	 * @return the aTTRIBUTES
	 */
	public String getATTRIBUTES() {
		return ATTRIBUTES;
	}
	/**
	 * @param aTTRIBUTES the aTTRIBUTES to set
	 */
	public void setATTRIBUTES(String aTTRIBUTES) {
		ATTRIBUTES = aTTRIBUTES;
	}
	
	/*声明一个无参构造函数*/
	
	/**
	 * 
	 */
	public WCMAPPENDIX() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	/*声明一个形参构造函数*/
	
	/**
	 * @param aPPENDIXID
	 * @param aPPDOCID
	 * @param aPPFILE
	 * @param aPPFILETYPE
	 * @param aPPDESC
	 * @param aPPLINKALT
	 * @param aPPSERN
	 * @param aPPPROP
	 * @param aPPFLAG
	 * @param aPPTIME
	 * @param aPPAUTHOR
	 * @param aPPEDITOR
	 * @param aTTRIBUTE
	 * @param uSEDVERSIONS
	 * @param cRUSER
	 * @param cRTIME
	 * @param sRCFILE
	 * @param fILEEXT
	 * @param iSIMPORTTOIMAGELIB
	 * @param rELATEDCOLUMN
	 * @param rELATEPHOTOIDS
	 * @param aPPDESC2
	 * @param aTTRIBUTES
	 */
	public WCMAPPENDIX(int aPPENDIXID, int aPPDOCID, String aPPFILE,
			int aPPFILETYPE, String aPPDESC, String aPPLINKALT, int aPPSERN,
			int aPPPROP, int aPPFLAG, Date aPPTIME, String aPPAUTHOR,
			String aPPEDITOR, String aTTRIBUTE, int uSEDVERSIONS,
			String cRUSER, Date cRTIME, String sRCFILE, String fILEEXT,
			int iSIMPORTTOIMAGELIB, String rELATEDCOLUMN,
			String rELATEPHOTOIDS, String aPPDESC2, String aTTRIBUTES) {
		super();
		APPENDIXID = aPPENDIXID;
		APPDOCID = aPPDOCID;
		APPFILE = aPPFILE;
		APPFILETYPE = aPPFILETYPE;
		APPDESC = aPPDESC;
		APPLINKALT = aPPLINKALT;
		APPSERN = aPPSERN;
		APPPROP = aPPPROP;
		APPFLAG = aPPFLAG;
		APPTIME = aPPTIME;
		APPAUTHOR = aPPAUTHOR;
		APPEDITOR = aPPEDITOR;
		ATTRIBUTE = aTTRIBUTE;
		USEDVERSIONS = uSEDVERSIONS;
		CRUSER = cRUSER;
		CRTIME = cRTIME;
		SRCFILE = sRCFILE;
		FILEEXT = fILEEXT;
		ISIMPORTTOIMAGELIB = iSIMPORTTOIMAGELIB;
		RELATEDCOLUMN = rELATEDCOLUMN;
		RELATEPHOTOIDS = rELATEPHOTOIDS;
		APPDESC2 = aPPDESC2;
		ATTRIBUTES = aTTRIBUTES;
	}
	
	
	
}
