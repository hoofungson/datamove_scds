package cn.gov.scds.test;

import cn.gov.scds.util.HtmlRegexpUtil1;

public class HtmlFilterTest {
	
	private static void run2(){
		String htmlStr="<object codebase=\"http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=7,0,19,0\" classid=\"clsid:D27CDB6E-AE6D-11cf-96B8-444553540000\" width=\"500\" height=\"375\">"
            +"<param name=\"movie\" value=\"/UserFiles/Flash/07skqx.swf\" />"
            +"<param name=\"quality\" value=\"high\" /></object>";
		HtmlRegexpUtil1 htmlRegexpUtil=new HtmlRegexpUtil1();
		String htmlMStr=htmlRegexpUtil.replaceHtmlTag(htmlStr, "param", "value", "<embed src=\"", "\" />");
		htmlMStr=htmlRegexpUtil.fiterHtmlTag(htmlMStr, "object");
		htmlMStr=htmlMStr.replace("name=\"movie\" ", "");
		htmlMStr=htmlMStr.replace("name=\"quality\" <embed src=\"high\" />", "");
		
		System.out.println("过滤前："+htmlStr);
		System.out.println("过滤后："+htmlMStr.replace("</object>", ""));
	}
	
	private static void run1(){
		String htmlStr="<object id=\"MediaPlayer\" height=\"343\" width=\"397\" classid=\"clsid:22D6F312-B0F6-11D0-94AB-0080C74C7E95\" src=\"/UserFiles/Media/18ssxcydt03.mp3\">"
				  +"<param name=\"BorderStyle\" value=\"1\" />"
				  +"<param name=\"MousePointer\" value=\"0\" />"
				  +"<param name=\"Enabled\" value=\"1\" />"
				  +"<param name=\"Min\" value=\"0\" />"
				  +"<param name=\"Max\" value=\"10\" />"
				+"</object>";
		
		String htmlMStr="";
		HtmlRegexpUtil1 htmlRegexpUtil=new HtmlRegexpUtil1();
		htmlMStr=htmlRegexpUtil.replaceHtmlTag(htmlStr, "object id=\"MediaPlayer\" height=\"343\" width=\"397\"", "src", "<embed src=\"", "\"/>");
		//htmlMStr=htmlRegexpUtil.replaceHtmlTag(htmlMStr, "param", "value", "<embed src=\"", "\"/>");
		htmlMStr=htmlRegexpUtil.fiterHtmlTag(htmlMStr, "param");
		System.out.println("过滤前："+htmlStr);
		System.out.println("过滤后："+htmlMStr.replace("</object>", ""));
	}

	private static void run(){
		String htmlStr="<img src=\"song.jpg\">哈哈，我是松哥。进入<a href=\"www.hoofungson.cn\">个人网站</a>";
		String htmlString="<object src=\"ss.mp4\">正在努力</object>啦啦<img src=\"mk.mpeg\"/>";
		String htmlString2="<object classid=\"clsid:F08DF954-8592-11D1-B16A-00C0F0283628\" id=\"Slider1\" "
+"width=\"100\" height=\"50\">"
  +"<param name=\"BorderStyle\" value=\"1\" />"
  +"<param name=\"MousePointer\" value=\"0\" />"
  +"<param name=\"Enabled\" value=\"1\" />"
  +"<param name=\"Min\" value=\"0\" />"
  +"<param name=\"Max\" value=\"10\" />"
+"</object>";
        String htmlString3="<object id=\"MediaPlayer\" height=\"343\" width=\"397\" classid=\"clsid:22D6F312-B0F6-11D0-94AB-0080C74C7E95\" src=\"/UserFiles/Media/18ssxcydt03.mp3\"></object>";

		String htmlMStr="";
		HtmlRegexpUtil1 htmlRegexpUtil=new HtmlRegexpUtil1();
		//htmlMStr=htmlRegexpUtil.filterHtml(htmlStr);
		htmlMStr=htmlRegexpUtil.replaceHtmlTag(htmlString3, "object id=\"MediaPlayer\" height=\"343\" width=\"397\"", "src", "<embed src=\"", "\"/>");
		htmlMStr=htmlRegexpUtil.replaceHtmlTag(htmlMStr, "param", "value", "<embed src=\"", "\"/>");
		
		System.out.println("过滤前："+htmlStr);
		System.out.println("过滤后："+htmlMStr.replace("</object>", ""));
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		run2();
	}

}
