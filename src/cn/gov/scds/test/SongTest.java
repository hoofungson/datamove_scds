package cn.gov.scds.test;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.sun.org.apache.bcel.internal.generic.NEW;
import com.sun.org.apache.regexp.internal.RESyntaxException;
import com.sun.org.apache.xpath.internal.operations.And;
import com.trs.infra.persistent.db.DBConnectionConfig;
import com.trs.web2frame.WCMServiceCaller;
import com.trs.web2frame.WCMServiceCallerTest;
import com.trs.web2frame.dispatch.Dispatch;
import com.trs.web2frame.domain.WDocumentMgr;
import com.trs.web2frame.entity.WDocument;
import com.trs.web2frame.util.JsonHelper;

import cn.gov.scds.dao.BaseDao;
import cn.gov.scds.dao.ContentDao;
import cn.gov.scds.dao.impl.ContentDaoImpl;
import cn.gov.scds.entity.Content;
import cn.gov.scds.entity.ContentFile;
import cn.gov.scds.entity.Item;
import cn.gov.scds.entity.WCMCHANNEL;
import cn.gov.scds.jdbc.DBConnection;
import cn.gov.scds.jdbc.DBUitl;
import cn.gov.scds.service.ContentService;
import cn.gov.scds.service.impl.ContentServiceImpl;

public class SongTest {
	public static Connection connect=null;
	public static Statement statement=null;
	public static ResultSet resultSet=null;
	
	
	static Map<Integer, Item> items=new HashMap<Integer, Item>();
	static List<Content> contents=new ArrayList<Content>();
	static List<ContentFile> contentFiles=new ArrayList<ContentFile>();
	
	
	static Map<Integer, ContentFile> contentFileMaps=new HashMap<Integer, ContentFile>();

	private static void readContent(){
		System.out.println("==============进入方法===============");
		BaseDao<Content> baseDao=new BaseDao<Content>();
		//System.out.println("==============断点一===============");
		ContentDao contentDao=new ContentDaoImpl(baseDao);
		ContentService contentService=new ContentServiceImpl(contentDao);
		//System.out.println("==============断点二===============");
		System.out.println(contentService.findAll());
		//System.out.println("==============退出方法===============");
	}
	
	private static void readItems(){
		connect=DBConnection.getConnection();
		statement=DBUitl.getStatement(connect);
		resultSet=DBUitl.getResultSet(statement, "select * from CMS_ITEM");
		try {
			while (resultSet.next()) {
				Item item=new Item();
				item.setItem_id(resultSet.getInt("ITEM_ID"));
				item.setItem_name(resultSet.getString("ITEM_NAME"));
				item.setItem_desc(resultSet.getString("ITEM_DESC"));
				items.put(item.getItem_id(), item);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			DBUitl.close(connect, resultSet, statement);
		}
		
		
	}
	
	private static int readItemIterator(int mk){
		connect=DBConnection.getConnection();
		statement=DBUitl.getStatement(connect);
		int returnFlag=0;
		for(Integer mkey:items.keySet()){
			if(mkey==mk){
				//System.out.println("映射栏目编号："+items.get(mkey).getItem_id());
				String itemName=items.get(mkey).getItem_name();
				returnFlag=readChannel(itemName, 69,statement);
				if(returnFlag!=0){
					//System.out.println("对应的栏目名称："+returnFlag);
					//return returnFlag;
				}
			}
		}
		/*for(Item item:items){
			System.out.println("条目名称："+item.getItem_name());
			if(readChannel(item.getItem_name(), 69,statement)!=0){
				System.out.println("对应的栏目名称："+readChannel(item.getItem_name(), 69,statement));
			}
		}*/
		DBUitl.close(connect, resultSet, statement);
		return returnFlag;
	}
	
	private static int readChannel(String keyword,int parentID,Statement statement){
		
		//resultSet=DBUitl.getResultSet(statement, "select * from TRSWCM.WCMCHANNEL where CHNLDESC like '%"+keyword+"%' and PARENTID="+parentID);
		resultSet=DBUitl.getResultSet(statement, "select * from TRSWCM.WCMCHANNEL where CHNLDESC like '%"+keyword+"%' ");
		try {
			while (resultSet.next()) {
				//System.out.println("栏目名称："+resultSet.getString("CHNLDESC"));
				return resultSet.getInt("CHANNELID");
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			//DBUitl.close(connect, resultSet, statement);
		}
		return 0;
	}
	
	private static void readContentFromJDBC(){
		connect=DBConnection.getConnection();
		statement=DBUitl.getStatement(connect);
		//System.out.println(DBConnection.getConnection());
		//ResultSet resultSet=DBUitl.getResultSet(DBUitl.getStatement(DBConnection.getConnection()), "select * from CMS_ITEM_CONTENT where rownum<=10 and ITEM_ID=555 order by ISSUE_DATE desc");
		//ResultSet resultSet=DBUitl.getResultSet(statement, "select CMS_ITEM_CONTENT.* from CMS_ITEM_CONTENT_FILES left join CMS_ITEM_CONTENT on CMS_ITEM_CONTENT.ID=CMS_ITEM_CONTENT_FILES.CONTENT_ID where ITEM_ID=554 and ISSUE_DATE<=to_date('2015-05-23,00:00:00','yyyy-mm-dd,hh24:mi:ss') and rownum<=200 order by ISSUE_DATE desc");
		ResultSet resultSet=DBUitl.getResultSet(statement,"select * from CMS_ITEM_CONTENT where ITEM_ID=554 and ISSUE_DATE<=to_date('2015-05-31,23:59:59','yyyy-mm-dd,hh24:mi:ss') and rownum<=100 order by ISSUE_DATE");
		//List<Content> contents=new ArrayList<Content>();
		try {
			while(resultSet.next()){
				Content content=new Content();
				content.setContentID(resultSet.getInt("ID"));
				content.setItem_ID(resultSet.getInt("ITEM_ID"));
				content.setTitle_1(resultSet.getString("TITLE_1"));
				//content.setTitle_2(resultSet.getString("TITLE_2"));
				//content.setTitle_3(resultSet.getString("TITLE_3"));
				content.setSubject(resultSet.getString("SUBJECT"));
				content.setKeyword(resultSet.getString("KEYWORD"));
				String item_text=resultSet.getString("ITEM_TEXT").replace("", "");
				content.setItem_text(item_text);
				content.setInscribe(resultSet.getString("INSCRIBE"));
				content.setStatus(resultSet.getString("STATUS"));
				content.setWrite_date(resultSet.getTimestamp("WRITE_DATE"));
				//System.out.println("文档撰写时间："+resultSet.getTimestamp("WRITE_DATE"));
				content.setWrite_user(resultSet.getInt("WRITE_USER"));
				content.setAudit_date(resultSet.getTimestamp("AUDIT_DATE"));
				content.setAudit_user(resultSet.getInt("AUDIT_USER"));
				content.setIssue_date(resultSet.getTimestamp("ISSUE_DATE"));
				content.setIssue_user(resultSet.getInt("ISSUE_USER"));
				content.setShow_number(resultSet.getInt("SHOW_NUMBER"));
				//content.setDocchannel(readItemIterator(resultSet.getInt("ITEM_ID")));
				//contents.add(content);
				
				//System.out.println("文档名称："+resultSet.getString("TITLE_1"));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			DBUitl.close(connect, resultSet, statement);
		}
		
		for(Content mContent:contents){
			//if(mContent.getDocchannel()!=0){
			System.out.println("编号："+mContent.getContentID());
			System.out.println("栏目编号："+mContent.getItem_ID());
			//System.out.println("对应的新数据栏目编号："+mContent.getDocchannel());
			System.out.println("标题一："+mContent.getTitle_1());
			//System.out.println("标题二："+mContent.getTitle_2());
			//System.out.println("标题三："+mContent.getTitle_3());
			System.out.println("科目："+mContent.getSubject());
			
			/*System.out.println("============文档写入开始============");
			
			String fileUrl = "http://155.16.16.60:7001/manager/download/downfj.jsp?fileid="+conFile.getFile_name(); 
			testSaveAll(String filePath);*/
			
			System.out.println("==================分隔线================");
			//}
		}
		
		//readContentFile(10);
	}
	
	private static void readContentFile(int DOCID){
		connect=DBConnection.getConnection();
		statement=DBUitl.getStatement(connect);
		ResultSet resultSet=DBUitl.getResultSet(statement, "select CMS_ITEM_CONTENT_FILES.* from CMS_ITEM_CONTENT_FILES left join CMS_ITEM_CONTENT on CMS_ITEM_CONTENT.ID=CMS_ITEM_CONTENT_FILES.CONTENT_ID where ISSUE_DATE<=to_date('2015-05-23,00:00:00','yyyy-mm-dd,hh24:mi:ss') and rownum<=200 order by ISSUE_DATE desc");
		//List<Content> contents=new ArrayList<Content>();
		try {
			while(resultSet.next()){
				//Map<Integer, ContentFile> conMap=new HashMap<Integer, ContentFile>();
				ContentFile contentFile=new ContentFile();
				contentFile.setContent_id(resultSet.getInt("CONTENT_ID"));
				contentFile.setFile_name(resultSet.getString("FILE_NAME"));
				contentFile.setFile_info(resultSet.getString("FILE_INFO"));
				contentFile.setFile_size(resultSet.getInt("FILE_SIZE"));
				contentFile.setFile_text(resultSet.getString("FILE_EXT"));
				
				contentFileMaps.put(contentFile.getContent_id(), contentFile);
				
				//contentFiles.add(conMap);
				
				//System.out.println("文档名称："+resultSet.getString("FILE_ID"));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			//DBUitl.close(connect, resultSet, statement);
		}
		
		for(Integer mkey:contentFileMaps.keySet()){
			System.out.println("编号："+contentFileMaps.get(mkey).getContent_id());
			System.out.println("文件名："+contentFileMaps.get(mkey).getFile_name());
			System.out.println("文件信息："+contentFileMaps.get(mkey).getFile_info());
			System.out.println("文件大小："+contentFileMaps.get(mkey).getFile_size());
			System.out.println("文件类型："+contentFileMaps.get(mkey).getFile_text());
			
			String fileUrl = "http://155.16.16.60:7001/manager/download/downfj.jsp?fileid="+contentFileMaps.get(mkey).getFile_name(); 
			String fileName = contentFileMaps.get(mkey).getFile_name();
			try {
				getRemoteFile(fileUrl, fileName);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			System.out.println("=============分隔线=============");
		}
		
		/*for(ContentFile conFile:contentFiles){
			System.out.println("编号："+conFile.getContent_id());
			System.out.println("文件名："+conFile.getFile_name());
			System.out.println("文件信息："+conFile.getFile_info());
			System.out.println("文件大小："+conFile.getFile_size());
			System.out.println("文件类型："+conFile.getFile_text());
			
			//System.out.println("=============附件读取开始=============");
			
			String fileUrl = "http://155.16.16.60:7001/manager/download/downfj.jsp?fileid="+conFile.getFile_name(); 
			String fileName = conFile.getFile_name();
			
			String localFile="G://AppData/SCDS/Appendix/"+conFile.getFile_name();
			
			try {
				testSaveAll(localFile);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			try {
				getRemoteFile(fileUrl, fileName);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			System.out.println("=============附件读取完毕=============");
			
			System.out.println("=============分隔线=============");
		}*/
	}
	
	private static int jiexi(Dispatch oDispatch){
		Map oJson = oDispatch.getJson();
		String mStr="";
		
		char[] mapStr=oJson.toString().toCharArray();
		for(int i=0;i<mapStr.length;i++){
			if(mapStr[i]>=48&&mapStr[i]<=57){
				mStr=mStr+mapStr[i];
			}
		}
		
		//System.out.println("文档列表："+oJson.entrySet());
		
		
		
		return Integer.parseInt(mStr);
	}
	
	private static void writeContentToDocument(){
		System.out.println("==================进入栏目写方法==================");
		//说明：在Id为10的栏目下导入文档lalala…
		String sServiceId = "wcm6_document";
		String sMethodName = "save";
		/*Map oPostData = new HashMap();*/
		/*oPostData.put("ChannelId", new Integer(90));
		oPostData.put("ObjectId ", new Integer(0));*/
		
		for(Content mmContent:contents){
			Map oPostData = new HashMap();
			oPostData.put("ChannelId", new Integer(81));
			oPostData.put("ObjectId ", new Integer(0));
			oPostData.put("DocID ", mmContent.getContentID());
			oPostData.put("DOCTYPE", new Integer(20));
			oPostData.put("DocTitle ", mmContent.getTitle_1());
			oPostData.put("DocHtmlCon ", mmContent.getItem_text());
			oPostData.put("DOCPUBTIME ", mmContent.getIssue_date());
			oPostData.put("DOCSTATUS ", new Integer(10));
			oPostData.put("DOCSOURCENAME ", "四川省地方税务局");
			oPostData.put("HITSCOUNT ", mmContent.getShow_number());
		
		
		/*oPostData.put("DocTitle ", "我的测试数据");
		oPostData.put("DocHtmlCon ", "测试栏目写入数据是否成功！哈哈哈，好像成功了！");*/
		try {
			Dispatch oDispatch = WCMServiceCaller.Call(sServiceId, sMethodName,
			                oPostData, true);
			System.out.println("返回："+oDispatch);
			System.out.println("返回文编号："+jiexi(oDispatch));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		}
		
		System.out.println("==================完成栏目写方法==================");
	}
	
	private static void readWaiFile(){
		File beforefile = new File("e://a.txt");
		//这是你要保存之后的文件，是自定义的，本身不存在
		File afterfile = new File("d://a.txt");
		//定义文件输入流，用来读取beforefile文件
		FileInputStream fis = null;
		try {
			fis = new FileInputStream(beforefile);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		//定义文件输出流，用来把信息写入afterfile文件中
		FileOutputStream fos = null;
		try {
			fos = new FileOutputStream(afterfile);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		//文件缓存区
		byte[] b = new byte[1024];
		//将文件流信息读取文件缓存区，如果读取结果不为-1就代表文件没有读取完毕，反之已经读取完毕
		try {
			while(fis.read(b)!=-1){
			//将缓存区中的内容写到afterfile文件中
			fos.write(b);
			fos.flush();
			}
			System.out.println("========哈哈，搞定===========");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	/** 
	* 通过HTTP方式获取文件 
	* 
	* @param strUrl 
	* @param fileName 
	* @return 
	* @throws IOException 
	*/ 
	private static boolean getRemoteFile(String strUrl, String fileName) throws IOException { 
	   URL url = new URL(strUrl);
	   HttpURLConnection conn = (HttpURLConnection) url.openConnection(); 
	   DataInputStream input = new DataInputStream(conn.getInputStream()); 
	   DataOutputStream output = new DataOutputStream(new FileOutputStream("G://AppData/SCDS/Appendix/"+fileName)); 
	   byte[] buffer = new byte[1024 * 8]; 
	   int count = 0; 
	   while ((count = input.read(buffer)) > 0) {
	      output.write(buffer, 0, count); 
	   } 
	   output.close(); 
	   input.close(); 
	   return true; 
	}
	
	private static void getFileByHTTP(){
		String fileUrl = "http://155.16.16.60:7001/manager/download/downfj.jsp?fileid=四川省地方税务局2014年度纳税信用A级纳税人名单1430272078515.xls"; 
		String fileName = "四川省地方税务局2014年度纳税信用A级纳税人名单1430272078515.xls";
		
		try {
			System.out.println(getRemoteFile(fileUrl, fileName));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
	}
	
	/**
	 * 保存文档及其所有相关项，包括引用栏目，附件，相关文档
	 * 
	 * @param _oDocument
	 * @throws Exception 
	 */
	private static void saveAll(WDocument _oDocument) throws Exception {
	    
	    
		WDocumentMgr.save(_oDocument);
		WDocumentMgr.quoteTo(_oDocument);
		//WDocumentMgr.saveAppendixs(_oDocument);
		//WDocumentMgr.saveRelations(_oDocument);
		
		
	}
	
	/** 附件类型标识：文档附件 */
    public final static int FLAG_DOCAPD = 10; // 文件:Document Appendix

    /** 附件类型标识：文档图片 */
    public final static int FLAG_DOCPIC = 20; // 图片:Document Picture

    /** 附件类型标识：HTML */
    public final static int FLAG_HTMLPIC = 30; // HTML图片:Html Picture

    /** 附件类型标识：链接附件 */
    public final static int FLAG_LINK = 40; // 链接:Link
    
    
	public static void testSaveAll() throws Exception {
		System.out.println("=========进入文档写方法======");
		int i=1;
		for(Content mmContent:contents){
			/*if(i<=20){*/
		  //if(contentFileMaps.get(new Integer(mmContent.getContentID()))!=null&&mmContent.getDocchannel()!=0){
			//if(contentFileMaps.get(new Integer(mmContent.getContentID()))!=null){
		  WDocument oDocument = new WDocument();
		
		  oDocument.setFieldValue("ChannelId", 133);
		  oDocument.setFieldValue("ObjectId", new Integer(0));
		
		  oDocument.setFieldValue("DocTitle", mmContent.getTitle_1());
		  oDocument.setFieldValue("DOCTYPE", new Integer(20));
		  oDocument.setFieldValue("CRTIME", mmContent.getWrite_date());
		  oDocument.setFieldValue("DOCRELTIME", mmContent.getWrite_date());
		  //oDocument.setFieldValue("CRUSER", mmContent.getWrite_user());
		  oDocument.setFieldValue("DOCEDITOR", mmContent.getIssue_user());
		  oDocument.setFieldValue("DOCCONTENT", mmContent.getItem_text());
		  oDocument.setFieldValue("DocHtmlCon", mmContent.getItem_text());
		  oDocument.setFieldValue("DOCPUBTIME ", mmContent.getIssue_date());
		  oDocument.setFieldValue("DOCSTATUS ", new Integer(10));
		  oDocument.setFieldValue("DOCSOURCENAME ", "四川省地方税务局");
		  oDocument.setFieldValue("HITSCOUNT ", mmContent.getShow_number());
		  
		  //System.out.println("打印出来："+contentFileMaps.size()+",编号："+mmContent.getContentID());
		  
		  System.out.println("创建时间："+mmContent.getWrite_date());
		  System.out.println("发布时间："+mmContent.getIssue_date());
		  System.out.println("撰写时间："+mmContent.getWrite_date());
		  
		  //System.out.println("组合："+contentFileMaps.get(new Integer(mmContent.getContentID())));
		
		  //String localFile="G://AppData/SCDS/Appendix/"+contentFileMaps.get(mmContent.getContentID()).getFile_name();
		  //oDocument.addAppendix(FLAG_DOCAPD, localFile);
		  System.out.println("附件编号："+i);
		
		
		  //oDocument.addRelation(898);
		  //oDocument.addRelation(884);
		
		  saveAll(oDocument);
		  i++;
			//}
		  //}
		}
		
		System.out.println("=========完毕文档写方法======");
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//readItems();
		//readContentFromJDBC();
		//readItemIterator();
	    //System.out.println("栏目名称："+readChannel("图片要闻",69));
		readContentFromJDBC();
		//readContentFile(10);
		
		try {
			testSaveAll();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//writeContentToDocument();
		//writeDocumentByJDBC();
		//readWaiFile();
		//getFileByHTTP();
	}

}
