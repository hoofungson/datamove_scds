package cn.gov.scds.test;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import cn.gov.scds.jdbc.DBConnection;
import cn.gov.scds.jdbc.DBUitl;
import cn.gov.scds.util.DataContentUtil;
import cn.gov.scds.util.HtmlTagCharacterUtil;

public class SungTest {

	private static void myIterator(){
		System.out.println("============栏目遍历开始.......");
		
		digui(5);
		
		System.out.println("============栏目遍历结束.......");
	}
	
	private static void digui(int fid){
		
		ResultSet resultSet=DBUitl.getResultSet(DBUitl.getStatement(DBConnection.getConnection()), "select * from CMS_ITEM where ITEM_PRIOR="+fid);
		try {
			while (resultSet.next()) {
				System.out.println("栏目名称："+resultSet.getString("ITEM_NAME"));
				
			}
			
			if(resultSet.getInt("ITEM_ID")==resultSet.getInt("ITEM_PRIOR")){
				digui(resultSet.getInt("ITEM_ID"));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//runsong();
		run5();
	}
	
	private static void runsong(){
		   /*String regex = "第[0-9]*条";
		   String str = "第9条,数据错误,错误信息,第jjj哦条哦条我的条件如何？第221条xx";*/
		   /*String regex = "m[0-9]{1,3};";*/
		String regex = "m[\u4e00-\u9fa5\\w]{1,10};";
		   String str = "style=\"font-size:5;famil:微软雅黑;\" style=\"font-size:22;famil:微软雅黑;\"";
		   /*str=str.replace("font-size:", "m");*/
		   str=str.replace("famil:", "m");
		   Pattern pat = Pattern.compile(regex);
		   Matcher matcher = pat.matcher(str);
		   System.out.println(matcher.groupCount());
		   int i=1;
		   List<String> reStrings=new ArrayList<String>();
		   while (matcher.find()) {
			   
			   //System.out.println("第："+i+"次替换");
		     String temp = str.substring(matcher.start(),matcher.end());
		     //str = str.replaceAll(temp, temp.substring(0,temp.lastIndexOf(";"))+"");
		     //str = str.replace(matcher.group(), "");
		     //System.out.println(temp);
		     System.out.println(matcher.group());
		     reStrings.add(matcher.group());
		     //i++;
		   }
		   
		   for(String string:reStrings){
			   str=str.replace(string, "");
		   }
		   System.out.println(str);
	}
	
	private static void run5(){
		String htmlStr="<p style=\"TEXT-INDENT: 24px;font-size:32px;\"><font style=\"FONT-SIZE: 14px\">通知要求，要充分认识学习贯彻省第九次党代会精神的重大意义。全面学习贯彻落实杜青林同志在省第九次党代会上所作的工作报告，认真领会精神实质，准确把握实践要求。要紧密联系地税实际，始终坚持“聚财为国，执法为民”的税务工作宗旨，发扬“艰苦奋斗，勇挑重担，团结拼博，求实开拓”的地税精神，为今后五年我省经济建设、政治建设、文化建设、社会建设和党的建设以及富民强省全面小康的奋斗目标，做好本职工作。</font></p>";
		System.out.println(DataContentUtil.replaceContentCharacter(htmlStr));
	}

}
