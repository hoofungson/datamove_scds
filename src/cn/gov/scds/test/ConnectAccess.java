package cn.gov.scds.test;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Properties;

public class ConnectAccess {
	
    public static void main(String args[]) throws Exception {
        ConnectAccess ca=new ConnectAccess();
        //ca.ConnectAccessFile();
        ca.ConnectAccessDataSource();
    }  
    public void ConnectAccessFile() throws Exception   
    {  
        Class.forName("sun.jdbc.odbc.JdbcOdbcDriver");
        
        Properties props = new Properties();
        props.put("charSet", "gb2312");
        String dbur1 = "jdbc:odbc:driver={Microsoft Access Driver (*.mdb,*.accdb)};DBQ=d:\\SiteWeaver_dznw.mdb";  
        Connection conn = DriverManager.getConnection(dbur1,props);
        Statement stmt = conn.createStatement();
        ResultSet rs = stmt.executeQuery("select * from PE_Article");
        while (rs.next()) {
            System.out.println(rs.getInt(1));
        }  
        rs.close();
        stmt.close();
        conn.close();
    }  
    public void ConnectAccessDataSource()throws Exception {  
        Class.forName("sun.jdbc.odbc.JdbcOdbcDriver");
        
        String dbur1 = "jdbc:odbc:dznw_data";
        
        Properties props = new Properties();
        props.put("charSet", "gb2312");
        //Connection conn = DriverManager.getConnection(dbur1, "hoosung", "h123456s");
        Connection conn = DriverManager.getConnection(dbur1,props);
        Statement stmt = conn.createStatement();
        ResultSet rs = stmt.executeQuery("select * from PE_Article");
        while (rs.next()) {
            System.out.println(rs.getString(1));
        }
        rs.close();
        stmt.close();
        conn.close();
    }
}
