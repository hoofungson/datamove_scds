package cn.gov.scds.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class DBConnection {
	private static DBConnection db = null;
	private final String user = "music";
	private final String pass = "123456";
	//private final String url = "jdbc:oracle:thin:@155.16.171.80:1521:orcl";
	/*oracle数据库驱动*/
	private final String driver = "oracle.jdbc.driver.OracleDriver";
	/*mysql数据库驱动*/
	//private final String driver = "com.mysql.jdbc.Driver";
	/*sqlserver数据驱动*/
	//private final String driver = "com.microsoft.sqlserver.jdbc.SQLServerDriver";
	
	/*Access数据库驱动*/
	//private final String driver = "sun.jdbc.odbc.JdbcOdbcDriver";

	private DBConnection() {
		try {
			Class.forName(driver);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}

	private static synchronized DBConnection getInstance() {
		if (db == null)
			db = new DBConnection();
		return db;
	}

	private final Connection connection() {
		Connection conn = null;
		try {
			//conn = DriverManager.getConnection(url, user, pass);
			/*四川地税外网数据库连接*/
			
			/*四川地税内网数据库连接*/
			
			
			/*成都地税内网数据库连接*/
			
			
			/*成都地税外网数据库连接*/
			
			
			/*巴中地税内网数据库连接*/
			
			
			/*巴中地税外网数据库连接*/
			
			
			/*南充地税内网数据连接*/
			
			/*自贡地税内网数据连接*/
			
			
			/*自贡地税外网数据连接*/
			
			
			/*攀枝花地税内网数据连接*/
			
			
			/*宜宾地税内网数据连接*/
			
			
			/*德阳地税外网数据连接*/
			
			
			/*乐山地税外网数据连接*/
			
			
			/*达州地税外网数据连接*/
			
			
			/*凉山地税外网数据连接*/
			
			
			/*达州地税内网数据连接*/
			/*Properties props = new Properties();
	        props.put("charSet", "gb2312");
	        String dbur1 = "jdbc:odbc:dznw_data";
			conn=DriverManager.getConnection(dbur1,props);*/
			
			/*乐山地税内网数据连接*/
			/*Properties props = new Properties();
	        props.put("charSet", "gb2312");
	        String dbur1 = "jdbc:odbc:lsnw_data";
			conn=DriverManager.getConnection(dbur1,props);*/
			
			/*内江地税内网*/
			/*Properties props = new Properties();
	        props.put("charSet", "gb2312");
	        String dbur1 = "jdbc:odbc:njnw_data";
			conn=DriverManager.getConnection(dbur1,props);*/
			
			/*内江地税外网*/
			/*Properties props = new Properties();
	        props.put("charSet", "gb2312");
	        String dbur1 = "jdbc:odbc:njww_data";
			conn=DriverManager.getConnection(dbur1,props);*/
			
			/*眉山地税内网*/
			/*Properties props = new Properties();
	        props.put("charSet", "gb2312");
	        String dbur1 = "jdbc:odbc:msnw_data";
			conn=DriverManager.getConnection(dbur1,props);*/
			
			/*自贡地税外网数据连接*/
			
			
			/*眉山地税外网数据连接*/
			
			
			/*资阳地税外网数据连接*/
			
			
			/*德阳地税内网数据连接*/
			
			
			/*广安地税外网数据连接*/
			
			
			/*宜宾地税外网数据连接*/
			
			
			/*攀枝花地税外网数据连接*/
			
			
		} catch (SQLException e) {
			e.printStackTrace();
			System.out.print("连接异常======================");
		}
		return conn;
	}

	private final void close(Connection conn) {
		try {
			conn.close();
			conn = null;
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public synchronized final static Connection getConnection() {
		return getInstance().connection();
	}

	public synchronized final static void closeConnection(Connection conn) {
		getInstance().close(conn);
	}
}
