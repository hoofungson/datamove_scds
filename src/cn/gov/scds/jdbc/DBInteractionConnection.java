package cn.gov.scds.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBInteractionConnection {
	private static DBInteractionConnection db = null;
	private final String user = "music";
	private final String pass = "123456";
	private final String url = "jdbc:oracle:thin:@155.16.171.80:1521:orcl";
	private final String driver = "oracle.jdbc.driver.OracleDriver";

	private DBInteractionConnection() {
		try {
			Class.forName(driver);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}

	private static synchronized DBInteractionConnection getInstance() {
		if (db == null)
			db = new DBInteractionConnection();
		return db;
	}

	private final Connection connection() {
		Connection conn = null;
		try {
			//conn = DriverManager.getConnection(url, user, pass);
		} catch (SQLException e) {
			e.printStackTrace();
			System.out.print("连接异常======================");
		}
		return conn;
	}

	private final void close(Connection conn) {
		try {
			conn.close();
			conn = null;
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public synchronized final static Connection getConnection() {
		return getInstance().connection();
	}

	public synchronized final static void closeConnection(Connection conn) {
		getInstance().close(conn);
	}
}
