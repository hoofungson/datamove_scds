package cn.gov.scds.jdbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class DBUitl {
     //获取连接。
	//public static Connection connect=null;
	public static Statement statement=null;
	public static ResultSet resultSet=null;
	public static String ErrorInfo=null;
	
	public static Statement getStatement(Connection connect) {
		try {
			if (connect != null) {
				statement = connect.createStatement();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return statement;
	}


	public static ResultSet getResultSet(Statement statement,String sql) {
		try {
			if (statement != null) {
				resultSet = statement.executeQuery(sql);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return resultSet;
	}
	
	public static void close(Connection connect,ResultSet resultSet,Statement statement) {
		 try {
			//如果记录为空。
	            if (resultSet != null) {
	                try {
	                	resultSet.close();
	                } catch (SQLException e) {
	                    //e.printStackTrace();
	                	e.printStackTrace();
	                }
	            }
	 
	           /* 如果statment对象为空。*/
	            if (statement != null) {
	                try {
	                	statement.close();
	                } catch (SQLException e) {
	                   // e.printStackTrace();
	                	e.printStackTrace();
	                }
	            }
	 
	            //如果连接为空。
	            if (connect != null) {
	                try {
	                	connect.close();
	                } catch (SQLException e) {
	                    //e.printStackTrace();
	                	e.printStackTrace();
	                }
	            }
		} catch (Exception e) {
			e.printStackTrace();
	   }
	}
}
