/**
 * 
 */
package cn.gov.scds.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * @author SungHu
 *
 */
public class DBC {
	
	public static Connection connect=null;
	public static Statement statement=null;
	public static ResultSet resultSet=null;
	public static String ErrorInfo=null;
	/**
	 * @param args
	 */
	public static Connection getConnection(){
           try {
			Class.forName("org.gjt.mm.mysql.Driver").newInstance();
		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
            try {
				//connect =DriverManager.getConnection("jdbc:mysql://localhost:3306/webbook", "root","root");
            	connect =DriverManager.getConnection("jdbc:mysql://127.0.0.1:3306/music?user=root&password=&useUnicode=true&characterEncoding=utf8");
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		return connect;
	}
	public static Statement getStatement(Connection connect) {
		try {
			if (connect != null) {
				statement = connect.createStatement();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return statement;
	}
	public static ResultSet getResultSet(Statement statement,String sql) {
		try {
			if (statement != null) {
				resultSet = statement.executeQuery(sql);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return resultSet;
	}
	public static void close(Connection connect,ResultSet resultSet,Statement statement) {
		 try {
			 // �رռ�¼��
	            if (resultSet != null) {
	                try {
	                	resultSet.close();
	                } catch (SQLException e) {
	                    //e.printStackTrace();
	                	e.printStackTrace();
	                }
	            }
	 
	            // �ر�����
	            if (statement != null) {
	                try {
	                	statement.close();
	                } catch (SQLException e) {
	                   // e.printStackTrace();
	                	e.printStackTrace();
	                }
	            }
	 
	            // �ر����Ӷ���
	            if (connect != null) {
	                try {
	                	connect.close();
	                } catch (SQLException e) {
	                    //e.printStackTrace();
	                	e.printStackTrace();
	                }
	            }
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
       System.out.println(getConnection());
       System.out.println(getStatement(getConnection()));
       //System.out.println(getResultSet(getStatement(getConnection()),"select * from book"));
       //System.out.println(close(getConnection(),ResultSet resultSet,Statement statement));
	}
}
