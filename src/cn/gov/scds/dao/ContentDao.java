package cn.gov.scds.dao;

import java.util.List;

import cn.gov.scds.entity.Content;

public interface ContentDao {
	public List<Content> findAll();
    public List<Content> findByItemID(int item_id);
    public List<Content> findByID(int contentID);
    public List<Content> find(int item_id,int contentID);
}
