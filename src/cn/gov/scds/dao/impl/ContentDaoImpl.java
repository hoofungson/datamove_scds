package cn.gov.scds.dao.impl;

import java.util.List;

import cn.gov.scds.dao.BaseDao;
import cn.gov.scds.dao.ContentDao;
import cn.gov.scds.entity.Content;

public class ContentDaoImpl implements ContentDao{

	private BaseDao<Content> baseDao;
	
	public ContentDaoImpl(BaseDao<Content> baseDao){
		this.setBaseDao(baseDao);
	}
	
	public BaseDao<Content> getBaseDao() {
		return baseDao;
	}

	public void setBaseDao(BaseDao<Content> baseDao) {
		this.baseDao = baseDao;
	}

	public List<Content> findAll() {
		// TODO Auto-generated method stub
		return this.getBaseDao().list("from Content");
	}

	public List<Content> findByItemID(int item_id) {
		// TODO Auto-generated method stub
		return this.getBaseDao().list("from Content where ITEM_ID="+item_id);
	}

	public List<Content> findByID(int contentID) {
		// TODO Auto-generated method stub
		return this.getBaseDao().list("from Content where CONTENT_ID="+contentID);
	}

	public List<Content> find(int item_id, int contentID) {
		// TODO Auto-generated method stub
		return this.getBaseDao().list("from Content where ITEM_ID="+item_id+" and CONTENT_ID="+contentID);
	}

}
