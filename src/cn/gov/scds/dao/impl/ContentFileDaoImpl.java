package cn.gov.scds.dao.impl;

import java.util.List;

import cn.gov.scds.dao.BaseDao;
import cn.gov.scds.dao.ContentFileDao;
import cn.gov.scds.entity.ContentFile;

public class ContentFileDaoImpl implements ContentFileDao{
	
	private BaseDao<ContentFile> baseDao;
	
	public BaseDao<ContentFile> getBaseDao() {
		return baseDao;
	}

	public void setBaseDao(BaseDao<ContentFile> baseDao) {
		this.baseDao = baseDao;
	}

	public List<ContentFile> findAll() {
		// TODO Auto-generated method stub
		return this.getBaseDao().list("from ContentFile");
	}

	public List<ContentFile> findByContentID(int content_id) {
		// TODO Auto-generated method stub
		return this.getBaseDao().list("from ContentFile where CONTENT_ID="+content_id);
	}

	public List<ContentFile> findByID(int contentID) {
		// TODO Auto-generated method stub
		return this.getBaseDao().list("from ContentFile where FILE_ID="+contentID);
	}

	public List<ContentFile> find(int content_id, int contentID) {
		// TODO Auto-generated method stub
		return this.getBaseDao().list("from ContentFile where FILE_ID="+contentID+" and CONTENT_ID="+content_id);
	}

}
