package cn.gov.scds.dao;

import java.io.Serializable;
import java.util.List;

import org.hibernate.Session;

import cn.gov.scds.util.HibernateUtil;


public class BaseDao<T> {

	public void create(T object){
		/*
		 * �����Ự��
		 * */
		Session session=HibernateUtil.getSessionFactory().openSession();
		try {
			session.beginTransaction();//��������
			session.persist(object);//�����󱣴�����ݿ⡣
			session.getTransaction().commit();
		} catch (Exception e) {
			// TODO: handle exception
			session.getTransaction().rollback();//�����쳣�׳�����ع�����
		}finally{
			session.close();
		}
	}
	
	public void update(T object){
		/*
		 * �������·�����
		 * */
		Session session=HibernateUtil.getSessionFactory().openSession();
		try {
			session.beginTransaction();//��������
			session.update(object);//����object��Ӧ�������С�
			session.getTransaction().commit();
		} catch (Exception e) {
			// TODO: handle exception
			session.getTransaction().rollback();//�����쳣�׳�����ع�����
		}finally{
			session.close();
		}
	}
	
	public void delete(T object){
		/*
		 * ����ɾ��������
		 * */
		Session session=HibernateUtil.getSessionFactory().openSession();
		try {
			session.beginTransaction();//��������
			session.delete(object);//ɾ��object��Ӧ�������С�
			session.getTransaction().commit();
		} catch (Exception e) {
			// TODO: handle exception
			session.getTransaction().rollback();//�����쳣�׳�����ع�����
		}finally{
			session.close();
		}
	}
	
	public T find(Class<? extends T> clazz,Serializable id){
		/*
		 * ����ID����ʵ�塣
		 * */
		Session session=HibernateUtil.getSessionFactory().openSession();
		try {
			session.beginTransaction();//��������
			return (T) session.get(clazz, id);//����id��ѯʵ�������
		}finally{
			session.getTransaction().commit();
			session.close();
		}
		
	}
	
	public List<T> list(String hql){
		/*
		 * ���Ҷ��ʵ�塣
		 * */
		Session session=HibernateUtil.getSessionFactory().openSession();
		try {
			session.beginTransaction();//��������
			return session.createQuery(hql).list();//����id��ѯʵ�������
		}finally{
			session.getTransaction().commit();
			session.close();
		}
	}
}
