package cn.gov.scds.dao;

import java.util.List;

import cn.gov.scds.entity.ContentFile;

public interface ContentFileDao {
	public List<ContentFile> findAll();
    public List<ContentFile> findByContentID(int content_id);
    public List<ContentFile> findByID(int contentID);
    public List<ContentFile> find(int content_id,int contentID);
}
